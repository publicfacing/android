package com.example.warefhaque.dapeandroid.Messages;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.SectionIndexer;
import android.widget.TextView;

import com.example.warefhaque.dapeandroid.Profile.ProfilePresenter;
import com.example.warefhaque.dapeandroid.R;
import com.example.warefhaque.dapeandroid.Utils.AvatarImageView;
import com.example.warefhaque.dapeandroid.Utils.ImageLoader;
import com.layer.sdk.messaging.Conversation;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by warefhaque on 2/21/17.
 */

public class MessagesListAdapter extends BaseAdapter implements StickyListHeadersAdapter,SectionIndexer,PushNotificationReceiver.PushListener, MessagesListActivity.UpdateConversationListener, DareInterfaceActivity.UpDateDare{
    public static final String TAG = "MessagesListAdapter";
    private final Context mContext;
    public List<DareActivity> mActivities;
    public int[] mSectionIndices;
    public ParseObject[] mSectionHeaders;
    private LayoutInflater mInflater;
    private Handler mHandler;
    public List<DareActivity> sectionHelper;
    public ImageLoader mImageLoader;
    public static final int CHECK_IN_CELL = 0;
    public static final int UNOPENED_DARE_CELL = 1;
    public static final int OPENED_DARE_CELL = 2;
    public static final int CONVERSATION_CELL=3;
    HashMap<String, Date> checkedInTimes = new HashMap<>();

    UpdateActivities mUpdateActivities;

    MessagesListAdapter(Context context, UpdateActivities updateActivities){
        sectionHelper= new ArrayList<>();
        mContext = context;
        mInflater = LayoutInflater.from(context);
        mActivities = new ArrayList<>();
        mUpdateActivities = updateActivities;
        DareInterfaceActivity.upDateDare = this;
        mImageLoader = new ImageLoader(context);


        try {
            if (ParseUser.getCurrentUser()!=null){
                ParseUser.getCurrentUser().fetch();
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

        MessagesListActivity.setUpdateConversationListener(this);
        if (mActivities.size()>0 && mActivities!=null){
            mSectionIndices = getSectionIndices();
            mSectionHeaders = getSectionLetters();
            //call another function to save the object ids to the date objects in the hashmap
        }else{
            Log.e(TAG, "No activities found");
        }
        //setting up the Push
        PushNotificationReceiver.setUpListener(this);
        mHandler = new Handler(Looper.getMainLooper());
    }

    /**
     * Somehow have to get the places indices here and call this in the interface callback
     * @return
     */
    public int[] getSectionIndices() {
        if (mActivities.size() <= 0 || mActivities ==null) {
            int[] empty = new int[0];
            return empty;
        }
        ArrayList<Integer> sectionIndices = new ArrayList<Integer>();

        ParseObject place = mActivities.get(0).mActivity.getParseObject("place");

        assert place!=null;

        String placeId = place.getObjectId();

        sectionIndices.add(0);
        for (int i = 0; i < mActivities.size(); i++) {
            /**
             * if the place id is not equal you save the section index in section indices
             * and save the place object in the sections list
             */
            assert mActivities.get(i).mActivity.getParseObject("place")!=null;
            //add the place ids which become sections to an array and check if the one checking it contains it
            if (!mActivities.get(i).mActivity.getParseObject("place").getObjectId().equals(placeId)) {
                placeId = mActivities.get(i).mActivity.getParseObject("place").getObjectId();
                sectionIndices.add(i);
            }
        }
        /**
         * only copies the arraylist content over to an integer array
         * suitable for re
         */
        int[] sections = new int[sectionIndices.size()];
        for (int i = 0; i < sectionIndices.size(); i++) {
            sections[i] = sectionIndices.get(i);
        }
        return sections;
    }

    /**
     *
     * @return
     */

    public ParseObject[] getSectionLetters() {
        ParseObject[] placesSection = new ParseObject[mSectionIndices.length];

            for (int i = 0; i < mSectionIndices.length; i++) {
                placesSection[i] = mActivities.get(mSectionIndices[i]).mActivity.getParseObject("place");

        }
        return placesSection;
    }

    @Override
    public int getCount() {
        return mActivities.size();
    }

    @Override
    public Object getItem(int position) {
        return mActivities.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        return 4;
    }

    @Override
    public int getItemViewType(int position) {
        if (mActivities.get(position).mType.equals("checkin")){
            return CHECK_IN_CELL;
        }else if (mActivities.get(position).mType.equals("dare")){
            List<ParseUser> readBy = mActivities.get(position).mActivity.getList("readBy");

            if ( mActivities.get(position).mActivity.getParseObject("createdBy").getObjectId().equals(ParseUser.getCurrentUser().getObjectId())){
                return OPENED_DARE_CELL;
            }

            if (readBy.size()==0){
                return UNOPENED_DARE_CELL;
            }else{
                return OPENED_DARE_CELL;
            }

        }else{
            return CONVERSATION_CELL;
        }
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        int viewType = getItemViewType(position);
        if (viewType==CHECK_IN_CELL){
            CheckinHolder checkinHolder;
            if (convertView==null){
                checkinHolder = new CheckinHolder();
                convertView = mInflater.inflate(R.layout.checkin_cell, parent, false);
                convertView.setTag(checkinHolder);
            }else{
                checkinHolder = (CheckinHolder) convertView.getTag();
            }
            return convertView;
        }else if (viewType==CONVERSATION_CELL){
            ViewHolder holder;
            if (convertView == null)
            {
                holder = new ViewHolder();
                convertView = mInflater.inflate(R.layout.conversation_item, parent, false);
                holder.participantName = (TextView) convertView.findViewById(R.id.participants);
                holder.icon = (ImageView) convertView.findViewById(R.id.flow_item_icon);
                holder.unreadMessageCircle = (ImageView) convertView.findViewById(R.id.unread_message);
                convertView.setTag(holder);
            }
            else
            {
                holder = (ViewHolder) convertView.getTag();
            }

            holder.setupViews(mActivities.get(position));

            return convertView;
        }else if (viewType==OPENED_DARE_CELL){
            OpenedDare openedDare;
            if (convertView == null) {
                openedDare = new OpenedDare();
                convertView = mInflater.inflate(R.layout.opened_dare_item, parent, false);
                openedDare.name = (TextView) convertView.findViewById(R.id.participants);
                openedDare.mprofilePic = (AvatarImageView) convertView.findViewById(R.id.flow_item_icon);
                convertView.setTag(openedDare);
            } else {
                openedDare = (OpenedDare) convertView.getTag();
            }

            openedDare.setUpViews(mActivities.get(position));

            return convertView;
        }else {
            //unopened dare cell
            UnopenedDareHolder unopenedDareHolder;
            if (convertView == null) {
                unopenedDareHolder = new UnopenedDareHolder();
                convertView = mInflater.inflate(R.layout.unopened_dare_item, parent, false);
                unopenedDareHolder.mDareIcon = (ImageView) convertView.findViewById(R.id.unopened_dare);
                convertView.setTag(unopenedDareHolder);
            } else {
                unopenedDareHolder = (UnopenedDareHolder) convertView.getTag();
            }

            unopenedDareHolder.setUpViews();

            return convertView;
        }

    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = mInflater.inflate(R.layout.flow_places, parent, false);
            holder.placeName = (TextView) convertView.findViewById(R.id.flow_place_name);
            holder.placeAddress = (TextView) convertView.findViewById(R.id.flow_place_address);
            holder.placeImage = (ImageView) convertView.findViewById(R.id.flow_image_actual);
            holder.timeCheckedIn = (TextView) convertView.findViewById(R.id.flow_user_check_in_time);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }

        // set header text as first char in name
        DareActivity dareActivity = mActivities.get(position);
        holder.setupViews(dareActivity);

        return convertView;
    }

    /**
     * Remember that these have to be static, postion=1 should always return
     * the same Id that is.
     */
    @Override
    public long getHeaderId(int position) {
        // return the first character of the country as ID because this is what
        // headers are based upon
        return Long.parseLong(mActivities.get(position).mActivity.getParseObject("place").getObjectId(),36);
    }

    @Override
    public int getPositionForSection(int section) {
        if (mSectionIndices.length == 0) {
            return 0;
        }

        if (section >= mSectionIndices.length) {
            section = mSectionIndices.length - 1;
        } else if (section < 0) {
            section = 0;
        }
        return mSectionIndices[section];
    }

    @Override
    public int getSectionForPosition(int position) {
        for (int i = 0; i < mSectionIndices.length; i++) {
            if (position < mSectionIndices[i]) {
                return i - 1;
            }
        }
        return mSectionIndices.length - 1;
    }

    @Override
    public Object[] getSections() {
        return mSectionHeaders;
    }

    @Override
    public void onReceivePush(final Conversation conversation) {
        Log.d(TAG, "Conversation id" + conversation.getId());
        mUpdateActivities.updateActivities();
    }
    @Override
    public void updateConversation(Conversation conversation) {
       mUpdateActivities.updateActivities();

    }


    @Override
    public void updateDareCell(String objectId) {
        for (int i =0; i<mActivities.size();i++ ){
            if (mActivities.get(i).mActivity.getObjectId().equals(objectId)){
                mActivities.get(i).mActivity.addUnique("readBy", ParseUser.getCurrentUser());
                mActivities.get(i).mActivity.saveInBackground();
            }
        }

        notifyDataSetChanged();
    }


    class CheckinHolder {

    }

    class HeaderViewHolder {
        TextView placeName;
        TextView placeAddress;
        TextView timeCheckedIn;
        ImageView placeImage;

        public void setupViews(DareActivity dareActivity){
            placeName.setText(dareActivity.mActivity.getParseObject("place").getString("name"));
            placeAddress.setText(formatAddress(dareActivity.mActivity.getParseObject("place").getString("address")));
            //should not be null eveer! other wise big problem!!
            timeCheckedIn.setText(formatTime(dareActivity.checkinTime));
            mImageLoader.DisplayImage(dareActivity.mActivity.getParseObject("place").getString("placeId"),placeImage);
        }

    }
    public String formatAddress(String address){
        String[] split = address.split(",");

        StringBuilder sb = new StringBuilder("");
        sb.append(split[0]).append(",");
        if (split.length>1){
            sb.append(split[1]);
        }


        return sb.toString();
    }

    class ViewHolder {
        TextView participantName;
        ImageView icon;
        TextView lastActivityTime;
        ParseObject mActivity;
        ImageView unreadMessageCircle;

        public void setupViews(DareActivity dareActivity){
            mActivity = dareActivity.mActivity;
            String type = mActivity.getString("type");
            if (type.equals("conversation")){
                setupConversationItem(dareActivity.mConversation);
            }
        }

        /**
         * Responsible for setting up each conversation item on the list
         * @param conversation
         */
        public void setupConversationItem(Conversation conversation){
            List<ParseObject> participants = mActivity.getList("participants");
            for (ParseObject particpant:participants){
                if (!particpant.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())){

                    if (!particpant.has("name")){
                       continue;
                    }
                    participantName.setText(formatName(particpant.getString("name")));
                    participantName.setTypeface(null, Typeface.NORMAL);
                    unreadMessageCircle.setVisibility(View.GONE);
                    //check if the last item was sent by you and adjust icon here
                    Log.d(TAG, "NAME: "+particpant.getString("name")+ " Last Message By: "+conversation.getLastMessage().getSender().getId());
                    String myIdentityURI = "layer:///identities/"+ParseUser.getCurrentUser().getObjectId();

                    if (conversation.getLastMessage().getSender().getId().toString().equals(myIdentityURI)){
                        icon.setImageResource(R.drawable.chat_filled_right);
                        icon.setVisibility(View.VISIBLE);
                    }else{
                        //must be the other person
                        icon.setImageResource(R.drawable.chat_filled_left);
                        icon.setVisibility(View.VISIBLE);
                    }

                    //check if read or not
                    if (!conversation.getTotalUnreadMessageCount().equals(0)){
                        //testing for now, bold later
                        participantName.setText(formatName(particpant.getString("name")));
                        participantName.setTypeface(null, Typeface.BOLD);
                        unreadMessageCircle.setVisibility(View.VISIBLE);
                    }
                }
            }
        }

    }

    class UnopenedDareHolder {
        ImageView mDareIcon;

        public void setUpViews(){}

    }

    class OpenedDare {
        AvatarImageView mprofilePic;
        TextView name;

        public void setUpViews(DareActivity dareActivity){
            List<ParseObject> participants = dareActivity.mActivity.getList("participants");
            for (ParseObject particpant:participants){
                if (!particpant.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())){
                    name.setText(formatName(particpant.getString("name")));
                    ProfilePresenter.fetchPeopleImage(mprofilePic, (ParseUser) particpant,"profilePic",0,0,mContext);
                }
            }
        }

    }

    public static String formatName(String name){
        String[] split = name.split(" ");
        return split[0];
    }

        public static String formatTime(Date createdAt){
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat("E. MMMM dd h:mm a");

        return simpleDateFormat.format(createdAt);
    }

    public static class DareActivity{
        Conversation mConversation;
        ParseObject mActivity;
        ParseUser mCreatedBy;
        String mType;
        Date checkinTime= null;

        DareActivity(Conversation conversation, ParseObject activity, ParseUser createdBy, String type){
            mConversation = conversation;
            mActivity = activity;
            mCreatedBy = createdBy;
            mType = type;
        }



    }

    public interface UpdateActivities{
        public void updateActivities();
    }
}
