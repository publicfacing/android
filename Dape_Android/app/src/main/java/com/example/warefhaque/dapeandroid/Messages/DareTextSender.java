package com.example.warefhaque.dapeandroid.Messages;

import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.widget.Button;
import android.widget.EditText;

import com.example.warefhaque.dapeandroid.Layer.LayerImpl;
import com.layer.atlas.messagetypes.text.TextSender;
import com.layer.atlas.util.Log;
import com.layer.sdk.listeners.LayerProgressListener;
import com.layer.sdk.messaging.Conversation;
import com.layer.sdk.messaging.Identity;
import com.layer.sdk.messaging.Message;
import com.layer.sdk.messaging.MessageOptions;
import com.layer.sdk.messaging.MessagePart;
import com.layer.sdk.messaging.PushNotificationPayload;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.HashMap;

/**
 * Created by warefhaque on 3/12/17.
 */

public class DareTextSender extends TextSender {
    private int mMaxNotificationLength;
    public Conversation mConversation;
    MessagesListActivity mMessagesListActivity;
    EditText mMessageEditText;
    Button mSendButton;
    public Handler mHandler;


    public DareTextSender(Context messagesListActivity ) {
        mHandler = new Handler(Looper.getMainLooper());
        mMessagesListActivity = (MessagesListActivity) messagesListActivity;
        mMaxNotificationLength = 200;
    }

    public void requestSend(String text, final Conversation conversation, EditText messageEditText, Button sendButon) {
        mMessageEditText = messageEditText;
        mSendButton = sendButon;
        mConversation = conversation;
        if (text == null || text.trim().length() == 0) {
            if (Log.isLoggable(Log.ERROR))
                Log.e("No text to send");

        }
        if (Log.isLoggable(Log.VERBOSE)) Log.v("Sending text message");

        if (Log.isPerfLoggable()) {
            Log.perf("TextSender is attempting to send a message");
        }

        // Create notification string
        Identity me = LayerImpl.getLayerClient().getAuthenticatedUser();
        String myName = MessagesListAdapter.formatName(ParseUser.getCurrentUser().getString("name"));
        String notificationString = getContext().getString(com.layer.atlas.R.string.atlas_notification_text, myName, (text.length() < mMaxNotificationLength) ? text : (text.substring(0, mMaxNotificationLength) + "…"));

        // Send message
        MessagePart part = LayerImpl.getLayerClient().newMessagePart(text);
        PushNotificationPayload payload = new PushNotificationPayload.Builder()
                .text(notificationString)
                .build();
//        if (mMessagesListActivity.toUserPlaceId!=null && mMessagesListActivity.toUserId!=null){
//            mHandler.post(new Runnable() {
//                @Override
//                public void run() {
//                    mMessagesListActivity.setLoadingIndicator(true);
//                }
//            });
//        }
        Message message = LayerImpl.getLayerClient().newMessage(new MessageOptions().defaultPushNotificationPayload(payload), part);
        conversation.send(message, new LayerProgressListener() {
             @Override
             public void onProgressStart(MessagePart messagePart, Operation operation) {
                Log.d("PROGRESS START!");
             }

             @Override
             public void onProgressUpdate(MessagePart messagePart, Operation operation, long l) {
                 Log.d("PROGRESS UPDATE!");
             }

             @Override
             public void onProgressComplete(MessagePart messagePart, Operation operation) {

                 if (mMessagesListActivity.toUserId!=null && mMessagesListActivity.toUserPlaceId!=null){
                     HashMap<String, String > params = new HashMap<String, String>();
                     params.put("to", mMessagesListActivity.toUserId);
                     params.put("conversationID",conversation.getId().toString());
                     params.put("to_place",mMessagesListActivity.toUserPlaceId);
                     ParseCloud.callFunctionInBackground("register-chat", params, new FunctionCallback<Object>() {
                         @Override
                         public void done(Object object, ParseException e) {


                             if (e==null){
                                 android.util.Log.d("NO ERRORS", "SHOULD BE SAVED");
                             }else{
                                 android.util.Log.e("ERRORS",e.getMessage().toString());
                             }
                             mMessagesListActivity.setLoadingIndicator(false);
                             mMessagesListActivity.singleTextSent(conversation);
                             mMessageEditText.setText("");
                             mSendButton.setEnabled(false);

                         }
                     });

                 }
             }

             @Override
             public void onProgressError(MessagePart messagePart, Operation operation, Throwable throwable) {
                 Log.e("PRogress error");
             }
         });

        mMessagesListActivity.singleTextSent(conversation);
        mMessageEditText.setText("");
        mSendButton.setEnabled(false);
    }

}
