package com.example.warefhaque.dapeandroid.People;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.Database.ParseAuthentication;
import com.example.warefhaque.dapeandroid.Layer.LayerImpl;
import com.example.warefhaque.dapeandroid.LoadingDialog;
import com.example.warefhaque.dapeandroid.Messages.MessagesListActivity;
import com.example.warefhaque.dapeandroid.Messages.PushNotificationReceiver;
import com.example.warefhaque.dapeandroid.Profile.ProfilePresenter;
import com.example.warefhaque.dapeandroid.R;
import com.example.warefhaque.dapeandroid.Utils.AvatarImageView;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.layer.sdk.messaging.Conversation;
import com.layer.sdk.messaging.ConversationOptions;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.joda.time.DateTime;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by warefhaque on 1/16/17.
 */

public class DetailedPeopleItem {
    public static final String TAG = "Detailed People Item";
    AvatarImageView mProfilePicture;
    TextView mAboutMe;
    TextView mAboutMeTitle;
    TextView mName;
    TextView mContactTitle;
    TextView mMutualFriends;
    ImageView messageButton;
    Activity mActivity;
    ImageView mMutualFriendsIcon;
    ImageView mProfileLarge;
    Button mDareButton;
    ParseUser mProfileUser;
   Context mContext;
    public static Conversation mNewConversation = null;
    private Handler mHandler;
    public ParseObject mUserCheckedInPlace;

    public LoadingDialog mLoadingDialog;

    //fetch the user based on the id and then populate the fragment
    public View DetailedPeopleItem(LayoutInflater inflater, ViewGroup container, final String userId, final Context context){
        final View view =  inflater.inflate(R.layout.user_profile, container, false);
        mNewConversation = null;
        mContext = context;
        mProfileLarge = (ImageView) view.findViewById(R.id.profilebg);
        mProfilePicture = (AvatarImageView) view.findViewById(R.id.profile_displ_pic);
        mAboutMe = (TextView) view.findViewById(R.id.profile_about_me);
        mName = (TextView) view.findViewById(R.id.profile_name);
        mAboutMeTitle = (TextView) view.findViewById(R.id.about_user_title);
        mContactTitle = (TextView) view.findViewById(R.id.reach_out_title);
        mMutualFriends = (TextView) view.findViewById(R.id.mutual_friends);
        mMutualFriendsIcon = (ImageView) view.findViewById(R.id.mutual_friends_icon);
        messageButton = (ImageView) view.findViewById(R.id.message_button);
        mDareButton = (Button) view.findViewById(R.id.dare_button);
        mActivity = (Activity) context;
        mHandler = new Handler(Looper.getMainLooper());

        mDareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                loadingIndicator(true);
                sendDare();
            }
        });

        messageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                createNewConversation();
            }
        });
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("objectId", userId);
        query.include("checkedIn");
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(final ParseUser object, ParseException e) {
                mProfileUser = object;
                mUserCheckedInPlace = object.getParseObject("checkedIn");
                String name  = object.getString("name");
                String[] nameArray = name.split(" ");
                mContactTitle.setText("Reach out to "+nameArray[0]+"!");
                mAboutMe.setText(object.getString("about"));
                mAboutMeTitle.setText("About "+ nameArray[0]);
                ProfilePresenter.fetchUserProfileImage(mProfileLarge, object, "profilePic",140,140, context);
                showSocialMediaEntitles(view, object);
                fetchMutualFriends(object, mMutualFriends);


            }
        });

        // TODO: 4/28/17 not t
        mProfileLarge.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(mContext, FullScreenImageProfile.class);
                intent.putExtra("userId", mProfileUser.getObjectId());
                // only one large picture showing
                intent.putExtra("position", 1);
                mContext.startActivity(intent);
            }
        });
        return  view;
    }

    public void createNewConversation() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                List<String> participantIdList = new ArrayList<>();
                Date currDate= new Date();
                Date daysAgo = new DateTime(currDate).minusHours(72).toDate();
                participantIdList.add(ParseUser.getCurrentUser().getObjectId());
                participantIdList.add(mProfileUser.getObjectId());
//                String[] participantIdsArray = participantIdList.toArray(new String[participantIdList.size()]);
                ParseQuery<ParseObject> query = ParseQuery.getQuery("UserActivity");
                query.whereEqualTo("type","conversation");
                query.whereContainsAll("participantsIDs",participantIdList);
                query.whereGreaterThan("createdAt",daysAgo);
                try {
                    final List<ParseObject> parseObjectList = query.find();
                    if (parseObjectList!=null && parseObjectList.size()>0){

                        mHandler.post(new Runnable() {
                            @Override
                            public void run() {
                                loadingIndicator(false);
                                Uri conversationUri = Uri.parse(parseObjectList.get(0).getString("cid"));
                                Intent intent = new Intent(mActivity, MessagesListActivity.class);
                                intent.putExtra(PushNotificationReceiver.LAYER_CONVERSATION_KEY, conversationUri);
                                intent.putExtra("participantName",mProfileUser.getString("name"));
                                mActivity.startActivity(intent);
                            }
                        });


                    }else{

                        Set<String> parts = new HashSet<>();
                        parts.add(ParseUser.getCurrentUser().getObjectId());
                        parts.add(mProfileUser.getObjectId());
                        ConversationOptions conversationOptions= new ConversationOptions();
                        conversationOptions.distinct(false);
                        mNewConversation  = LayerImpl.getLayerClient().newConversationWithUserIds(conversationOptions,parts);

                        mHandler.post(new Runnable() {

                            @Override
                            public void run() {

                                if (mNewConversation!=null){
                                    Intent intent = new Intent(mActivity, MessagesListActivity.class);
                                    intent.putExtra(MessagesListActivity.NEW_CONVO,true);
                                    intent.putExtra("place_id", mUserCheckedInPlace.getObjectId());
                                    intent.putExtra("user_id",mProfileUser.getObjectId());
                                    intent.putExtra("participantName",mProfileUser.getString("name"));
                                    mActivity.startActivity(intent);
                                }else{
                                    Log.e(TAG, "Layer returned with no convo");
                                }
                            }
                        });

                    }
                } catch (ParseException e) {
                    e.printStackTrace();
                }

            }
        };
        Thread thread = new Thread(runnable);
        thread.start();

    }

    public void sendDare(){
        try {
            mProfileUser.fetch();
            ParseObject userCheckedInPlace = mProfileUser.getParseObject("checkedIn");

            if (userCheckedInPlace!=null){
                userCheckedInPlace.fetch();
                String userPlaceId = userCheckedInPlace.getObjectId();

                HashMap<String,String> params = new HashMap<>();
                params.put("to",mProfileUser.getObjectId());
                params.put("place",userPlaceId);

                ParseCloud.callFunctionInBackground("dare", params, new FunctionCallback<Object>() {
                    @Override
                    public void done(Object object, ParseException e) {
                        loadingIndicator(false);
                        if (e==null){
                            String[] nameArray = mProfileUser.getString("name").split(" ");
                            Toast.makeText(mActivity, "You've daped "+nameArray[0]+"!", Toast.LENGTH_SHORT).show();
                            FragmentStatePagerSupport.dareCompleted.updateFlowList(mProfileUser.getString("name"));

                        }else{
                            Toast.makeText(mActivity, "Error: "+e.toString(), Toast.LENGTH_SHORT).show();
                            FragmentStatePagerSupport.dareCompleted.updateFlowList(mProfileUser.getString("name"));
                        }
                    }
                });
            }else{
                Log.e(TAG, "USer not checked in anywhere");
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    public void showSocialMediaEntitles(View view, ParseUser user){

        List<UserProfileSocialItem> items = getSocialItems(user);

        if (items!=null || items.size()>=0){

            RelativeLayout relativeLayout = (RelativeLayout) view.findViewById(R.id.social_items_layout);

            for (int i = 0; i<items.size(); i++){
                if (!items.get(i).mAccount.equals("")){
                    RelativeLayout item = (RelativeLayout) relativeLayout.getChildAt(i);
                    item.setVisibility(View.VISIBLE);
                    ImageView icon = (ImageView) item.getChildAt(0);
                    TextView account = (TextView) item.getChildAt(1);
                    icon.setImageResource(items.get(i).mResId);
                    account.setText(items.get(i).mAccount);
                }
            }
        }

    }

    public class UserProfileSocialItem{
        int mResId;
        String mAccount;

        UserProfileSocialItem(int resId, String account){
            mResId = resId;
            mAccount = account;
        }
    }

    List<UserProfileSocialItem> getSocialItems(ParseUser currUser){

        List<String> accounts = new ArrayList<>();
        List<UserProfileSocialItem>result = new ArrayList<>();
        if (currUser.getString("instagram")!=null)
            accounts.add(currUser.getString("instagram"));

        if (currUser.getString("twitter")!=null)
            accounts.add(currUser.getString("twitter"));

        if (currUser.getString("snapchat")!=null)
            accounts.add(currUser.getString("snapchat"));

        if (currUser.getString("whatsapp")!=null)
            accounts.add(currUser.getString("whatsapp"));

        if (currUser.getString("otherSM")!=null)
            accounts.add(currUser.getString("otherSM"));


        List<String> publicEntitleMents = currUser.getList("publicEntitlements");
        List<String> currSocialMedia = currUser.getList("currentSocialMedia");

        if (publicEntitleMents!=null &&  currSocialMedia!=null){
            for (int i = 0; i<currSocialMedia.size();i++){
                for (int j= 0; j< publicEntitleMents.size(); j++){
                    if (currSocialMedia.get(i).equals(publicEntitleMents.get(j))){
                        int resId = 0;
                        if (publicEntitleMents.get(j).equals(ParseAuthentication.INSTAGRAM))
                            resId = R.drawable.instagram_logo_profile;
                        if (publicEntitleMents.get(j).equals(ParseAuthentication.TWITTER))
                            resId = R.drawable.twitter_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.SNAPCHAT))
                            resId = R.drawable.snapchat_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.WHATSAPP))
                            resId = R.drawable.whatsapp_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.DEFAULT))
                            resId = R.drawable.hashtag_filled;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.PINTEREST))
                            resId = R.drawable.pinterest_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.LINKEDIN))
                            resId = R.drawable.linked_in_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.TUMBLR))
                            resId = R.drawable.tumblr_logo_profile;
                        if (publicEntitleMents.get(j).equals(ParseAuthentication.FACEBOOK))
                            resId = R.drawable.facebook_logo_profile;
                        // TODO: 3/8/17 come back later for this
                        if (accounts.size()>i){
                            result.add(new UserProfileSocialItem(resId,accounts.get(i)));
                        }

                    }
                }
            }

        }


        return result;
    }

    void fetchMutualFriends(ParseUser user, final TextView textView) {
        Bundle params = new Bundle();
        params.putString("fields", "context.fields(mutual_friends)");
        StringBuilder sb = new StringBuilder("/");
        Log.d(TAG, sb.toString());
        sb.append(user.getString("facebookID"));
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                sb.toString(),
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        /* handle the result */
                        JSONObject friends = response.getJSONObject();
                        if (friends != null) {
                            Log.d(TAG, friends.toString());
                            try {
                                int mutualFriendCount = friends.getJSONObject("context").getJSONObject("mutual_friends").getJSONObject("summary").getInt("total_count");
                                textView.setText("You have " + mutualFriendCount + " mutual friends");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        } else {
                            Log.d(TAG, "friends is null");
                        }
                    }
                }
        ).executeAsync();
    }

    public void loadingIndicator(boolean active) {
        if (mLoadingDialog==null){
            mLoadingDialog=new LoadingDialog(mActivity);
        }
        if (active){
            mLoadingDialog.show();
        }else{
            mLoadingDialog.dismiss();
        }
    }



}
