package com.example.warefhaque.dapeandroid.People;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.warefhaque.dapeandroid.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warefhaque on 4/28/17.
 */

public class FullScreenImageProfile extends FragmentActivity{
    public static final String TAG = "Full screen image";
    static int NUM_ITEMS = 3;
    static List<String> uris;
    ImageButton mBackButton;

    MyAdapter mAdapter;

    ViewPager mPager;

    static String mUserId;

    int mpositionClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_full_screen);

//        mBackButton = (ImageButton) findViewById(R.id.btnClose);
//        Picasso.with(getApplicationContext())
//                .load(R.drawable.back_arrow)
//                .fit()
//                .into(mBackButton);
//        mBackButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                Log.d(TAG, "clicked Back");
//                finish();
//            }
//        });


        uris = new ArrayList<>();
        mUserId = getIntent().getStringExtra("userId");
        mpositionClicked = getIntent().getIntExtra("position", 0);

        mAdapter = new MyAdapter(getSupportFragmentManager());

        mPager = (ViewPager)findViewById(R.id.image_pager);
        mPager.setAdapter(mAdapter);

        //set the position to the item in the list clicked
        mPager.setCurrentItem(mpositionClicked);

    }

    public static class MyAdapter extends FragmentStatePagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            return ArrayListFragment.newInstance(position);
        }
    }

    public static class ArrayListFragment extends Fragment {
        int pictureNumber;
        ImageView mFullScreenImage;


        /**
         * Create a new instance of CountingFragment, providing "num"
         * as an argument.
         */
        static ArrayListFragment newInstance(int picNum) {
           ArrayListFragment f = new ArrayListFragment();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putInt("picNum", picNum);
            f.setArguments(args);

            return f;
        }

        /**
         * When creating, retrieve this instance's number from its arguments.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            pictureNumber = getArguments() != null ? getArguments().getInt("picNum"): 0;
        }

        /**
         * The Fragment's UI is just a simple text view showing its
         * instance number.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.fragment_full_screen_image, container, false);

            mFullScreenImage = (ImageView) view.findViewById(R.id.imgDisplay);

            ParseQuery<ParseUser> query = ParseUser.getQuery();
            query.whereEqualTo("objectId", mUserId);
            query.getFirstInBackground(new GetCallback<ParseUser>() {
                @Override
                public void done(ParseUser object, ParseException e) {

                    ParseFile file;

                    if (pictureNumber == 0)
                    {
                       file = object.getParseFile("leftPic");
                    }
                    else if (pictureNumber == 1)
                    {
                        file = object.getParseFile("profilePic");
                    }
                    else
                    {
                        file = object.getParseFile("rightPic");
                    }

                    if (file !=null)
                    {
                        String pictureUrl = file.getUrl();
                        Picasso.with(getActivity())
                                .load(pictureUrl)
                                .resize(400,400)
                                .centerCrop()
                                .onlyScaleDown()
                                .placeholder(R.drawable.placeholder_profile)
                                .into(mFullScreenImage);
                    }

                }
            });

            return view;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

        }
    }
}
