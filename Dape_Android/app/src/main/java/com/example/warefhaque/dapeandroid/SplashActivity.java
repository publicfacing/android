package com.example.warefhaque.dapeandroid;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.Authentication.LoginActivity;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

/**
 * Created by warefhaque on 3/6/17.
 */

public class SplashActivity extends AppCompatActivity{

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        if (!isNetworkAvailable(this)){
            Toast.makeText(this, "Network Error: Please connect to the internet and open the app again", Toast.LENGTH_SHORT).show();
        }
        if (ParseUser.getCurrentUser()!=null){
            ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                   if (e==null){
                       Intent intent = new Intent(SplashActivity.this, LauncherActivity.class);
                       startActivity(intent);
                       finish();
                   }else {
                       if (e.getCode()==209){
                           Toast.makeText(SplashActivity.this, "Your session token has expired Please login again", Toast.LENGTH_SHORT).show();
                           Intent intent = new Intent(SplashActivity.this, LoginActivity.class);
                           startActivity(intent);
                       }
                   }
                }
            });

        }else{
            Intent intent = new Intent(this, LoginActivity.class);
            startActivity(intent);
            finish();
        }

    }



    private boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) this.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null;
    }

}
