package com.example.warefhaque.dapeandroid.Messages;

import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.warefhaque.dapeandroid.Database.ParseAuthentication;
import com.example.warefhaque.dapeandroid.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warefhaque on 3/7/17.
 */

public class DareFragment extends Fragment {

    ImageView mImageView;
    TextView mDareName;
    ImageView mMoreLessIcon;
    FloatingActionButton mShowDareInfo;
    FloatingActionButton mShowLessInfo;

    CardView mDareCard;

//    RelativeLayout mDareCard;
    public static DareFragment newInstance(){
        return new DareFragment();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.activity_dare,container,false);
        SlidingUpPanelLayout layout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);
        layout.setAnchorPoint(0.4f);

        mImageView = (ImageView) view.findViewById(R.id.dare_image);
        mDareName = (TextView) view.findViewById(R.id.dare_title);
        mMoreLessIcon = (ImageView) view.findViewById(R.id.more_less_icon);

        layout.addPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
                if (slideOffset>0.0f){
                    mMoreLessIcon.setImageResource(R.drawable.less_arrow_purple);
                }
                else{
                    mMoreLessIcon.setImageResource(R.drawable.more_arrow_blue);
                }
            }

            @Override
            public void onPanelStateChanged(View panel, SlidingUpPanelLayout.PanelState previousState, SlidingUpPanelLayout.PanelState newState) {

            }
        });
//        mShowDareInfo = (FloatingActionButton) view.findViewById(R.id.show_dare_info);
//        mShowLessInfo = (FloatingActionButton) view.findViewById(R.id.hide_dare_info);
//        mDareCard = (CardView) view.findViewById(R.id.dare_card);
//        mShowDareInfo.setVisibility(View.VISIBLE);
//        mShowLessInfo.setVisibility(View.GONE);
//        mShowDareInfo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                    mDareCard.setVisibility(View.VISIBLE);
//                    mShowLessInfo.setVisibility(View.VISIBLE);
//                    mShowDareInfo.setVisibility(View.GONE);
//            }
//        });
//
//        mShowLessInfo.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                mDareCard.setVisibility(View.GONE);
//                mShowDareInfo.setVisibility(View.VISIBLE);
//                mShowLessInfo.setVisibility(View.GONE);
//            }
//        });
        DareInterfaceActivity dareInterfaceActivity = (DareInterfaceActivity) getActivity();
        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("objectId",dareInterfaceActivity.mParticipantId);
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(final ParseUser object, ParseException e) {
                mDareName.setText(MessagesListAdapter.formatName(object.getString("name")));
                ParseFile file = object.getParseFile("profilePic");
                if (file!=null) {
                    String pictureUrl = file.getUrl();
                    Uri fileUri = Uri.parse(pictureUrl);
                    Picasso.with(getActivity()).load(fileUri.toString())
                            .fit()
                            .centerCrop()
                            .into(mImageView);
                    showSocialMediaEntitles(view, object);
                }else{
                    Log.e("PROFILE PIC NULL!", "DARE!");
                }

            }
        });

        return view;
    }
    public void showSocialMediaEntitles(View view, ParseUser user){

        List<UserProfileSocialItem> items = getSocialItems(user);
        SlidingUpPanelLayout slidingUpPanelLayout = (SlidingUpPanelLayout) view.findViewById(R.id.sliding_layout);


        if (items!=null || items.size()>=0){

            RelativeLayout relativeLayout = (RelativeLayout) slidingUpPanelLayout.findViewById(R.id.dare_items_layout);

            for (int i = 0; i<items.size(); i++){
                RelativeLayout item = (RelativeLayout) relativeLayout.getChildAt(i);
                item.setVisibility(View.VISIBLE);
                ImageView icon = (ImageView) item.getChildAt(0);
                TextView account = (TextView) item.getChildAt(1);
                icon.setImageResource(items.get(i).mResId);
                account.setText(items.get(i).mAccount);
            }
        }

    }
    public class UserProfileSocialItem{
        int mResId;
        String mAccount;

        UserProfileSocialItem(int resId, String account){
            mResId = resId;
            mAccount = account;
        }
    }

    List<UserProfileSocialItem> getSocialItems(ParseUser currUser){

        List<String> accounts = new ArrayList<>();
        List<UserProfileSocialItem>result = new ArrayList<>();
        if (currUser.getString("instagram")!=null)
            accounts.add(currUser.getString("instagram"));

        if (currUser.getString("twitter")!=null)
            accounts.add(currUser.getString("twitter"));

        if (currUser.getString("snapchat")!=null)
            accounts.add(currUser.getString("snapchat"));

        if (currUser.getString("whatsapp")!=null)
            accounts.add(currUser.getString("whatsapp"));

        if (currUser.getString("otherSM")!=null)
            accounts.add(currUser.getString("otherSM"));


        List<String> publicEntitleMents = currUser.getList("dareEntitlements");
        List<String> currSocialMedia = currUser.getList("currentSocialMedia");

        if (publicEntitleMents!=null &&  currSocialMedia!=null){
            for (int i = 0; i<currSocialMedia.size();i++){
                for (int j= 0; j< publicEntitleMents.size(); j++){
                    if (currSocialMedia.get(i).equals(publicEntitleMents.get(j))){
                        int resId = 0;
                        if (publicEntitleMents.get(j).equals(ParseAuthentication.INSTAGRAM))
                            resId = R.drawable.instagram_logo_profile;
                        if (publicEntitleMents.get(j).equals(ParseAuthentication.TWITTER))
                            resId = R.drawable.twitter_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.SNAPCHAT))
                            resId = R.drawable.snapchat_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.WHATSAPP))
                            resId = R.drawable.whatsapp_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.DEFAULT))
                            resId = R.drawable.placeholder_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.PINTEREST))
                            resId = R.drawable.pinterest_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.LINKEDIN))
                            resId = R.drawable.linked_in_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.TUMBLR))
                            resId = R.drawable.tumblr_logo_profile;
                        if (publicEntitleMents.get(j).equals(ParseAuthentication.FACEBOOK))
                            resId = R.drawable.facebook_logo_profile;

                        result.add(new UserProfileSocialItem(resId,accounts.get(i)));
                    }
                }
            }

        }
        return result;
    }
}
