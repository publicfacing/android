package com.example.warefhaque.dapeandroid.Authentication;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.LauncherActivity;
import com.example.warefhaque.dapeandroid.LoadingDialog;
import com.example.warefhaque.dapeandroid.LoginSignupActivity;
import com.example.warefhaque.dapeandroid.R;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by warefhaque on 12/14/16.
 */

public class LoginFragment extends android.support.v4.app.Fragment implements LoginInterface.View {
    private LoginInterface.Presenter mPresenter;
    private LoadingDialog mLoadingDialog;
    public TextView welcomeText;
    public TextView subtext;
    //// TODO: 12/18/16 the login screen does not scroll on horizontal view
    public LoginFragment(){
        //empty compulsory constructor
    }

    public static LoginFragment newInstance(){
        return new LoginFragment();
    }
    @Override
    public void onResume(){
        super.onResume();
    }
    @Override
    public void onActivityCreated(Bundle savedInstanceState){
        super.onActivityCreated(savedInstanceState);
    }
    @Nullable
    @Override
    public View onCreateView (LayoutInflater inflater, ViewGroup container,
                              Bundle savedInstanceStat){

        View root = inflater.inflate(R.layout.fragment_login, container, false);
        Button mFbLoginButton = (Button) root.findViewById(R.id.btn_fb_login);

        Typeface custom_font = Typeface.createFromAsset(getActivity().getAssets(), "fonts/RockoFLF.ttf");

        TextView nothingPosted = (TextView) root.findViewById(R.id.nothing_posted);
//        nothingPosted.setTypeface(custom_font);

        TextView privacyAndTermsOfUse = (TextView) root.findViewById(R.id.privacy_and_terms_of_use);
//        privacyAndTermsOfUse.setTypeface(custom_font);

        welcomeText = (TextView) root.findViewById(R.id.welcome);
        welcomeText.setTypeface(custom_font);

        TextView testLogin = (TextView) root.findViewById(R.id.test_login);
        testLogin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getActivity(), LoginSignupActivity.class);
                startActivity(intent);
            }
        });
//        subtext = (TextView) root.findViewById(R.id.click_away);
//        subtext.setTypeface(custom_font);

        TextView checkInText = (TextView) root.findViewById(R.id.check_in_text);
        checkInText.setTypeface(custom_font);
        TextView browseText = (TextView) root.findViewById(R.id.browse_text);
        browseText.setTypeface(custom_font);
        TextView dareText = (TextView) root.findViewById(R.id.dare_text);
        dareText.setTypeface(custom_font);




        mFbLoginButton.setTypeface(custom_font);
        mFbLoginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mPresenter.facebookLogin(getActivity());
            }
        });

        SpannableString ss = new SpannableString("By signing in you agree to our Privacy Policies and Terms of Use");
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
//                Intent intent = new Intent(getApplicationContext(), DisplayPrivacyActivity_.class);
//                startActivity(intent);
            }
        };
        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View widget) {
//                Intent intent = new Intent(getApplicationContext(), DisplayTermsOfUseActivity_.class);
//                startActivity(intent);

            }
        };
        ss.setSpan(span1, 31, 47, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span2, 52, 64, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        privacyAndTermsOfUse.setText(ss);
        privacyAndTermsOfUse.setMovementMethod(LinkMovementMethod.getInstance());

        return root;
    }

    @Override
    public void facebookSignupComplete(String name) {
        Log.d("facebookSignupComplete", "IN the fragment---SUCCESS");

        Toast.makeText(getActivity(), "New user:" + name + " Signed up", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(getActivity(), LauncherActivity.class);
        intent.putExtra("first_time", true);
        startActivity(intent);

    }

    @Override
    public void facebookLoginComplete(String name) {
        String[] nameArray = name.split(" ");
        if (nameArray.length>0){
            Toast.makeText(getActivity(), "Welcome back " + nameArray[0] + "!", Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getActivity(), LauncherActivity.class);
            intent.putExtra("first_time", false);
            startActivity(intent);
        }
    }

    @Override
    public void facebookLoginIncomplete(String error) {
        Log.d("Facebook login failed","In the fragment --- FAILURE");

        Toast.makeText(getActivity(), "Failure:" +  error +". Please try again.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void facebookSignupIncomplete(String error) {
        Log.d("facebookSignupFailed","In the fragment --- FAILURE");

        Toast.makeText(getActivity(), "Failure:" +  error +". Please try again.", Toast.LENGTH_LONG).show();

    }

    @Override
    public void setLoadingIndicator(boolean active) {
        if (mLoadingDialog==null){
            mLoadingDialog=new LoadingDialog(getActivity());
        }
        if (active){
            mLoadingDialog.show();
        }else{
            mLoadingDialog.dismiss();
        }
    }

    @Override
    public void setPresenter(LoginInterface.Presenter presenter) {
        mPresenter = presenter;
    }
}
