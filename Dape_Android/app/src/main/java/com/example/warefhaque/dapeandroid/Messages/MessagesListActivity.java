package com.example.warefhaque.dapeandroid.Messages;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.warefhaque.dapeandroid.BaseActivity;
import com.example.warefhaque.dapeandroid.LoadingDialog;
import com.example.warefhaque.dapeandroid.People.DetailedPeopleItem;
import com.example.warefhaque.dapeandroid.R;
import com.layer.atlas.AtlasAddressBar;
import com.layer.atlas.AtlasHistoricMessagesFetchLayout;
import com.layer.atlas.AtlasMessagesRecyclerView;
import com.layer.atlas.AtlasTypingIndicator;
import com.layer.atlas.messagetypes.generic.GenericCellFactory;
import com.layer.atlas.messagetypes.location.LocationCellFactory;
import com.layer.atlas.messagetypes.location.LocationSender;
import com.layer.atlas.messagetypes.singlepartimage.SinglePartImageCellFactory;
import com.layer.atlas.messagetypes.text.TextCellFactory;
import com.layer.atlas.messagetypes.threepartimage.CameraSender;
import com.layer.atlas.messagetypes.threepartimage.GallerySender;
import com.layer.atlas.messagetypes.threepartimage.ThreePartImageCellFactory;
import com.layer.atlas.typingindicators.BubbleTypingIndicatorFactory;
import com.layer.atlas.util.views.SwipeableItem;
import com.layer.sdk.LayerClient;
import com.layer.sdk.changes.LayerChange;
import com.layer.sdk.changes.LayerChangeEvent;
import com.layer.sdk.listeners.LayerChangeEventListener;
import com.layer.sdk.messaging.Conversation;
import com.layer.sdk.messaging.Identity;
import com.layer.sdk.messaging.LayerObject;
import com.layer.sdk.messaging.Message;
import com.parse.ParseUser;

public class MessagesListActivity extends BaseActivity{
    public static final String TAG = "Messages List Activity";
    private UiState mState;
    private Conversation mConversation;
    public static final String NEW_CONVO = "new_conversation";
    public static final String REPLY_MODE = "reply_mode";
    public static final String SINGLE_TEXT_SENT = "single_text_sent";
    private AtlasAddressBar mAddressBar;
    private AtlasHistoricMessagesFetchLayout mHistoricFetchLayout;
    private AtlasMessagesRecyclerView mMessagesList;
    private AtlasTypingIndicator mTypingIndicator;
    private DareMessageComposer mMessageComposer;
    private IdentityChangeListener mIdentityChangeListener;
    private String mParticipantName;
    public String mFirstName;
    public String toUserId = null;
    TextView SingleText;
    String toUserPlaceId = null;
    Conversation mNewConversation;
    LoadingDialog mLoadingDialog;
    public  static UpdateConversationListener mUpdateConversationListener;
    RelativeLayout mNewConvoLayout;

    public MessagesListActivity() {
        super(R.layout.activity_messages, R.menu.view_pager_menu, R.string.title_select_conversation, true);
    }

    private void setUiState(UiState state) {
        if (mState == state) return;
        mState = state;
        switch (state) {
//            case ADDRESS:
//                mAddressBar.setVisibility(View.VISIBLE);
//                mAddressBar.setSuggestionsVisibility(View.VISIBLE);
//                mHistoricFetchLayout.setVisibility(View.GONE);
//                mMessageComposer.setVisibility(View.GONE);
//                break;
//
//            case ADDRESS_COMPOSER:
//                mAddressBar.setVisibility(View.VISIBLE);
//                mAddressBar.setSuggestionsVisibility(View.VISIBLE);
//                mHistoricFetchLayout.setVisibility(View.GONE);
//                mMessageComposer.setVisibility(View.VISIBLE);
//                break;
//
//            case ADDRESS_CONVERSATION_COMPOSER:
//                mAddressBar.setVisibility(View.VISIBLE);
//                mAddressBar.setSuggestionsVisibility(View.GONE);
//                mHistoricFetchLayout.setVisibility(View.VISIBLE);
//                mMessageComposer.setVisibility(View.VISIBLE);
//                break;

            case CONVERSATION_COMPOSER:
                //doesnt matter what the single text textview  is
                mHistoricFetchLayout.setVisibility(View.VISIBLE);
                mMessageComposer.setVisibility(View.VISIBLE);
                mNewConvoLayout.setVisibility(View.VISIBLE);
                break;
            case SINGLE_TEXT_SENT:
                //doesnt matter what the single text textview is
                mHistoricFetchLayout.setVisibility(View.VISIBLE);
                mMessageComposer.setVisibility(View.GONE);
                mNewConvoLayout.setVisibility(View.GONE);
                break;
            case NEW_CONVERSATION:
                mHistoricFetchLayout.setVisibility(View.GONE);
                mNewConvoLayout.setVisibility(View.VISIBLE);
                mMessageComposer.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        mUpdateConversationListener.updateConversation(mConversation);

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        mAddressBar = ((AtlasAddressBar) findViewById(R.id.conversation_launcher))
//                .init(getLayerClient(), getPicasso())
//                .setOnConversationClickListener(new AtlasAddressBar.OnConversationClickListener() {
//                    @Override
//                    public void onConversationClick(AtlasAddressBar addressBar, Conversation conversation) {
//                        setConversation(conversation, true);
//                        setTitle(true);
//                    }
//                })
//                .setOnParticipantSelectionChangeListener(new AtlasAddressBar.OnParticipantSelectionChangeListener() {
//                    @Override
//                    public void onParticipantSelectionChanged(AtlasAddressBar addressBar, final List<Identity> participants) {
//                        if (participants.isEmpty()) {
//                            setConversation(null, false);
//                            return;
//                        }
//                        try {
//                            setConversation(getLayerClient().newConversation(new ConversationOptions().distinct(true), new HashSet<>(participants)), false);
//                        } catch (LayerConversationException e) {
//                            setConversation(e.getConversation(), false);
//                        }
//                    }
//                })
//                .addTextChangedListener(new TextWatcher() {
//                    @Override
//                    public void beforeTextChanged(CharSequence s, int start, int count, int after) {
//
//                    }
//
//                    @Override
//                    public void onTextChanged(CharSequence s, int start, int before, int count) {
//
//                    }
//
//                    @Override
//                    public void afterTextChanged(Editable s) {
//                        if (mState == UiState.ADDRESS_CONVERSATION_COMPOSER) {
//                            mAddressBar.setSuggestionsVisibility(s.toString().isEmpty() ? View.GONE : View.VISIBLE);
//                        }
//                    }
//                })
//                .setOnEditorActionListener(new TextView.OnEditorActionListener() {
//                    @Override
//                    public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
//                        if (actionId == EditorInfo.IME_ACTION_DONE || event.getKeyCode() == KeyEvent.KEYCODE_ENTER) {
//                            setUiState(UiState.CONVERSATION_COMPOSER);
//                            setTitle(true);
//                            return true;
//                        }
//                        return false;
//                    }
//                });
        SingleText = (TextView) findViewById(R.id.single_text_message);
        toUserId = null;
        toUserPlaceId = null;
        if (getIntent().hasExtra("participantName")){
            mParticipantName = getIntent().getStringExtra("participantName");
            SingleText.setText("You can only text " + mParticipantName.split(" ")[0]+ " once!");
        }
        if (getIntent().hasExtra("place_id")){
            toUserPlaceId = getIntent().getStringExtra("place_id");
        }
        if (getIntent().hasExtra("user_id")){
            toUserId = getIntent().getStringExtra("user_id");
        }

        mNewConvoLayout = (RelativeLayout) findViewById(R.id.new_conversation_layout);
        mHistoricFetchLayout = ((AtlasHistoricMessagesFetchLayout) findViewById(R.id.historic_sync_layout))
                .init(getLayerClient())
                .setHistoricMessagesPerFetch(20);

        mMessagesList = ((AtlasMessagesRecyclerView) findViewById(R.id.messages_list))
                .init(getLayerClient(), getPicasso())
                .addCellFactories(
                        new TextCellFactory(),
                        new ThreePartImageCellFactory(this, getLayerClient(), getPicasso()),
                        new LocationCellFactory(this, getPicasso()),
                        new SinglePartImageCellFactory(this, getLayerClient(), getPicasso()),
                        new GenericCellFactory())
                .setOnMessageSwipeListener(new SwipeableItem.OnSwipeListener<Message>() {
                    @Override
                    public void onSwipe(final Message message, int direction) {
                        AlertDialog.Builder builder = new AlertDialog.Builder(MessagesListActivity.this)
                                .setMessage(R.string.alert_message_delete_message)
                                .setNegativeButton(R.string.alert_button_cancel, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        // TODO: simply update this one message
                                        mMessagesList.getAdapter().notifyDataSetChanged();
                                        dialog.dismiss();
                                    }
                                })

                                .setPositiveButton(R.string.alert_button_delete_all_participants, new DialogInterface.OnClickListener() {
                                    @Override
                                    public void onClick(DialogInterface dialog, int which) {
                                        message.delete(LayerClient.DeletionMode.ALL_PARTICIPANTS);
                                    }
                                });
                        // User delete is only available if read receipts are enabled
                        if (message.getConversation().isReadReceiptsEnabled()) {
                            builder.setNeutralButton(R.string.alert_button_delete_my_devices, new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialog, int which) {
                                    message.delete(LayerClient.DeletionMode.ALL_MY_DEVICES);
                                }
                            });
                        }
                        builder.show();
                    }
                });

        mTypingIndicator = new AtlasTypingIndicator(this)
                .init(getLayerClient())
                .setTypingIndicatorFactory(new BubbleTypingIndicatorFactory())
                .setTypingActivityListener(new AtlasTypingIndicator.TypingActivityListener() {
                    @Override
                    public void onTypingActivityChange(AtlasTypingIndicator typingIndicator, boolean active) {
                        mMessagesList.setFooterView(active ? typingIndicator : null);
                    }
                });

        mMessageComposer = (DareMessageComposer) ((DareMessageComposer) findViewById(R.id.message_composer)).init(getLayerClient())
                .setTextSender(new DareTextSender(this))
                .addAttachmentSenders(
                        new CameraSender(R.string.attachment_menu_camera,
                                R.drawable.ic_photo_camera_white_24dp, this,
                                getApplicationContext().getPackageName() + ".file_provider"),
                        new GallerySender(R.string.attachment_menu_gallery, R.drawable.ic_photo_white_24dp, this),
                        new LocationSender(R.string.attachment_menu_location, R.drawable.ic_place_white_24dp, this))
                .setOnMessageEditTextFocusChangeListener(new View.OnFocusChangeListener() {
                    @Override
                    public void onFocusChange(View v, boolean hasFocus) {
                        if (hasFocus) {
////                            setUiState(UiState.CONVERSATION_COMPOSER);
//                            setTitle(true);
                        }
                    }
                });;

    mMessageComposer.setMessagesListActivity(this);
        if (getIntent().hasExtra(NEW_CONVO)){
            if (DetailedPeopleItem.mNewConversation!=null){
                setConversation(DetailedPeopleItem.mNewConversation, NEW_CONVO);
                DetailedPeopleItem.mNewConversation=null;
            }

        }else{
            // Get or create Conversation from Intent extras
            Conversation conversation = null;
            Intent intent = getIntent();
            if (intent != null) {
                if (intent.hasExtra(PushNotificationReceiver.LAYER_CONVERSATION_KEY)) {

                    Uri conversationId = intent.getParcelableExtra(PushNotificationReceiver.LAYER_CONVERSATION_KEY);
                    conversation = getLayerClient().getConversation(conversationId);
                    String myIdentityURI = "layer:///identities/"+ParseUser.getCurrentUser().getObjectId();
                    if (conversation.
                            getLastMessage()
                            .getSender()
                            .getId()
                            .toString()
                            .equals(myIdentityURI) || conversation.getTotalMessageCount()>=2){

                        setConversation(conversation,SINGLE_TEXT_SENT);
                    }
                    else{
                        setConversation(conversation,REPLY_MODE);
                    }

                }
            }
        }

      setUpActionBar(mParticipantName);

    }

    @Override
    protected void onResume() {
        // Clear any notifications for this conversation
        PushNotificationReceiver.getNotifications(this).clear(mConversation);
        super.onResume();
        setTitle(mConversation != null);

        // Register for identity changes and update the activity's title as needed
        mIdentityChangeListener = new IdentityChangeListener();
        getLayerClient().registerEventListener(mIdentityChangeListener);
    }

    public void setUpActionBar(String name){

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.messages_list_activity_tab_layout);
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/RockoFLF.ttf");

        //declare the views
        ImageView goToProfile = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.msg_act_prof_btn);
        TextView participantName = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.msg_act_part_name);

        //set FIRST name and custom font
        String title = (mParticipantName != null) ? mParticipantName.split(" ")[0] : ("Messages");
        participantName.setText(title);
        participantName.setTypeface(custom_font);


        //same action as when a "read" dare is opened in MessagesFragment
        goToProfile.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(getApplicationContext(),DareOpenedActivity.class);
                intent.putExtra("user_id",toUserId);
                startActivity(intent);

            }
        });
    }

    @Override
    protected void onPause() {
        // Update the notification position to the latest seen
        PushNotificationReceiver.getNotifications(this).clear(mConversation);

        getLayerClient().unregisterEventListener(mIdentityChangeListener);
        super.onPause();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        mMessagesList.onDestroy();
    }

    public void setTitle(boolean useConversation) {

        if (mParticipantName!=null){

        }else{
            setTitle("Messages");
        }


    }


    private void setConversation(Conversation conversation, String type) {
        mConversation = conversation;
        mHistoricFetchLayout.setConversation(conversation);
        mMessagesList.setConversation(conversation);
        mTypingIndicator.setConversation(conversation);
        mMessageComposer.setConversation(conversation);

//        // UI state
//        if (conversation == null) {
//            setUiState(UiState.ADDRESS);
//            return;
//        }

        if (type.equals(NEW_CONVO)) {
            setUiState(UiState.NEW_CONVERSATION);
            return;
        }else if (type.equals(SINGLE_TEXT_SENT)){
            setUiState(UiState.SINGLE_TEXT_SENT);
        }else if (type.equals(REPLY_MODE)){
            setUiState(UiState.CONVERSATION_COMPOSER);
        }

//        if (conversation.getHistoricSyncStatus() == Conversation.HistoricSyncStatus.INVALID) {
//            // New "temporary" conversation
//            setUiState(UiState.ADDRESS_COMPOSER);
//        } else {
//            setUiState(UiState.ADDRESS_CONVERSATION_COMPOSER);
//        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        mMessageComposer.onActivityResult(this, requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        mMessageComposer.onRequestPermissionsResult(requestCode, permissions, grantResults);
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
    }

    public void singleTextSent(Conversation conversation){
       setConversation(conversation, SINGLE_TEXT_SENT);
    }

   public void setLoadingIndicator(boolean active){
       if (mLoadingDialog == null) {
           mLoadingDialog = new LoadingDialog(this);
           mLoadingDialog.setCancelable(false);
       }
       if (active) {
           mLoadingDialog.show();
       } else {
           mLoadingDialog.dismiss();
       }


   }
    private enum UiState {
//        ADDRESS,
//        ADDRESS_COMPOSER,
//        ADDRESS_CONVERSATION_COMPOSER,
        CONVERSATION_COMPOSER,
        SINGLE_TEXT_SENT,
        NEW_CONVERSATION
    }

    private class IdentityChangeListener implements LayerChangeEventListener.Weak {
        @Override
        public void onChangeEvent(LayerChangeEvent layerChangeEvent) {
            // Don't need to update title if there is no conversation
            if (mConversation == null) {
                return;
            }

            for (LayerChange change : layerChangeEvent.getChanges()) {
                if (change.getObjectType().equals(LayerObject.Type.IDENTITY)) {
                    Identity identity = (Identity) change.getObject();
                    if (mConversation.getParticipants().contains(identity)) {
                        setTitle(true);
                    }
                }
            }
        }
    }

    public static void setUpdateConversationListener(UpdateConversationListener updateConversationListener){
        mUpdateConversationListener = updateConversationListener;
    }

    public static interface UpdateConversationListener{
        public void updateConversation(Conversation conversation);
    }


}

