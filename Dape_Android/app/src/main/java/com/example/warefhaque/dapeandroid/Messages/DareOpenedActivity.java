package com.example.warefhaque.dapeandroid.Messages;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.warefhaque.dapeandroid.Database.ParseAuthentication;
import com.example.warefhaque.dapeandroid.People.FullScreenImageProfile;
import com.example.warefhaque.dapeandroid.Profile.ProfilePresenter;
import com.example.warefhaque.dapeandroid.R;
import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warefhaque on 3/9/17.
 *
 * Activity responsible for when a dare is opened
 * and from the goToProfile from MessagesListActivity
 */

public class DareOpenedActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.user_profile);

        if (getIntent().hasExtra("user_id")){
            Log.e(TAG, "user_id= "+getIntent().getStringExtra("user_id"));
            String userId = getIntent().getStringExtra("user_id");
            DetailedPeopleItem(userId);
        }

        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }

    }
    public static final String TAG = "Detailed People Item";
    ImageView mProfilePicture;
    TextView mAboutMe;
    TextView mAboutMeTitle;
    TextView mName;
    TextView mContactTitle;
    TextView mMutualFriends;
    ImageView messageButton;
    Activity mActivity;
    ImageView mMutualFriendsIcon;

    //fetch the user based on the id and then populate the fragment
    public void DetailedPeopleItem(final String userId){
        mProfilePicture = (ImageView) findViewById(R.id.profilebg);
        mAboutMe = (TextView) findViewById(R.id.profile_about_me);
        mName = (TextView) findViewById(R.id.profile_name);
        mAboutMeTitle = (TextView)findViewById(R.id.about_user_title);
        mContactTitle = (TextView) findViewById(R.id.reach_out_title);
        mMutualFriends = (TextView) findViewById(R.id.mutual_friends);
        mMutualFriendsIcon = (ImageView) findViewById(R.id.mutual_friends_icon);
        messageButton = (ImageView) findViewById(R.id.message_button);
        mActivity = this;


        ParseQuery<ParseUser> query = ParseUser.getQuery();
        query.whereEqualTo("objectId", userId);
        query.getFirstInBackground(new GetCallback<ParseUser>() {
            @Override
            public void done(final ParseUser object, ParseException e) {
                String name  = object.getString("name");
                String[] nameArray = name.split(" ");
                mContactTitle.setText("Reach out to "+nameArray[0]+"!");
                mAboutMe.setText(object.getString("about"));
                mAboutMeTitle.setText("About "+ nameArray[0]);
                ProfilePresenter.fetchUserProfileImage(mProfilePicture, object, "profilePic",140,140, getApplicationContext());
                showSocialMediaEntitles(object);
                fetchMutualFriends(object, mMutualFriends);


            }
        });

        mProfilePicture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(DareOpenedActivity.this, FullScreenImageProfile.class);
                intent.putExtra("userId", userId);
                // only one large picture showing
                intent.putExtra("position", 1);
                startActivity(intent);
            }
        });
    }

    public void showSocialMediaEntitles( ParseUser user){

        List<UserProfileSocialItem> items = getSocialItems(user);

        if (items!=null || items.size()>=0){

            RelativeLayout relativeLayout = (RelativeLayout)findViewById(R.id.social_items_layout);

            for (int i = 0; i<items.size(); i++){

                if (!items.get(i).mAccount.equals("")){
                    RelativeLayout item = (RelativeLayout) relativeLayout.getChildAt(i);
                    item.setVisibility(View.VISIBLE);
                    ImageView icon = (ImageView) item.getChildAt(0);
                    TextView account = (TextView) item.getChildAt(1);
                    icon.setImageResource(items.get(i).mResId);
                    account.setText(items.get(i).mAccount);
                }

            }
        }

    }

    public class UserProfileSocialItem{
        int mResId;
        String mAccount;

        UserProfileSocialItem(int resId, String account){
            mResId = resId;
            mAccount = account;
        }
    }

    List<UserProfileSocialItem> getSocialItems(ParseUser currUser){

        List<String> accounts = new ArrayList<>();
        List<UserProfileSocialItem>result = new ArrayList<>();
        if (currUser.getString("instagram")!=null)
            accounts.add(currUser.getString("instagram"));

        if (currUser.getString("twitter")!=null)
            accounts.add(currUser.getString("twitter"));

        if (currUser.getString("snapchat")!=null)
            accounts.add(currUser.getString("snapchat"));

        if (currUser.getString("whatsapp")!=null)
            accounts.add(currUser.getString("whatsapp"));

        if (currUser.getString("otherSM")!=null)
            accounts.add(currUser.getString("otherSM"));


        List<String> publicEntitleMents = currUser.getList("publicEntitlements");
        List<String> currSocialMedia = currUser.getList("currentSocialMedia");

        if (publicEntitleMents!=null &&  currSocialMedia!=null){
            for (int i = 0; i<currSocialMedia.size();i++){
                for (int j= 0; j< publicEntitleMents.size(); j++){
                    if (currSocialMedia.get(i).equals(publicEntitleMents.get(j))){
                        int resId = 0;
                        if (publicEntitleMents.get(j).equals(ParseAuthentication.INSTAGRAM))
                            resId = R.drawable.instagram_logo_profile;
                        if (publicEntitleMents.get(j).equals(ParseAuthentication.TWITTER))
                            resId = R.drawable.twitter_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.SNAPCHAT))
                            resId = R.drawable.snapchat_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.WHATSAPP))
                            resId = R.drawable.whatsapp_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.DEFAULT))
                            resId = R.drawable.hashtag_filled;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.PINTEREST))
                            resId = R.drawable.pinterest_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.LINKEDIN))
                            resId = R.drawable.linked_in_logo_profile;

                        if (publicEntitleMents.get(j).equals(ParseAuthentication.TUMBLR))
                            resId = R.drawable.tumblr_logo_profile;
                        if (publicEntitleMents.get(j).equals(ParseAuthentication.FACEBOOK))
                            resId = R.drawable.facebook_logo_profile;
                        // TODO: 3/8/17 come back later for this
                        if (accounts.size()>i){
                            result.add(new UserProfileSocialItem(resId,accounts.get(i)));
                        }
                    }
                }
            }

        }


        return result;
    }

    void fetchMutualFriends(ParseUser user, final TextView textView){
        Bundle params = new Bundle();
        params.putString("fields", "context.fields(mutual_friends)");
        StringBuilder sb = new StringBuilder("/");
        Log.d(TAG, sb.toString());
        sb.append(user.getString("facebookID"));
        /* make the API call */
        new GraphRequest(
                AccessToken.getCurrentAccessToken(),
                sb.toString(),
                params,
                HttpMethod.GET,
                new GraphRequest.Callback() {
                    public void onCompleted(GraphResponse response) {
                        /* handle the result */
                        JSONObject friends =response.getJSONObject();
                        if (friends!=null){
                            Log.d(TAG, friends.toString());
                            try {
                                int mutualFriendCount = friends.getJSONObject("context").getJSONObject("mutual_friends").getJSONObject("summary").getInt("total_count");
                                textView.setText("You have "+mutualFriendCount+" mutual friends");
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            Log.d(TAG, "friends is null");
                        }
                    }
                }
        ).executeAsync();
    }

}
