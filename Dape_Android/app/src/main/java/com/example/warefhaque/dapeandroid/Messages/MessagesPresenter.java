package com.example.warefhaque.dapeandroid.Messages;

import android.util.Log;

import com.example.warefhaque.dapeandroid.Layer.LayerImpl;
import com.layer.sdk.messaging.Conversation;
import com.layer.sdk.query.Query;
import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import org.joda.time.DateTime;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by warefhaque on 2/22/17.
 */

public class MessagesPresenter {
    public static final String TAG = "Messages Presenter";
    MessageAdapterListener messageAdapterListener;

    MessagesPresenter(MessageAdapterListener listener){
        messageAdapterListener = listener;
    }

    public void fetchUserActivities(){
        assert ParseUser.getCurrentUser()!=null;
        ArrayList<String> userIdArray = new ArrayList<>();
        userIdArray.add(ParseUser.getCurrentUser().getObjectId());
        ParseQuery<ParseObject> query = ParseQuery.getQuery("UserActivity");
        query.whereContainedIn("participantsIDs",userIdArray);
        query.include("place").include("participants");
        Date currDate= new Date();
        Date daysAgo = new DateTime(currDate).minusHours(72).toDate();
        query.whereGreaterThan("when",daysAgo);

        query.findInBackground(new FindCallback<ParseObject>() {
            @Override
            public void done(List<ParseObject> objects, ParseException e) {
                if (e==null){
                    Log.d(TAG, objects.toString() );

                    Query conversationQuery = Query.builder(Conversation.class).build();
                    List<Conversation> conversations = LayerImpl.getLayerClient().executeQuery(conversationQuery, Query.ResultType.OBJECTS);
                    messageAdapterListener.fetchedListOfActivities(objects,conversations);
                }else{
                    e.printStackTrace();
                }
            }
        });

    }

    public static interface MessageAdapterListener{
        void fetchedListOfActivities(List<ParseObject> activities, List<Conversation> conversations);
    }

}
