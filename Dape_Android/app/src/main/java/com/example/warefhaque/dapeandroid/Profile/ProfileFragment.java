package com.example.warefhaque.dapeandroid.Profile;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.Authentication.LoginActivity;
import com.example.warefhaque.dapeandroid.Database.ParseAuthentication;
import com.example.warefhaque.dapeandroid.LauncherActivity;
import com.example.warefhaque.dapeandroid.LoadingDialog;
import com.example.warefhaque.dapeandroid.People.FullScreenImageProfile;
import com.example.warefhaque.dapeandroid.R;
import com.example.warefhaque.dapeandroid.Utils.AvatarImageView;
import com.example.warefhaque.dapeandroid.Utils.MainActivity;
import com.parse.ParseUser;
import com.theartofdev.edmodo.cropper.CropImage;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by warefhaque on 12/18/16.
 */

public class ProfileFragment extends Fragment implements ProfileInterface.View {


    public static final String TAG = "Profile Fragment";

    public ProfileInterface.Presenter mPresenter;
    private Bitmap mMainBitmap;
    private Bitmap mLeftBitmap;
    private Bitmap mRightBitmap;
    private static final int CAMERA_REQUEST = 1888;
    private static final int SELECT_FILE = 2000;
    private static  final int CHANGE_SOCIAL_MEDIA = 3000;
    private AvatarImageView mMainImage;
    private Uri mCropImageUri;
    private AvatarImageView mLeftImage;
    private AvatarImageView mRightImage;
    private EditText mInstagramAccount;
    private EditText mSnapChatAdcount;
    private EditText mTwitterAccount;
    private EditText mWhatsAppAccount;
    private LoadingDialog mLoadingDialog;
    public EditText mAboutMe;
    private EditText mOtherSm;
    private CheckBox mDareFirstSocialMedia;
    private CheckBox mProfileFirstSocialMedia;
    private CheckBox mDareThirdSocialMedia;
    private CheckBox mProfileSecondSocialMedia;
    private CheckBox mDareSecondSocialMedia;
    private CheckBox mProfileThirdSocialMedia;
    private CheckBox mDareFourthSocialMedia;
    private CheckBox mProfileFourthSocialMedia;
    private CheckBox mDareFifthSocialMedia;
    private CheckBox mProfileFifthSocialMedia;
    private ImageView mFirstSocialMediaLogo;
    private ImageView mSecondSocialMediaLogo;
    private ImageView mThirdSocialMediaLogo;
    private ImageView mFourthSocialMediaLogo;
    private ImageView mFifthSocialMediaLogo;
    public TextView mName;

    public static List<String>profileImageUrls;

    //the current selection that the use has RN
    public List<String> mCurrentSocialMedia;
    //not going to change
    public static List<String> mDefaultSocialMedia;
    //all socialMedia
    private List<String> mAllSociaLMedia;

    private List<String> mDareEntitlements;

    private List<String> mProfileEntitlements;
    //controls the edit and the save modes at the top
    public boolean state = false;
    //to see which image was selected and which image to upload to
    Boolean[] imageSelector = new Boolean[3];

    public static final String FILL_EDIT_TEXT = "Please add your social account first!";

    public ProfileFragment() {
        //empty default constructor
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mDareEntitlements = new ArrayList<>();
        mProfileEntitlements = new ArrayList<>();
        mCurrentSocialMedia = new ArrayList<>();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_profile, container, false);

        mDareEntitlements = new ArrayList<>();
        mProfileEntitlements = new ArrayList<>();
        mCurrentSocialMedia = new ArrayList<>();
        //initialize the default social media set we are providing

        mDefaultSocialMedia = new ArrayList<>();
        mDefaultSocialMedia.add(ParseAuthentication.FACEBOOK);
        mDefaultSocialMedia.add(ParseAuthentication.INSTAGRAM);
        mDefaultSocialMedia.add(ParseAuthentication.SNAPCHAT);
        mDefaultSocialMedia.add(ParseAuthentication.WHATSAPP);
        mDefaultSocialMedia.add(ParseAuthentication.TWITTER);

        //initialize all social media
        mAllSociaLMedia = new ArrayList<>();
        mAllSociaLMedia.add(ParseAuthentication.FACEBOOK);
        mAllSociaLMedia.add(ParseAuthentication.INSTAGRAM);
        mAllSociaLMedia.add(ParseAuthentication.TWITTER);
        mAllSociaLMedia.add(ParseAuthentication.SNAPCHAT);
        mAllSociaLMedia.add(ParseAuthentication.WHATSAPP);
        mAllSociaLMedia.add(ParseAuthentication.DEFAULT);
        mAllSociaLMedia.add(ParseAuthentication.LINKEDIN);
        mAllSociaLMedia.add(ParseAuthentication.PINTEREST);
        mAllSociaLMedia.add(ParseAuthentication.TUMBLR);

        Arrays.fill(imageSelector, Boolean.FALSE);
        //declare the views
        mMainImage = (AvatarImageView) view.findViewById(R.id.profile_pic_main);
        mMainImage = (AvatarImageView) view.findViewById(R.id.profile_pic_main);
        mLeftImage = (AvatarImageView) view.findViewById(R.id.profile_pic_left);
        mRightImage = (AvatarImageView) view.findViewById(R.id.profile_pic_right);
        mInstagramAccount = (EditText) view.findViewById(R.id.instagram_account);
        mSnapChatAdcount = (EditText) view.findViewById(R.id.snapchat_account);
        mTwitterAccount = (EditText) view.findViewById(R.id.twitter_account);
        mWhatsAppAccount = (EditText) view.findViewById(R.id.whatsapp_account);
        mAboutMe = (EditText) view.findViewById(R.id.about_me_edit_text);
        mOtherSm = (EditText) view.findViewById(R.id.other_account);
        mName = (TextView) view.findViewById(R.id.profile_name);

        if (mPresenter != null)
        {
            Log.d(TAG,"Called fetch profile");
            mPresenter.fetchCurrentUserProfile(mInstagramAccount, mSnapChatAdcount, mTwitterAccount, mWhatsAppAccount, mAboutMe, mOtherSm, mName,getActivity());
            mPresenter.fetchImage(mMainImage,"profilePic",170, 170, getActivity());
            mPresenter.fetchImage(mLeftImage, "leftPic", 150, 150, getActivity());
            mPresenter.fetchImage(mRightImage,"rightPic", 150, 150, getActivity());

        }
        ImageView mBackgroundImage = (ImageView) view.findViewById(R.id.profilebg);
//        Picasso.with(getActivity())
//                .load(R.drawable.profilebg)
//                .centerCrop()
//                .resize(400, 260)
////                .onlyScaleDown()
//                .into(mBackgroundImage);


        mDareFirstSocialMedia = (CheckBox) view.findViewById(R.id.dare_enable_instagram);
        mDareSecondSocialMedia = (CheckBox) view.findViewById(R.id.dare_enable_twitter);
        mDareThirdSocialMedia = (CheckBox) view.findViewById(R.id.dare_enable_snapchat);
        mDareFourthSocialMedia = (CheckBox) view.findViewById(R.id.dare_enable_whatsapp);
        mDareFifthSocialMedia = (CheckBox) view.findViewById(R.id.dare_enable_other);
        mProfileFirstSocialMedia = (CheckBox) view.findViewById(R.id.profile_enable_instagram);
        mProfileSecondSocialMedia = (CheckBox) view.findViewById(R.id.profile_enable_twitter);
        mProfileThirdSocialMedia = (CheckBox) view.findViewById(R.id.profile_enable_snapchat);
        mProfileFourthSocialMedia = (CheckBox) view.findViewById(R.id.profile_enable_whatsapp);
        mProfileFifthSocialMedia = (CheckBox) view.findViewById(R.id.profile_enable_other);

        //Logos
        mFirstSocialMediaLogo = (ImageView) view.findViewById(R.id.first_social_media_logo);
        mSecondSocialMediaLogo = (ImageView) view.findViewById(R.id.second_social_media_logo);
        mThirdSocialMediaLogo = (ImageView) view.findViewById(R.id.third_social_media_logo);
        mFourthSocialMediaLogo = (ImageView) view.findViewById(R.id.fourth_social_media_logo);
        mFifthSocialMediaLogo = (ImageView) view.findViewById(R.id.fifth_social_media_logo);

        //name

        mFirstSocialMediaLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupSocialMediaChoice((ImageView) v, mInstagramAccount, mDareFirstSocialMedia, mProfileFirstSocialMedia, 0);
            }
        });
        mSecondSocialMediaLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupSocialMediaChoice((ImageView) v, mTwitterAccount, mDareSecondSocialMedia, mProfileSecondSocialMedia, 1);
            }
        });
        mThirdSocialMediaLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupSocialMediaChoice((ImageView) v, mSnapChatAdcount, mDareThirdSocialMedia, mProfileThirdSocialMedia, 2);
            }
        });
        mFourthSocialMediaLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupSocialMediaChoice((ImageView) v, mWhatsAppAccount, mDareFourthSocialMedia, mProfileFourthSocialMedia, 3);
            }
        });
        mFifthSocialMediaLogo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setupSocialMediaChoice((ImageView) v, mOtherSm, mDareFifthSocialMedia, mProfileFifthSocialMedia, 4);
            }
        });

//        setStateActive(false);


        //Profile Image click listeners
        mMainImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (state)
                {
                    Arrays.fill(imageSelector, Boolean.FALSE);
                    imageSelector[1] = Boolean.TRUE;
                    selectImage("profilePic");
                }
                else
                {
                    Intent intent = new Intent(getActivity(), FullScreenImageProfile.class);
                    intent.putExtra("userId", ParseUser.getCurrentUser().getObjectId());
                    intent.putExtra("position", 1);
                    startActivity(intent);
                }

            }
        });
        mLeftImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (state)
                {
                    Arrays.fill(imageSelector, Boolean.FALSE);
                    imageSelector[0] = Boolean.TRUE;
                    selectImage("leftPic");
                }
                else
                {
                    Intent intent = new Intent(getActivity(), FullScreenImageProfile.class);
                    intent.putExtra("userId", ParseUser.getCurrentUser().getObjectId());
                    intent.putExtra("position", 0);
                    startActivity(intent);
                }

            }
        });
        mRightImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (state)
                {
                    Arrays.fill(imageSelector, Boolean.FALSE);
                    imageSelector[2] = Boolean.TRUE;
                    selectImage("rightPic");
                }
                else
                {
                    Intent intent = new Intent(getActivity(), FullScreenImageProfile.class);
                    intent.putExtra("userId", ParseUser.getCurrentUser().getObjectId());
                    intent.putExtra("position", 2);
                    startActivity(intent);
                }

            }
        });


        mDareFirstSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){


                            if (!TextUtils.isEmpty(mInstagramAccount.getText()))
                            {

//                                mProfileFirstSocialMedia.setChecked(true);
                                buttonView.setButtonDrawable(R.drawable.select);
                                Log.d(TAG, "DareInterfaceActivity Entitlements Size: " + mDareEntitlements.size());
                                Log.d(TAG, "Current Social Media: " +  LauncherActivity.currentSocialMediaCopy.size());
                                if (!mDareEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(0))){
                                    mDareEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(0));
                                }

                            }
                            else
                            {
                                showDialog(FILL_EDIT_TEXT);

                            }

//
                        }else{
                            buttonView.setButtonDrawable(R.drawable.deselect);
                            mDareEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(0));
                        }
                        Log.d("DARE ENTITLES", mDareEntitlements.toString());

                    }

                }

            }
        });
        mProfileFirstSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){

                            if (!TextUtils.isEmpty(mInstagramAccount.getText()))
                            {


                                mDareFirstSocialMedia.setChecked(true);
                                buttonView.setButtonDrawable(R.drawable.select);

                                if (!mProfileEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(0))){
                                    mProfileEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(0));
                                }


                            }
                            else
                            {
                                showDialog(FILL_EDIT_TEXT);

                            }

                        }else{
                            mDareFirstSocialMedia.setChecked(false);
                            if (!mDareFirstSocialMedia.isChecked()){
                                buttonView.setButtonDrawable(R.drawable.deselect);
                                mProfileEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(0));
                            }



                        }
                        Log.d("PROFILE ENTITLES", mProfileEntitlements.toString());
                    }
                }


            }
        });
        mDareSecondSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){

                            if (!TextUtils.isEmpty(mTwitterAccount.getText()))
                            {
//                                mProfileSecondSocialMedia.setChecked(true);
                                buttonView.setButtonDrawable(R.drawable.select);

                                if (!mDareEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(1))){
                                    mDareEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(1));
                                }


                            }
                            else
                            {
                                showDialog(FILL_EDIT_TEXT);

                            }

//
                        }else{
                            buttonView.setButtonDrawable(R.drawable.deselect);
                            mDareEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(1));
                        }
                        Log.d("DARE ENTITLES", mDareEntitlements.toString());
                    }


                }

            }
        });
        mProfileSecondSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){


                            if (!TextUtils.isEmpty(mTwitterAccount.getText()))
                            {

                                mDareSecondSocialMedia.setChecked(true);
                                buttonView.setButtonDrawable(R.drawable.select);
                                if (!mProfileEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(1))){
                                    mProfileEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(1));
                                }


                            }
                            else
                            {
                                showDialog(FILL_EDIT_TEXT);

                            }

                        }else{
                            mDareSecondSocialMedia.setChecked(false);
                            if (!mDareSecondSocialMedia.isChecked()){

                                buttonView.setButtonDrawable(R.drawable.deselect);
                                mProfileEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(1));
                            }


                        }
                        Log.d("PROFILE ENTITLES", mProfileEntitlements.toString());
                    }
                }



            }
        });
        mDareThirdSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){

                            if (!TextUtils.isEmpty(mSnapChatAdcount.getText()))
                            {
//                                mProfileThirdSocialMedia.setChecked(true);
                                buttonView.setButtonDrawable(R.drawable.select);

                                if (!mDareEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(2))){
                                    mDareEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(2));
                                }


                            }
                            else
                            {
                                showDialog(FILL_EDIT_TEXT);

                            }

//
                        }else{
                            buttonView.setButtonDrawable(R.drawable.deselect);
                            mDareEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(2));
                        }
                        Log.d("DARE ENTITLES", mDareEntitlements.toString());
                    }
                }



            }
        });
        mProfileThirdSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){

                            if (!TextUtils.isEmpty(mSnapChatAdcount.getText()))
                            {

                                mDareThirdSocialMedia.setChecked(true);
                                buttonView.setButtonDrawable(R.drawable.select);

                                if (!mProfileEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(2))){
                                    mProfileEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(2));
                                }

                            }
                            else
                            {
                                showDialog(FILL_EDIT_TEXT);

                            }


                        }else{
                            mDareThirdSocialMedia.setChecked(false);
                            if (!mDareThirdSocialMedia.isChecked()){
                                buttonView.setButtonDrawable(R.drawable.deselect);
                                mProfileEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(2));
                            }



                        }
                        Log.d("PROFILE ENTITLES", mProfileEntitlements.toString());
                    }
                }



            }
        });
        mDareFourthSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){

                            if (!TextUtils.isEmpty(mWhatsAppAccount.getText()))
                            {

//                        mProfileFourthSocialMedia.setChecked(true);
                                buttonView.setButtonDrawable(R.drawable.select);

                                if (!mDareEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(3))){
                                    mDareEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(3));
                                }


                            }
                            else
                            {

                                showDialog(FILL_EDIT_TEXT);

                            }

                        }else{


                            buttonView.setButtonDrawable(R.drawable.deselect);
                            mDareEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(3));
                        }
                        Log.d("DARE ENTITLES", mDareEntitlements.toString());
                    }
                }



            }
        });
        mProfileFourthSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){

                            if (!TextUtils.isEmpty(mWhatsAppAccount.getText()))
                            {

                                mDareFourthSocialMedia.setChecked(true);
                                buttonView.setButtonDrawable(R.drawable.select);

                                buttonView.setBackgroundResource(R.drawable.select);

                                if (!mProfileEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(3))){
                                    mProfileEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(3));
                                }

                            }
                            else
                            {
                                showDialog(FILL_EDIT_TEXT);

                            }


                        }else{
                            mDareFourthSocialMedia.setChecked(false);
                            if (!mDareFourthSocialMedia.isChecked()){

                                buttonView.setButtonDrawable(R.drawable.deselect);
                                mProfileEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(3));
                            }


                        }
                        Log.d("PROFILE ENTITLES", mProfileEntitlements.toString());
                    }
                }



            }
        });
        mDareFifthSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){

                            if (!TextUtils.isEmpty(mOtherSm.getText()))
                            {
                                buttonView.setButtonDrawable(R.drawable.select);

                                if (!mDareEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(4))){
                                    mDareEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(4));
                                }

                            }
                            else
                            {

                                showDialog(FILL_EDIT_TEXT);

                            }

//                        mProfileFifthSocialMedia.setChecked(true);

                        }else{
                            buttonView.setButtonDrawable(R.drawable.deselect);
                            mDareEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(4));
                        }
                        Log.d("DARE ENTITLES", mDareEntitlements.toString());
                    }
                }



            }
        });
        mProfileFifthSocialMedia.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (LauncherActivity.currentSocialMediaCopy!=null){
                    if (LauncherActivity.currentSocialMediaCopy.size()>0){
                        if (isChecked){

                            if (!TextUtils.isEmpty(mOtherSm.getText()))
                            {
                                mDareFifthSocialMedia.setChecked(true);
                                buttonView.setButtonDrawable(R.drawable.select);

                                if (!mProfileEntitlements.contains(LauncherActivity.currentSocialMediaCopy.get(4))){
                                    mProfileEntitlements.add(LauncherActivity.currentSocialMediaCopy.get(4));
                                }

                            }
                            else
                            {
                                showDialog(FILL_EDIT_TEXT);

                            }


                        }else{
                            mDareFifthSocialMedia.setChecked(false);
                            if (!mDareFifthSocialMedia.isChecked()){

                                buttonView.setButtonDrawable(R.drawable.deselect);
                                mProfileEntitlements.remove(LauncherActivity.currentSocialMediaCopy.get(4));
                            }


                        }
                        Log.d("PROFILE ENTITLES", mProfileEntitlements.toString());
                    }

                }


            }
        });


        return view;


    }

    @Override
    public void onCreateOptionsMenu(final Menu menu, MenuInflater inflater) {
//        menu.clear();
//        inflater.inflate(R.menu.profile_menu, menu);
//        Log.d(TAG, "onCreateOptionsMenu");
//        setStateActive(false);
//        menu.findItem(R.id.cancel).setTitle("Cancel").setVisible(false)
//                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//
//                        setStateActive(false);
//                        menu.findItem(R.id.edit_save).setTitle("Edit");
//                        item.setVisible(false);
//                        return false;
//                    }
//                });
//        menu.findItem(R.id.edit_save).setTitle("Edit")
//                .setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
//                    @Override
//                    public boolean onMenuItemClick(MenuItem item) {
//
//                        if (!state) {
//                            item.setTitle("Save");
//                            setStateActive(true);
//                            menu.findItem(R.id.cancel).setVisible(true);
//                        } else {
//                            //saving occurs here
//                            item.setTitle("Edit");
//                            setStateActive(false);
//                            menu.findItem(R.id.cancel).setVisible(false);
//                            String[] dareEntitlements = mDareEntitlements.toArray(new String[mDareEntitlements.size()]);
//                            String[] profileEntitlements = mProfileEntitlements.toArray(new String[mProfileEntitlements.size()]);
//                            String [] currentSocialMedia = mCurrentSocialMedia.toArray(new String[mCurrentSocialMedia.size()]);
//                            mPresenter.storeProfile(mInstagramAccount.getText(), mSnapChatAdcount.getText(), mTwitterAccount.getText(), mWhatsAppAccount.getText(), mAboutMe.getText(), mOtherSm.getText(), dareEntitlements, profileEntitlements,currentSocialMedia );
//
//                        }
//
//
//                        return true;
//                    }
//                });
    }

    @Override
    public void onResume() {
        super.onResume();
        Log.d(TAG, "onResume");
    }

    @Override
    public void setLoadingIndicator(boolean active) {
    }

    /**
     * main function which sets up the profile from the DB
     *
     * @param currentSocialMediaItems - Contains all info about the social media items
     * @param currentSocialMedia - to update the Current Social Media list
     */
    @Override
    public void fetchSuccess(List<ProfilePresenter.SocialItem> currentSocialMediaItems, List<String> currentSocialMedia) {

        mCurrentSocialMedia=currentSocialMedia;
        LauncherActivity.currentSocialMediaCopy = currentSocialMedia;
        Log.d("curr social (fetch)", mCurrentSocialMedia.toString());
        mFirstSocialMediaLogo.setImageResource(currentSocialMediaItems.get(0).mSocialLogoId);
        mSecondSocialMediaLogo.setImageResource(currentSocialMediaItems.get(1).mSocialLogoId);
        mThirdSocialMediaLogo.setImageResource(currentSocialMediaItems.get(2).mSocialLogoId);
        mFourthSocialMediaLogo.setImageResource(currentSocialMediaItems.get(3).mSocialLogoId);
        mFifthSocialMediaLogo.setImageResource(currentSocialMediaItems.get(4).mSocialLogoId);

        if (currentSocialMediaItems.get(0).mSocialAccount.equals("")){
            mInstagramAccount.setText("");
            mInstagramAccount.setHint(currentSocialMediaItems.get(0).mHint);
        }else{
            mInstagramAccount.setText(currentSocialMediaItems.get(0).mSocialAccount);
        }

        if (currentSocialMediaItems.get(1).mSocialAccount.equals("")){
            mTwitterAccount.setText("");
            mTwitterAccount.setHint(currentSocialMediaItems.get(1).mHint);
        }else{
            mTwitterAccount.setText(currentSocialMediaItems.get(1).mSocialAccount);
        }
        if (currentSocialMediaItems.get(2).mSocialAccount.equals("")){
            mSnapChatAdcount.setText("");
            mSnapChatAdcount.setHint(currentSocialMediaItems.get(2).mHint);
        }else{
            mSnapChatAdcount.setText(currentSocialMediaItems.get(2).mSocialAccount);
        }
        if (currentSocialMediaItems.get(3).mSocialAccount.equals("")){
            mWhatsAppAccount.setText("");
            mWhatsAppAccount.setHint(currentSocialMediaItems.get(3).mHint);
        }else{
            mWhatsAppAccount.setText(currentSocialMediaItems.get(3).mSocialAccount);
        }
        if (currentSocialMediaItems.get(4).mSocialAccount.equals("")){
            mOtherSm.setText("");
            mOtherSm.setHint(currentSocialMediaItems.get(4).mHint);
        }else{
            mOtherSm.setText(currentSocialMediaItems.get(4).mSocialAccount);
        }

        mDareFirstSocialMedia.setChecked(currentSocialMediaItems.get(0).mDareenabled);
        mDareSecondSocialMedia.setChecked(currentSocialMediaItems.get(1).mDareenabled);
        mDareThirdSocialMedia.setChecked(currentSocialMediaItems.get(2).mDareenabled);
        mDareFourthSocialMedia.setChecked(currentSocialMediaItems.get(3).mDareenabled);
        mDareFifthSocialMedia.setChecked(currentSocialMediaItems.get(4).mDareenabled);

        mProfileFirstSocialMedia.setChecked(currentSocialMediaItems.get(0).mProfileEnabled);
        mProfileSecondSocialMedia.setChecked(currentSocialMediaItems.get(1).mProfileEnabled);
        mProfileThirdSocialMedia.setChecked(currentSocialMediaItems.get(2).mProfileEnabled);
        mProfileFourthSocialMedia.setChecked(currentSocialMediaItems.get(3).mProfileEnabled);
        mProfileFifthSocialMedia.setChecked(currentSocialMediaItems.get(4).mProfileEnabled);

        LauncherActivity launcherActivity = (LauncherActivity) getActivity();
        if (launcherActivity.mFirstTimeLogin){
            setStateActive(true);
        }else{
            setStateActive(false);
        }

    }

    /**
     * Called from ViewPager Activity to link ProfileFragment and ProfilePresenter
     *
     * @param presenter = ProfilePresenter
     */

    @Override
    public void setPresenter(ProfileInterface.Presenter presenter) {
        Log.d(TAG, "setPresenter");
        mPresenter = presenter;
    }

    /**
     * Called from the Presenter to display errors or success messages
     * @param message - error messages sent from the presenter when saving or retrieving the profile
     */

    @Override
    public void presenterMessageCallback(String message) {
        Toast.makeText(getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void logOut() {
        Intent intent = new Intent(getActivity(), LoginActivity.class);
        startActivity(intent);
    }

    /**
     * Controls whether you can edit the screen or not
     *
     * @param active?true - can edit
     */
    public void setStateActive(boolean active) {

        state = active;
        mInstagramAccount.setEnabled(active);
        mSnapChatAdcount.setEnabled(active);
        mWhatsAppAccount.setEnabled(active);
        mTwitterAccount.setEnabled(active);
        mAboutMe.setEnabled(active);
//        mMainImage.setClickable(active);
//        mLeftImage.setClickable(active);
//        mRightImage.setClickable(active);
        mOtherSm.setEnabled(active);
        mDareFirstSocialMedia.setEnabled(active);
        mProfileFirstSocialMedia.setEnabled(active);
        mDareThirdSocialMedia.setEnabled(active);
        mProfileSecondSocialMedia.setEnabled(active);
        mDareSecondSocialMedia.setEnabled(active);
        mProfileThirdSocialMedia.setEnabled(active);
        mDareFourthSocialMedia.setEnabled(active);
        mProfileFourthSocialMedia.setEnabled(active);
        mDareFifthSocialMedia.setEnabled(active);
        mProfileFifthSocialMedia.setEnabled(active);
        mFirstSocialMediaLogo.setClickable(active);
        mSecondSocialMediaLogo.setClickable(active);
        mThirdSocialMediaLogo.setClickable(active);
        mFourthSocialMediaLogo.setClickable(active);
        mFifthSocialMediaLogo.setClickable(active);

        if (active)
        {
            mFirstSocialMediaLogo.setBackgroundResource(R.drawable.border);
            mSecondSocialMediaLogo.setBackgroundResource(R.drawable.border);
            mThirdSocialMediaLogo.setBackgroundResource(R.drawable.border);
            mFourthSocialMediaLogo.setBackgroundResource(R.drawable.border);
            mFifthSocialMediaLogo.setBackgroundResource(R.drawable.border);
        }
        else
        {
            mFirstSocialMediaLogo.setBackgroundResource(0);
            mSecondSocialMediaLogo.setBackgroundResource(0);
            mThirdSocialMediaLogo.setBackgroundResource(0);
            mFourthSocialMediaLogo.setBackgroundResource(0);
            mFifthSocialMediaLogo.setBackgroundResource(0);
        }
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        if (requestCode == CropImage.CAMERA_CAPTURE_PERMISSIONS_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                CropImage.startPickImageActivity(getActivity());
            } else {
                Toast.makeText(getActivity(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
        if (requestCode == CropImage.PICK_IMAGE_PERMISSIONS_REQUEST_CODE) {
            if (mCropImageUri != null && grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
//                mCurrentFragment.setImageUri(mCropImageUri);
                Log.d(TAG, "CAME INTO THE SET IMAGE CASE!!");
            } else {
                Toast.makeText(getActivity(), "Cancelling, required permissions are not granted", Toast.LENGTH_LONG).show();
            }
        }
    }



    private void selectImage(final String selectedImage)
    {
        Intent intent = new Intent(getActivity(), MainActivity.class);
        intent.putExtra("imgSel", selectedImage);
        startActivity(intent);
    }


//    @Override
//    public void onActivityResult(int requestCode, int resultCode, Intent data) {
//        super.onActivityResult(requestCode, resultCode, data);
//        Log.d(TAG,"onActivityResult-----");
//
//        Log.d(TAG,"resultCode=" + resultCode);
//        if (resultCode == RESULT_OK) {
//
//            if (requestCode == CAMERA_REQUEST) {
//                Intent intent = data;
//                Bundle bundle = intent.getExtras();
//                Uri imageUri = (Uri)bundle.get(Intent.EXTRA_STREAM);
//                CropImage.activity(imageUri)
//                        .start(getContext(), this);
//                CropImage.ActivityResult result = CropImage.getActivityResult(data);
//                if (resultCode == RESULT_OK) {
//                    Uri resultUri = result.getUri();
//                    Log.d(TAG, resultUri.toString());
//                } else if (resultCode == CropImage.CROP_IMAGE_ACTIVITY_RESULT_ERROR_CODE) {
//                    Exception error = result.getError();
//                }
//                Log.d(TAG,"Camera Request -----");
//
//                //For Samsung phones which require hardware save before processing
//                Bitmap thumbnail = (Bitmap) data.getExtras().get("data");
//                ByteArrayOutputStream bytes = new ByteArrayOutputStream();
//
//                File destination = new File(Environment.getExternalStorageDirectory(),
//                        System.currentTimeMillis() + ".jpg");
//
//                FileOutputStream fo;
//                try {
//                    destination.createNewFile();
//                    fo = new FileOutputStream(destination);
//                    fo.write(bytes.toByteArray());
//                    fo.close();
//                } catch (IOException e) {
//                    e.printStackTrace();
//                }
//                if (imageSelector[0]==Boolean.TRUE){
//                    mLeftImage.setImageBitmap(thumbnail);
//                    mPresenter.uploadImage(thumbnail,"leftImage");
//                }
//                if (imageSelector[1]==Boolean.TRUE){
//                    mMainImage.setImageBitmap(thumbnail);
//                    mPresenter.uploadImage(thumbnail,"mainImage");
//                }
//
//                if (imageSelector[2]==Boolean.TRUE){
//                    mRightImage.setImageBitmap(thumbnail);
//                    mPresenter.uploadImage(thumbnail,"rightImage");
//                }
//
//            } else if (requestCode == SELECT_FILE) {
//
//                try {
//                   Bitmap bm = MediaStore.Images.Media.getBitmap(getApplicationContext().getContentResolver(), data.getData());
//
//                    if (imageSelector[0]==Boolean.TRUE){
//                        mLeftImage.setImageBitmap(bm);
//                        mPresenter.uploadImage(bm,"leftImage");
//                    }
//                    if (imageSelector[1]==Boolean.TRUE){
//                        mMainImage.setImageBitmap(bm);
//                        mPresenter.uploadImage(bm,"mainImage");
//                    }
//
//                    if (imageSelector[2]==Boolean.TRUE){
//                        mRightImage.setImageBitmap(bm);
//                        mPresenter.uploadImage(bm,"rightImage");
//                    }
//
//                } catch (IOException e) {
//                    e.printStackTrace();
//                    Toast.makeText(getActivity(),"Error: Could not recieve gallery image. Please try again.",Toast.LENGTH_SHORT).show();
//                }
//            }
//        }
//    }

    //UTILITY:

    /**
     * For initial setting and when the array from DB is passed in
     * @param socialMedia = List of current Social Media
     */

    public void setmCurrentSocialMedia(List<String>socialMedia){

        mCurrentSocialMedia= new ArrayList<>();
        mCurrentSocialMedia.addAll(socialMedia);
    }

    public void setupSocialMediaChoice(final ImageView icon, final EditText account, final CheckBox dareEnabled, final CheckBox profileEnabled, final int position){
        final CharSequence [] items = getDialogItems();
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle("Choose what you want to share!");
        builder.setItems(items, new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int item) {

                if (items[item].equals("Instagram")) {
                    setUpCurrentSocialMedia(icon, account, dareEnabled, profileEnabled, R.drawable.instagram_logo_profile, "@", ParseAuthentication.INSTAGRAM, position );
                }else if (items[item].equals("Twitter")) {
                    setUpCurrentSocialMedia(icon, account, dareEnabled, profileEnabled, R.drawable.twitter_logo_profile, "@", ParseAuthentication.TWITTER, position );
                } else   if (items[item].equals("Linked In")) {
                    setUpCurrentSocialMedia(icon, account, dareEnabled, profileEnabled, R.drawable.linked_in_logo_profile, "@", ParseAuthentication.LINKEDIN,position );
                }else   if (items[item].equals("Snapchat")) {
                    setUpCurrentSocialMedia(icon, account, dareEnabled, profileEnabled, R.drawable.snapchat_logo_profile, "Type it here...", ParseAuthentication.SNAPCHAT,position );

                }else   if (items[item].equals("Whatsapp")) {
                    setUpCurrentSocialMedia(icon, account, dareEnabled, profileEnabled, R.drawable.whatsapp_logo_profile, "Add your number", ParseAuthentication.WHATSAPP,position );

                }else   if (items[item].equals("Tumblr")) {
                    setUpCurrentSocialMedia(icon, account, dareEnabled, profileEnabled, R.drawable.tumblr_logo_profile, "@", ParseAuthentication.TUMBLR,position );

                }else   if (items[item].equals("Pinterest")) {
                    setUpCurrentSocialMedia(icon, account, dareEnabled, profileEnabled, R.drawable.pinterest_logo_profile, "@", ParseAuthentication.PINTEREST,position );

                }else   if (items[item].equals("Other")) {
                    setUpCurrentSocialMedia(icon, account, dareEnabled, profileEnabled, R.drawable.hashtag_filled, "Choose from list", ParseAuthentication.DEFAULT,position );

                }else if (items[item].equals("Facebook")){
                    setUpCurrentSocialMedia(icon, account, dareEnabled, profileEnabled, R.drawable.facebook_logo_profile, "Name", ParseAuthentication.FACEBOOK,position );
                }
            }
        });
        builder.show();

    }

    public CharSequence[] getDialogItems(){

        List<String> resultList = new ArrayList<>();

        for (int i =0; i<mAllSociaLMedia.size();i++){
            boolean found = false;
            for (int j =0; j<mCurrentSocialMedia.size();j++ ){
                if (mAllSociaLMedia.get(i).equals(mCurrentSocialMedia.get(j))){
                    found = true;
                }
            }
            if (!found){
                resultList.add(mAllSociaLMedia.get(i));
            }
        }

        for (int i = 0; i<resultList.size();i ++){

            resultList.set(i, resultList.get(i).replace("_", " "));
            resultList.set(i,toTitleCase(resultList.get(i)));


        }
        return resultList.toArray(new CharSequence[resultList.size()]);
    }

    /**
     *
     * @param icon  the imageView which was clicked
     * @param account - text in the edit text
     * @param dareEnabled - if this social media will show up on a DareInterfaceActivity
     * @param profileEnabled -  if this social media will show up on a public prpfile
     * @param resourceId - resource of the icon picked form dialog
     * @param accountHint -
     * @param permisionReplacementString
     * @param position
     */
    public void setUpCurrentSocialMedia(ImageView icon, EditText account, CheckBox dareEnabled, CheckBox profileEnabled, int resourceId, String accountHint, String permisionReplacementString, int position){
        icon.setImageResource(resourceId);
        account.setText("");
        account.setHint(accountHint);
        dareEnabled.setChecked(false);
        profileEnabled.setChecked(false);
        mCurrentSocialMedia.set(position,permisionReplacementString);

        Log.d("CURRENT SOCIAL MEDIA", mCurrentSocialMedia.toString());
    }

    public static String toTitleCase(String input) {
        StringBuilder titleCase = new StringBuilder();
        boolean nextTitleCase = true;

        for (char c : input.toCharArray()) {
            if (Character.isSpaceChar(c)) {
                nextTitleCase = true;
            } else if (nextTitleCase) {
                c = Character.toTitleCase(c);
                nextTitleCase = false;
            }

            titleCase.append(c);
        }

        return titleCase.toString();
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.e(TAG, "ON DESTROY");
    }

    public void editProfileClicked(){
        //saving occurs here
        String[] dareEntitlements = mDareEntitlements.toArray(new String[mDareEntitlements.size()]);
        String[] profileEntitlements = mProfileEntitlements.toArray(new String[mProfileEntitlements.size()]);

        String [] currentSocialMedia = mCurrentSocialMedia.toArray(new String[mCurrentSocialMedia.size()]);
        mPresenter.storeProfile(mInstagramAccount.getText(), mSnapChatAdcount.getText(), mTwitterAccount.getText(), mWhatsAppAccount.getText(), mAboutMe.getText(), mOtherSm.getText(), dareEntitlements, profileEntitlements,currentSocialMedia );
        LauncherActivity launcherActivity = (LauncherActivity) getActivity();
        launcherActivity.mFirstTimeLogin=false;
        launcherActivity.unLockScreen();
    }

    public boolean shouldILockScreen(){
        if (mDareEntitlements!=null && mProfileEntitlements!=null){
            String[] dareEntitlements = mDareEntitlements.toArray(new String[mDareEntitlements.size()]);
            String[] profileEntitlements = mProfileEntitlements.toArray(new String[mProfileEntitlements.size()]);
            if  ((profileEntitlements.length==0) ||  (TextUtils.isEmpty(mInstagramAccount.getText()) && TextUtils.isEmpty(mSnapChatAdcount.getText()) && TextUtils.isEmpty(mWhatsAppAccount.getText()) && TextUtils.isEmpty(mOtherSm.getText()) &&  TextUtils.isEmpty(mTwitterAccount.getText()))){
                return true;
            }else{
                return false;
            }
        }else{
            return true;
        }

    }

    public void showDialog(String alertMessage){
        final AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setMessage(alertMessage);
        alertDialogBuilder.setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface arg0, int arg1) {
//                                LauncherActivity launcherActivity = (LauncherActivity) getActivity();
//                                launcherActivity.cancelSave();
//                                launcherActivity.mViewPager.setPagingEnabled(false);
                            }
                        });

        alertDialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
//                        LauncherActivity launcherActivity = (LauncherActivity) getActivity();
//                        launcherActivity.cancelSave();
//                        launcherActivity.mViewPager.setPagingEnabled(false);
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }
}