package com.example.warefhaque.dapeandroid.Nearby;

import android.os.Handler;
import android.os.Looper;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * Created by warefhaque on 1/7/17.
 */

public class NearbyPresenter implements NearbyInterface.Presenter{
    public static final String TAG = "Nearby Presenter";
    NearbyInterface.View mView;
    private Handler mHandler;

    public NearbyPresenter(NearbyInterface.View view){
        Log.d(TAG, "Declared the presenter");
        mView = view;
        mHandler = new Handler(Looper.getMainLooper());
    }

    @Override
    public void fetchPlacesNearby(String url) {

        OkHttpClient okHttpClient = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();

        okHttpClient.newCall(request).enqueue(new Callback() {
            @Override
            public void onFailure(Call call, final IOException e) {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mView.fetchFailure(e.getMessage());
                    }
                });

            }

            @Override
            public void onResponse(Call call, final Response response) throws IOException {

                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        if (response.isSuccessful()){
                            String jsonData = null;
                            try {
                                jsonData = response.body().string();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                            Log.d(TAG, jsonData);
                            try {
                                JSONObject places = new JSONObject(jsonData);
                                final JSONArray placesArray = places.getJSONArray("results");
                                mView.fetchSuccess(placesArray);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }else{
                            mView.fetchFailure(response.message());
                        }
                    }
                });

            }
        });
    }

    /**
     * Take the string ids from the nearby fragment static list of places ids and make
     * HTTP requests to fetch the place details.
     */

    @Override
    public void fetchPlacesList() {
        final JSONArray jsonArray= new JSONArray();
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                Log.d(TAG, "executing the fetch of places");
                if (NearbyFragment.localLikelyPlaces.size()>0){
                    for (int i = 0; i < NearbyFragment.localLikelyPlaces.size();i++){

                        final OkHttpClient client = new OkHttpClient();
                        StringBuilder sb = new StringBuilder("https://maps.googleapis.com/maps/api/place/details/json?");

                        if (NearbyFragment.localLikelyPlaces.size()>0){
                            sb.append("placeid=").append(NearbyFragment.localLikelyPlaces.get(i).getId());
                            sb.append("&key=AIzaSyA8TUEVYgazWsn6scxuMBJ51JwLg_uOLrg");

                            Request request = new Request.Builder()
                                    .url(sb.toString())
                                    .build();

                            Log.d(TAG,sb.toString());
                            try {
                                Response response =client.newCall(request).execute();
                                if (response.isSuccessful())
                                {
                                    String jsonData = response.body().string();
                                    JSONObject place = new JSONObject(jsonData);
                                    jsonArray.put(i,place);
                                }else{
                                    Log.d(TAG, "Response failed");
                                }
                            } catch (IOException | JSONException e) {
                                e.printStackTrace();
                            }
                        }

                    }
                }
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        mView.fetchSuccess(jsonArray);
                    }
                });

            }
        };
        Thread thread = new Thread(runnable);
        thread.start();
    }
}
