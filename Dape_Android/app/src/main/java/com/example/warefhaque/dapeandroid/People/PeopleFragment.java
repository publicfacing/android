package com.example.warefhaque.dapeandroid.People;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.GridView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.LauncherActivity;
import com.example.warefhaque.dapeandroid.MyPushNotificationReceiver;
import com.example.warefhaque.dapeandroid.R;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warefhaque on 1/14/17.
 */

public class PeopleFragment extends Fragment implements PeopleInterface.View, MyPushNotificationReceiver.PeopleNotificationListener{
    public static final String TAG = "People Fragment";
    public PeopleInterface.Presenter mPresenter;
    List<ParseUser> mAdapterList;
    PeopleAdapter mPeopleAdapter;
    ListView mPeopleListView;
    ArrayList<String> mUserIds;
    GridView mGridView;
    SwipeRefreshLayout swipeRefreshLayout;
    TextView mNoEvents;
    public FloatingActionButton mGridButton;



    public  void setPresenter(PeopleInterface.Presenter presenter){
        mPresenter = presenter;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View root = inflater.inflate(R.layout.fragment_people,container,false);


        MyPushNotificationReceiver.peopleNotificationListener = this;

        mPeopleAdapter  = new PeopleAdapter(getActivity());
        mPeopleListView = (ListView) root.findViewById(R.id.people_list);
        mNoEvents = (TextView) root.findViewById(R.id.no_events);
        mGridButton = (FloatingActionButton) root.findViewById(R.id.grid_button);
        swipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.ptr_people);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                if (mPresenter!=null){
                    mPresenter.fetchPeopleCheckedIn();
                }
                swipeRefreshLayout.setRefreshing(false);
            }
        });

        mUserIds = new ArrayList<>();
        mPeopleListView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent =new Intent(getActivity(), FragmentStatePagerSupport.class);
                intent.putStringArrayListExtra("userIds", mUserIds);
                intent.putExtra("position",position);
                startActivity(intent);

            }
        });
        if (mPeopleAdapter!=null){
            mPeopleListView.setAdapter(mPeopleAdapter);
        }else{
            Log.e(TAG, "People Adapter null!");
        }

        mGridView = (GridView) root.findViewById(R.id.gridview);
        mGridView.setAdapter(new ImageAdapter(getActivity()));

        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View v,
                                    int position, long id) {
                Intent intent =new Intent(getActivity(), FragmentStatePagerSupport.class);
                intent.putStringArrayListExtra("userIds", mUserIds);
                intent.putExtra("position",position);
                startActivity(intent);
            }
        });

        mGridButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (mPeopleListView.getVisibility() == View.VISIBLE){
                    mPeopleListView.setVisibility(View.GONE);
                    mGridView.setVisibility(View.VISIBLE);
                    mGridButton.setImageResource(R.drawable.bulleted_list);
                }else if (mGridView.getVisibility() == View.VISIBLE){
                    mPeopleListView.setVisibility(View.VISIBLE);
                    mGridView.setVisibility(View.GONE);
                    mGridButton.setImageResource(R.drawable.grid);
                }
            }
        });

        //OnCreate view ends here
        return root;
    }

    @Override
    public void onResume() {
        super.onResume();
        if (mPresenter!=null){
            try {
                ParseUser.getCurrentUser().fetchIfNeeded();

                ParseObject place;
                place = ParseUser.getCurrentUser().getParseObject("checkedIn");
                if (place!=null){
                    place.fetchIfNeeded();
                    setUiState(UIState.CHECKED_IN);
                    mPresenter.fetchPeopleCheckedIn();

                }else{
                    setUiState(UIState.CHECKED_OUT);
                }

            } catch (ParseException e) {
                e.printStackTrace();
            }

        }
    }

    @Override
    public void fetchedCheckedInPeople(List<ParseUser> parseUserList) {
        //update the userids as well.
        if (parseUserList.size()==0){
            setUiState(UIState.NO_USERS_CHECKED_IN);
        }else{
            setUiState(UIState.CHECKED_IN);
            mUserIds.clear();
            for (ParseUser user: parseUserList){
                mUserIds.add(user.getObjectId());
            }
            mAdapterList.clear();
            mAdapterList = parseUserList;
            mPeopleAdapter.notifyDataSetChanged();
        }

    }

    @Override
    public void failedfetch(boolean networkError) {
        if (networkError){
            Toast.makeText(getActivity(),"Network error: Please check your internet connection",Toast.LENGTH_SHORT).show();
        }else{
            setUiState(UIState.CHECKED_OUT);
        }
    }

    @Override
    public void updatePeopleList(ParseObject place) {

//    call it here with a location mPresenter.fetchPeopleCheckedIn();
//        mPresenter.fetchPeopleCheckedIn(place);
        LauncherActivity launcherActivity = (LauncherActivity) getActivity();
        launcherActivity.mViewPager.setCurrentItem(1);

    }

    @Override
    public void upDatePeopleList() {
        mPresenter.fetchPeopleCheckedIn();
    }

    /**
     * Adapter for the showing list of people starts here
     */
    private class PeopleAdapter extends BaseAdapter{

        Context mContext;
        public PeopleAdapter(Context context){
            mContext = context;
            mAdapterList = new ArrayList<>();
        }
        @Override
        public int getCount() {
            return mAdapterList.size();
        }

        @Override
        public ParseUser getItem(int position) {
            return mAdapterList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            PeopleListItem peopleListItem;
            if (convertView == null) {
                peopleListItem = new PeopleListItem();
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                convertView = peopleListItem.PeopleListItem(inflater,parent,mContext, mUserIds, position);
                convertView.setTag(peopleListItem);
            }else{
                peopleListItem = (PeopleListItem) convertView.getTag();
            }

            peopleListItem.setUpViews(mAdapterList.get(position),mContext);
            return convertView;
        }

    }

    /**
     * Adapter for the grid view starts here
     */

    public class ImageAdapter extends BaseAdapter {
        private Context mContext;

        public ImageAdapter(Context c) {
            mContext = c;
        }

        public int getCount() {
            return mAdapterList.size();
        }

        public PeopleGridItem getItem(int position) {
            return null;
        }

        public long getItemId(int position) {
            return position;
        }

        // create a new ImageView for each item referenced by the Adapter
        public View getView(int position, View convertView, ViewGroup parent) {
            View rowView = convertView;
            if (rowView == null) {
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
               PeopleGridItem peopleGridItem  = new PeopleGridItem();
                rowView = peopleGridItem.PeopleGridItem(inflater, mContext,  mAdapterList.get(position),parent);
            }

            return rowView;
        }

    }

    public void setUiState(UIState state){
        switch (state){
            case CHECKED_IN:
                mGridButton.setVisibility(View.VISIBLE);
                mGridView.setVisibility(View.GONE);
                mPeopleListView.setVisibility(View.VISIBLE);
                mNoEvents.setVisibility(View.GONE);
                break;
            case CHECKED_OUT:
                mGridButton.setVisibility(View.GONE);
                mGridView.setVisibility(View.GONE);
                mPeopleListView.setVisibility(View.GONE);
                mNoEvents.setVisibility(View.VISIBLE);
                mNoEvents.setText("Have a look in places list then check in to see people around you!");
                break;
            case NO_USERS_CHECKED_IN:
                mGridButton.setVisibility(View.GONE);
                mGridView.setVisibility(View.GONE);
                mPeopleListView.setVisibility(View.GONE);
                mNoEvents.setVisibility(View.VISIBLE);
                mNoEvents.setText("Kind of empty here");
                break;
        }

    }

    public enum UIState{
        CHECKED_IN,
        CHECKED_OUT,
        NO_USERS_CHECKED_IN
    }

    public static void upDatePeopleListNotification(){

    }



}
