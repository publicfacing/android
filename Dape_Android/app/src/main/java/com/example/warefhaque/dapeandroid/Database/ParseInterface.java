package com.example.warefhaque.dapeandroid.Database;

/**
 * Created by warefhaque on 12/14/16.
 *
 * Description: Contains the list of interaces and methods for accessing the DB and getting callbacks
 */


public interface ParseInterface {
    /**
     *Implemented by: ParseAuthentication
     *Called by: LoginPresenter
     */
    interface authentication{
        void getUserDetailsFb();
        void getUserDetailsParse();
    }

    /**
     *Implemented by: LoginPresenter
     *Called by:  ParseAuthentication
     */
    interface authenticationCallBack{
        //the new user signup was successful
        void newUserSaveSuccessful(String name);
        void userRetrievalSuccessfull(String name);
        void userRetrievalFailed(String error);
        void newUserSignupFailed(String error);
        // retrieval was successful
    }

}
