package com.example.warefhaque.dapeandroid.Layer;

import android.app.Application;
import android.content.Context;
import android.util.Log;

import com.layer.atlas.util.picasso.requesthandlers.MessagePartRequestHandler;
import com.layer.sdk.LayerClient;
import com.layer.sdk.messaging.Message;
import com.layer.sdk.messaging.MessagePart;
import com.squareup.picasso.Picasso;

import java.io.UnsupportedEncodingException;
import java.text.SimpleDateFormat;
import java.util.List;

/**
 * Created by warefhaque on 2/19/17.
 */

public class LayerImpl {
    public static final String TAG = "Layer Impl";
    //You must set your Layer App ID. If you haven't already, create a Layer account at
    // http://layer.com/signup, then follow these instructions:
    // 1. Go to http://developer.layer.com to sign in
    // 2. Open the Dashboard, and select your app from the dropdown on the top left
    // 3. Click "Info" in the left panel
    // 4. Copy your Staging App ID and paste it here:
    private static String LayerAppID = "layer:///apps/staging/c1176e86-e785-11e6-afcf-c4196c035e12";
    private static Picasso sPicasso;
    private static Application sInstance;



    //The LayerClient object. There should only be one instance of this object in your app.
    private static LayerClient mLayerClient;

    //THe callback handlers. Whenever the state changes, the callback will call a function in the
    // current Activity
    private static MyConnectionListener connectionListener;
    private static MyAuthenticationListener authenticationListener;


    //Merely checks to see if you have updated the App ID. If the App ID is entered incorrectly,
    // the LayerClient will fail to initialize
    public static boolean hasValidAppID(){
        return !LayerAppID.equals("layer:///apps/staging/c1176e86-e785-11e6-afcf-c4196c035e12");

    }

    //Called when any Activity is created to make sure the LayerClient is created, the callbacks
    // are registered, and the LayerClient is connected
    public static void initialize(Context context, Application application){
        sInstance= application;
        if(mLayerClient == null){
            LayerClient.Options options = new LayerClient.Options();
            options.useFirebaseCloudMessaging(true);
            options.broadcastPushInForeground(true);
            mLayerClient = LayerClient.newInstance(context, LayerAppID, options);
        }

        if(connectionListener == null) {
            connectionListener = new MyConnectionListener();
            mLayerClient.registerConnectionListener(connectionListener);
        }
        if(authenticationListener == null) {
            authenticationListener = new MyAuthenticationListener();
            mLayerClient.registerAuthenticationListener(authenticationListener);
        }
        Log.d(TAG, "checking if connected...");
        if(!mLayerClient.isConnected()){
            Log.d(TAG, "Calling Connect --");
            connectClient();
        }
    }

    //Connects to the Layer service
    public static void connectClient(){
        if(mLayerClient != null)
            mLayerClient.connect();
    }

    //Starts the Authentication process. The actual User registration happens in the
    // MyAuthenticationListener callbacks
    public static void authenticateUser(){
        if(mLayerClient != null)
            mLayerClient.authenticate();
        else{
            Log.e("AUTHENTICATE USER","LAYER CLIENT NULL!--CANNOT AUTHENTICATE");
        }
    }

    //Returns true if the LayerClient exists and is connected to the web service
    public static boolean isConnected(){

        if(mLayerClient != null)
            return mLayerClient.isConnected();

        return false;
    }

    //Returns true if the LayerClient exists and a user has been authenticated successfully
    public static boolean isAuthenticated(){

        if(mLayerClient != null)
            return mLayerClient.isAuthenticated();

        return false;
    }

    //When an Activity comes to the foreground (onResume), we want that Activity to handle any
    // callbacks
    public static void setContext(LayerCallbacks callbacks){

        if(connectionListener != null)
            connectionListener.setActiveContext(callbacks);

        if(authenticationListener != null)
            authenticationListener.setActiveContext(callbacks);
    }

    //Returns the App ID set by the developer
    public static String getLayerAppID(){
        return LayerAppID;
    }

    //Returns the actual LayerClient object
    public static LayerClient getLayerClient(){
        return mLayerClient;
    }

    //A helper function that takes a Message Object and returns the String contents of its data.
    // In this implementation, we make the assumption that all messages are text, but this could
    // be changed to handle any other file types (images, audio, JSON, etc)
    public static String getMessageText(Message msg){

        //The message content that will be returned
        String msgContent = "";
        if(msg != null){

            //Iterate through all the messasge parts, if the mime type indicates it is text, decode
            // it and add it to the string that will be returned
            List<MessagePart> parts = msg.getMessageParts();
            for(MessagePart part : parts){
                try {

                    if(part.getMimeType().equals("text/plain"))
                        msgContent += new String(part.getData(), "UTF-8") + "\n";

                } catch(UnsupportedEncodingException e){

                }
            }
        }

        return msgContent;
    }

    //A helper function that takes a Message object and returns a String representation of the
    // ReceivedAt time (the local time the message was downloaded to the device)
    public static String getReceivedAtTime(Message msg){
        String dateString = "";
        if(msg != null && msg.getReceivedAt() != null) {
            SimpleDateFormat format = new SimpleDateFormat("M/dd hh:mm:ss");
            dateString = format.format(msg.getReceivedAt());
        }
        return dateString;
    }
    public static Picasso getPicasso() {
        if (sPicasso == null) {
            // Picasso with custom RequestHandler for loading from Layer MessageParts.
            sPicasso = new Picasso.Builder(sInstance)
                    .addRequestHandler(new MessagePartRequestHandler(getLayerClient()))
                    .build();
        }
        return sPicasso;
    }


}

