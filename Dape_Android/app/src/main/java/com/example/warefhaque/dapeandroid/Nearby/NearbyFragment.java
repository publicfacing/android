package com.example.warefhaque.dapeandroid.Nearby;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.LauncherActivity;
import com.example.warefhaque.dapeandroid.LoadingDialog;
import com.example.warefhaque.dapeandroid.People.PeopleInterface;
import com.example.warefhaque.dapeandroid.R;
import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.PendingResult;
import com.google.android.gms.common.api.ResultCallback;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.PlaceFilter;
import com.google.android.gms.location.places.PlaceLikelihood;
import com.google.android.gms.location.places.PlaceLikelihoodBuffer;
import com.google.android.gms.location.places.Places;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.location.LocationListener;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;

import org.json.JSONArray;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;

/**
 * Created by warefhaque on 12/18/16.
 */

public class NearbyFragment extends Fragment implements
        OnMapReadyCallback,
        GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener,
        LocationListener,
        NearbyInterface.mapViewCallbackListener{
    public double mThreshold = 0.0;
    public static final String TAG ="Nearby Fragment";

    public static int TYPE_ART_GALLERY = 5;
    public static final int TYPE_BAR = 9;
    public static final int TYPE_CAFE = 15;
    public static final int TYPE_CASINO = 21;
    public static final int TYPE_CHURCH  = 23;
    public static final int TYPE_GYM = 44;
    public static final int TYPE_LIBRARY = 55;
    public static final int TYPE_MOSQUE = 62;
    public static final int TYPE_MUSEUM = 66;
    public static final int TYPE_NIGHT_CLUB = 67;
    public static final int TYPE_RESTAURANT = 79;
    public static final int TYPE_SCHOOL = 82;
    public static final int TYPE_SYNAGOGUE = 90;
    public static final int TYPE_UNIVERSITY = 94;

    public NearbyInterface.Presenter mPresenter;
    public PeopleInterface.View mPeopleFragment;

    public LoadingDialog mLoadingDialog;
    private MapView mMapView;
    private GoogleMap mGoogleMap;
    public static GoogleApiClient mGoogleApiClient;
    private boolean mLocationPermissionGranted;
    private static final int PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION = 1;
    public static Location mCurrentLocation;
    // A request object to store parameters for requests to the FusedLocationProviderApi.
    private LocationRequest mLocationRequest;
    private CameraPosition mCameraPosition;
    // Keys for storing activity state.
    private static final String KEY_CAMERA_POSITION = "camera_position";
    private static final String KEY_LOCATION = "location";
    // The desired interval for location updates. Inexact. Updates may be more or less frequent.
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;
    // The fastest rate for active location updates. Exact. Updates will never be more frequent
    // than this value.
    private static final long FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS =
            UPDATE_INTERVAL_IN_MILLISECONDS / 2;

    private static final int DEFAULT_ZOOM = 17;
    // A default location (Sydney, Australia) and default zoom to use when location permission is
    // not granted.
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);

    public static JSONArray localPlacesCopy;
    public static List<com.google.android.gms.location.places.Place> localLikelyPlaces;
    public RelativeLayout mMapLayout;
    public LinearLayout mListLayout;
    private List<PlaceLikelihood> localLikelyPlaceLikelihood;
    public FloatingActionButton mListButton;
    public NearbyListFragment mListFragment;
    public HashSet<Integer> mValidPlaceTypes;

    //list of placeIds for the list fetch
//    public static List<String> placeIds;

    public void setPresenter(NearbyInterface.Presenter presenter){
        mPresenter = presenter;
    }

    public void setUpPeopleFragment(PeopleInterface.View peopleFragment){
        mPeopleFragment = peopleFragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(TAG,"onCreate!");
        localLikelyPlaces = new ArrayList<>();
        localLikelyPlaceLikelihood = new ArrayList<>();

        mValidPlaceTypes = new HashSet<>();
        mValidPlaceTypes.add(TYPE_ART_GALLERY);
        mValidPlaceTypes.add(TYPE_BAR);
        mValidPlaceTypes.add(TYPE_CAFE);
        mValidPlaceTypes.add(TYPE_CASINO);
        mValidPlaceTypes.add(TYPE_CHURCH);
        mValidPlaceTypes.add(TYPE_GYM);
        mValidPlaceTypes.add(TYPE_MOSQUE);
        mValidPlaceTypes.add(TYPE_MUSEUM);
        mValidPlaceTypes.add(TYPE_LIBRARY);
        mValidPlaceTypes.add(TYPE_NIGHT_CLUB);
        mValidPlaceTypes.add(TYPE_RESTAURANT);
        mValidPlaceTypes.add(TYPE_SCHOOL);
        mValidPlaceTypes.add(TYPE_SYNAGOGUE);
        mValidPlaceTypes.add(TYPE_UNIVERSITY);

//        placeIds = new ArrayList<>();
        // Retrieve location and camera position from saved instance state.
        if (savedInstanceState != null) {
            mCurrentLocation = savedInstanceState.getParcelable(KEY_LOCATION);
            mCameraPosition = savedInstanceState.getParcelable(KEY_CAMERA_POSITION);
        }
        // TODO: 1/7/17 not retrieving the content view here but counting on the onCreateView to do it!!
        // Build the Play services client for use by the Fused Location Provider and the Places API.
        buildGoogleApiClient();

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        Log.d(TAG,"onCreateView");
        View view = inflater.inflate(R.layout.fragment_nearby, container, false);

//        setHasOptionsMenu(true);
        mMapLayout = (RelativeLayout) view.findViewById(R.id.map_layout);
        mListLayout = (LinearLayout) view.findViewById(R.id.list_layout);


        //TODO: code unnecessary if the setOffScreenLimit is 2-3
        if (mListFragment==null){
            mListFragment = new NearbyListFragment();
            mListFragment.setMapViewCallback(this);
            NearbyPresenter nearbyPresenter = new NearbyPresenter(mListFragment);
            mListFragment.setPresenter(nearbyPresenter);
        }
        if (mListFragment!=null){
            getChildFragmentManager().popBackStack();
        }
//        mListButton = (FloatingActionButton) view.findViewById(R.id.list_button);
//        mListButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                goToListView();
//            }
//        });
        mMapView = (MapView) view.findViewById(R.id.map);
        mMapView.onCreate(savedInstanceState);


        mMapView.onResume();

        try {
            MapsInitializer.initialize(getActivity().getApplicationContext());
        } catch (Exception e) {
            e.printStackTrace();
        }

        mGoogleApiClient.connect();
        getDeviceLocation();

        return view;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
    }

    public void goToListView(){
        mListLayout.setVisibility(View.VISIBLE);
        mMapLayout.setVisibility(View.GONE);
        android.support.v4.app.FragmentTransaction fragmentTransaction = getChildFragmentManager().beginTransaction();

        fragmentTransaction.replace(R.id.list_layout, mListFragment,"NearbyListFragment");
        fragmentTransaction.addToBackStack("NearbyListFragment");
        fragmentTransaction.commit();
//
//        mListButton.setVisibility(View.GONE);
//        mMapLayout.setVisibility(View.GONE);
    }

    @Override
    public void onResume() {
        super.onResume();
        mMapView.onResume();
        if (mGoogleApiClient.isConnected()){
            getDeviceLocation();
        }
        updateMarkers();
    }

    @Override
    public void onPause() {
        super.onPause();
        mMapView.onPause();
        if (mGoogleApiClient.isConnected()) {
            // TODO: 1/7/17 WARNING: CASTING DONE HERE. MIGHT CRASH
            LocationServices.FusedLocationApi.removeLocationUpdates(
                    mGoogleApiClient, this);
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        mMapView.onDestroy();
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
        mMapView.onLowMemory();
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {

    }

    /**
     * Handles the callback when location changes.
     */
    @Override
    public void onLocationChanged(Location location) {
        mCurrentLocation = location;
        updateMarkers();
    }

    /**
     * Saves the state of the map when the activity is paused.
     */
    @Override
    public void onSaveInstanceState(Bundle outState) {
        if (mGoogleMap != null) {
            outState.putParcelable(KEY_CAMERA_POSITION, mGoogleMap.getCameraPosition());
            outState.putParcelable(KEY_LOCATION, mCurrentLocation);
            super.onSaveInstanceState(outState);
        }
    }
    /**
     * Gets the device's current location and builds the map
     * when the Google Play services client is successfully connected.
     */
    @Override
    public void onConnected(@Nullable Bundle bundle) {
        getDeviceLocation();

        if (mMapView!=null){
            mMapView.getMapAsync(new OnMapReadyCallback() {
                @Override
                public void onMapReady(GoogleMap googleMap) {
                    mGoogleMap = googleMap;
                    // Turn on the My Location layer and the related control on the map.
                    updateLocationUI();
                    // Add markers for nearby places.
                    updateMarkers();

                    // Use a custom info window adapter to handle multiple lines of text in the
                    // info window contents.
                    mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                        @Override
                        // Return null here, so that getInfoContents() is called next.
                        public View getInfoWindow(Marker arg0) {
                            return null;
                        }

                        @Override
                        public View getInfoContents(final Marker marker) {
                            // Inflate the layouts for the info window, title and snippet.
                            View infoWindow = getActivity().getLayoutInflater().inflate(R.layout.custom_info_contents, null);

                            TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                            title.setText(marker.getTitle());

                            TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                            snippet.setText(marker.getSnippet());

                            infoWindow.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {

                                }
                            });

                            return infoWindow;
                        }
                    });

                    mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                        @Override
                        public void onInfoWindowClick(Marker marker) {
                            Place place = null;
                            NearbyListFragment.DemoPlace checkInPlace = null;
//                            Toast.makeText(getActivity(),marker.getSnippet(),Toast.LENGTH_SHORT).show();
                            for (int i = 0; i < NearbyFragment.localLikelyPlaces.size(); i++) {
                                Log.d(TAG, "____________---------------------------------------------------------________________");
                                Log.d(TAG, NearbyFragment.localLikelyPlaces.get(i).toString());
                                Log.d(TAG, marker.toString());
                                if (NearbyFragment.localLikelyPlaces.get(i).getName().equals(marker.getTitle())) {
                                    place = NearbyFragment.localLikelyPlaces.get(i);
                                    checkInPlace = new NearbyListFragment.DemoPlace(place.getName().toString(), place.getAddress().toString(), place.getLatLng().latitude, place.getLatLng().longitude, place.getId());
                                }
                            }

                            if (place != null) checkInUser(checkInPlace);
                            else
                                Toast.makeText(getActivity(), "Error: Unable to check you in. Please try again or check-in from the list view.", Toast.LENGTH_SHORT).show();


                        }
                    });
        /*
         * Set the map's camera position to the current location of the device.
         * If the previous state was saved, set the position to the saved state.
         * If the current location is unknown, use a default position and zoom value.
         */
                    if (mCameraPosition != null) {
                        mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
                    } else if (mCurrentLocation != null) {
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                new LatLng(mCurrentLocation.getLatitude(),
                                        mCurrentLocation.getLongitude()), DEFAULT_ZOOM));
                    } else {
                        Log.d(TAG, "Current location is null. Using defaults.");
                        mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                        mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                    }
                }
            });


        }else{
            Log.d(TAG,"Uh Oh, mMapView is null in onConnected!");
        }

    }


    /**
     * Handles suspension of the connection to the Google Play services client.
     */
    @Override
    public void onConnectionSuspended(int cause) {
        Log.d(TAG, "Play services connection suspended");
    }
    /**
     * Handles failure to connect to the Google Play services client.
     */
    @Override
    public void onConnectionFailed(@NonNull ConnectionResult result) {
        // Refer to the reference doc for ConnectionResult to see what error codes might
        // be returned in onConnectionFailed.
        Log.d(TAG, "Play services connection failed: ConnectionResult.getErrorCode() = "
                + result.getErrorCode());
    }
    private synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(getActivity())
                .enableAutoManage(getActivity() /* FragmentActivity */,
                        this /* OnConnectionFailedListener */)
                .addConnectionCallbacks(this)
                .addApi(LocationServices.API)
                .addApi(Places.GEO_DATA_API)
                .addApi(Places.PLACE_DETECTION_API)
                .build();
        createLocationRequest();
    }

    private void createLocationRequest(){
        mLocationRequest = new LocationRequest();

        /*
         * Sets the desired interval for active location updates. This interval is
         * inexact. You may not receive updates at all if no location sources are available, or
         * you may receive them slower than requested. You may also receive updates faster than
         * requested if other applications are requesting location at a faster interval.
         */
        mLocationRequest.setInterval(UPDATE_INTERVAL_IN_MILLISECONDS);

        /*
         * Sets the fastest rate for active location updates. This interval is exact, and your
         * application will never receive updates faster than this value.
         */
        mLocationRequest.setFastestInterval(FASTEST_UPDATE_INTERVAL_IN_MILLISECONDS);

        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

    }
    private void getDeviceLocation() {
        /*
         * Request location permission, so that we can get the location of the
         * device. The result of the permission request is handled by a callback,
         * onRequestPermissionsResult.
         */
        if (ContextCompat.checkSelfPermission(getActivity().getApplicationContext(),
                android.Manifest.permission.ACCESS_FINE_LOCATION)
                == PackageManager.PERMISSION_GRANTED) {
            mLocationPermissionGranted = true;
        } else {
            ActivityCompat.requestPermissions(getActivity(),
                    new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION},
                    PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION);
        }
        /*
         * Get the best and most recent location of the device, which may be null in rare
         * cases when a location is not available.
         * Also request regular updates about the device location.
         */
        if (mGoogleApiClient.isConnected()){
            if (mLocationPermissionGranted) {
                mCurrentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
                LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient,
                        mLocationRequest, this);


                if (mMapView!=null){
                    mMapView.getMapAsync(new OnMapReadyCallback() {
                        @Override
                        public void onMapReady(GoogleMap googleMap) {
                            mGoogleMap = googleMap;
                            mGoogleMap.setMinZoomPreference(15f);
                            // Turn on the My Location layer and the related control on the map.
                            updateLocationUI();
                            // Add markers for nearby places.
                            updateMarkers();

                            // Use a custom info window adapter to handle multiple lines of text in the
                            // info window contents.
                            mGoogleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

                                @Override
                                // Return null here, so that getInfoContents() is called next.
                                public View getInfoWindow(Marker arg0) {
                                    return null;
                                }

                                @Override
                                public View getInfoContents(final Marker marker) {
                                    // Inflate the layouts for the info window, title and snippet.
                                    View infoWindow = getActivity().getLayoutInflater().inflate(R.layout.custom_info_contents, null);

                                    TextView title = ((TextView) infoWindow.findViewById(R.id.title));
                                    title.setText(marker.getTitle());

                                    TextView snippet = ((TextView) infoWindow.findViewById(R.id.snippet));
                                    snippet.setText(marker.getSnippet());

                                    infoWindow.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {

                                        }
                                    });

                                    return infoWindow;
                                }
                            });

                            mGoogleMap.setOnInfoWindowClickListener(new GoogleMap.OnInfoWindowClickListener() {
                                @Override
                                public void onInfoWindowClick(Marker marker) {
                                    Place place = null;
                                    NearbyListFragment.DemoPlace checkInPlace = null;
//                            Toast.makeText(getActivity(),marker.getSnippet(),Toast.LENGTH_SHORT).show();
                                    for (int i = 0; i < NearbyFragment.localLikelyPlaces.size(); i++) {
                                        Log.d(TAG, "____________---------------------------------------------------------________________");
                                        Log.d(TAG, NearbyFragment.localLikelyPlaces.get(i).toString());
                                        Log.d(TAG, marker.toString());
                                        if (NearbyFragment.localLikelyPlaces.get(i).getName().equals(marker.getTitle())) {
                                            place = NearbyFragment.localLikelyPlaces.get(i);
                                            checkInPlace = new NearbyListFragment.DemoPlace(place.getName().toString(), place.getAddress().toString(), place.getLatLng().latitude, place.getLatLng().longitude, place.getId());
                                        }
                                    }

                                    if (place != null) checkInUser(checkInPlace);
                                    else
                                        Toast.makeText(getActivity(), "Error: Unable to check you in. Please try again or check-in from the list view.", Toast.LENGTH_SHORT).show();


                                }
                            });
        /*
         * Set the map's camera position to the current location of the device.
         * If the previous state was saved, set the position to the saved state.
         * If the current location is unknown, use a default position and zoom value.
         */
                            if (mCameraPosition != null) {
                                mGoogleMap.moveCamera(CameraUpdateFactory.newCameraPosition(mCameraPosition));
                            } else if (mCurrentLocation != null) {
                                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(
                                        new LatLng(mCurrentLocation.getLatitude(),
                                                mCurrentLocation.getLongitude()), DEFAULT_ZOOM));
                            } else {
                                Log.d(TAG, "Current location is null. Using defaults.");
                                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(mDefaultLocation, DEFAULT_ZOOM));
                                mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
                            }
                        }
                    });
                }else{
                    Log.d(TAG,"Uh Oh, mMapView is null in onConnected!");
                }

            }
        }

    }

    /**
     * Handles the result of the request for location permissions.
     */
    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String permissions[],
                                           @NonNull int[] grantResults) {
        mLocationPermissionGranted = false;
        switch (requestCode) {
            case PERMISSIONS_REQUEST_ACCESS_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    mLocationPermissionGranted = true;
                }
            }
        }
        updateLocationUI();
    }

    /**
     * Adds markers for places nearby the device and turns the My Location feature on or off,
     * provided location permission has been granted.
     */
    private void updateMarkers() {
        if (mGoogleMap == null) {
            return;
        }

        if (mLocationPermissionGranted) {
            // Get the businesses and other points of interest located
            // nearest to the device's current location.

//           mPresenter.fetchPlacesNearby(sb.toString());
            List<String> filters = new ArrayList<>();
            filters.add("cafe");
            filters.add("night_club");
            filters.add("bar");
            PlaceFilter placeFilter = new PlaceFilter(false, filters);

            @SuppressWarnings("MissingPermission")
            PendingResult<PlaceLikelihoodBuffer> result = Places.PlaceDetectionApi
                    .getCurrentPlace(mGoogleApiClient,null);
            result.setResultCallback(new ResultCallback<PlaceLikelihoodBuffer>() {
                @Override
                public void onResult(@NonNull PlaceLikelihoodBuffer likelyPlaces) {
                    localLikelyPlaces.clear();
                    localLikelyPlaceLikelihood.clear();
                    mGoogleMap.clear();
                    for (PlaceLikelihood placeLikelihood : likelyPlaces) {
                        // Add a marker for each place near the device's current location, with an
                        // info window showing place information.
                        String attributions = (String) placeLikelihood.getPlace().getAttributions();

                        if (attributions!=null){
                            Log.i("attributions",attributions);
                        }
                        String snippet = (String) placeLikelihood.getPlace().getAddress();
                        if (attributions != null) {
                            snippet = snippet + "\n" + attributions;
                        }

                        if (placeLikelihood.getLikelihood()>mThreshold){

                            // freeze it otherwise it will be null when accessed later
                            PlaceLikelihood placeLikelihoodFrozen = placeLikelihood.freeze();

                            // checks if the place associated with it is allowed to be shown
                            if (isPlaceValid(placeLikelihoodFrozen))
                            {
                                mGoogleMap.addMarker(new MarkerOptions()
                                        .position(placeLikelihoodFrozen.getPlace().getLatLng())
                                        .title((String) placeLikelihoodFrozen.getPlace().getName())
                                        .snippet(snippet));

                                localLikelyPlaceLikelihood.add(placeLikelihoodFrozen);
                            }
                        }
                    }

                    Collections.sort(localLikelyPlaceLikelihood,new CustomComparator());
                    for (int i = localLikelyPlaceLikelihood.size()-1; i>=0; i--)
                    {
                        localLikelyPlaces.add( localLikelyPlaceLikelihood.get(i).getPlace());
                    }
                    // Release the place likelihood buffer.
                    likelyPlaces.release();

                }
            });
        } else {
            mGoogleMap.addMarker(new MarkerOptions()
                    .position(mDefaultLocation)
                    .title(getString(R.string.default_info_title))
                    .snippet(getString(R.string.default_info_snippet)));
        }
    }

    public boolean isPlaceValid(PlaceLikelihood placeLikelihood)
    {
        boolean isValid = false;

        List<Integer> placeTypes = placeLikelihood.getPlace().getPlaceTypes();

        for (int placeType : placeTypes)
        {
            if (mValidPlaceTypes.contains(placeType))
            {
                isValid = true;
            }
        }

        return isValid;
    }

    /**
     * Updates the map's UI settings based on whether the user has granted location permission.
     */
    @SuppressWarnings("MissingPermission")
    private void updateLocationUI() {
        if (mGoogleMap == null) {
            return;
        }

        if (mLocationPermissionGranted) {
            mGoogleMap.setMyLocationEnabled(true);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(true);
        } else {
            mGoogleMap.setMyLocationEnabled(false);
            mGoogleMap.getUiSettings().setMyLocationButtonEnabled(false);
            mCurrentLocation = null;
        }
    }


    @Override
    public void displayMapFromList() {
        mListLayout.setVisibility(View.GONE);
        mMapLayout.setVisibility(View.VISIBLE);
        if (mListFragment!=null){
            getChildFragmentManager().popBackStack();
        }


//        mListButton.setVisibility(View.VISIBLE);
    }


    public class CustomComparator implements Comparator<com.google.android.gms.location.places.PlaceLikelihood> {

        @Override
        public int compare(PlaceLikelihood o1, PlaceLikelihood o2) {

            return Float.compare(o1.getLikelihood(), o2.getLikelihood());

        }
    }

    /**
     * Cloud function
     */
    public void checkInUser(final NearbyListFragment.DemoPlace secondPlace)
    {

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle("Check-in");
        alertDialogBuilder.setMessage("Would you like to check-in to " + secondPlace.name + " ?");

        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                // call the cloud
                checkInCloudCall(secondPlace);
            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i)
            {
                //nothing
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    public void checkInCloudCall(NearbyListFragment.DemoPlace place)
    {
        HashMap<String, Object> params = new HashMap<>();

        params.put("place_id", place.id);
        params.put("place_name", place.name);

        params.put("place_address", place.address);
        params.put("place_location_lat", place.lat);
        params.put("place_location_lon", place.lng);
        params.put("place_types", "_type");
        params.put("user_id", "CvvuqRtTrH");
        params.put("user_location_lat",  NearbyFragment.mCurrentLocation.getLatitude());
        params.put("user_location_lon",NearbyFragment.mCurrentLocation.getLongitude());
//

        ParseCloud.callFunctionInBackground("check-in", params, new FunctionCallback<Object>() {
            public void done(Object data, ParseException e) {
                loadingIndicator(false);
                if (e == null)
                {
                    Log.d(TAG, data.toString());
                    Log.d("NearbyListFragment","Cloud Call worked!");
                    String dataString = (String) data;

                        Toast.makeText(getActivity(),"You've checked in!",Toast.LENGTH_SHORT).show();
                        LauncherActivity launcherActivity = (LauncherActivity) getActivity();

                        launcherActivity.startTimer();

                        //update the people checked in to the place.
                        launcherActivity.peopleFragment.mPresenter.fetchPeopleCheckedIn();
                        //move focus to the people fragment

                        launcherActivity.mViewPager.setCurrentItem(1);
                        //update the "Flow" screen

                        launcherActivity.messagesFragment.updateActivities();
                        launcherActivity.mTabCheckOutButton.setImageResource(R.drawable.check_action_bar);
                        launcherActivity.mTabCheckOutButton.setEnabled(true);

                }
                else
                {
                    Toast.makeText(getActivity(),"You must be within 30m of the place to check in. Please try again",Toast.LENGTH_SHORT).show();
                    Log.d(TAG, e.getMessage());

                }
            }
        });
    }

    public void loadingIndicator(boolean active) {
        if (mLoadingDialog==null){
            mLoadingDialog=new LoadingDialog(getActivity());
        }
        if (active){
            mLoadingDialog.show();
        }else{
            mLoadingDialog.dismiss();
        }
    }


}
