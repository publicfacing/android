package com.example.warefhaque.dapeandroid.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.widget.ImageView;

import com.example.warefhaque.dapeandroid.Nearby.NearbyFragment;
import com.example.warefhaque.dapeandroid.R;
import com.google.android.gms.location.places.PlacePhotoMetadata;
import com.google.android.gms.location.places.PlacePhotoMetadataBuffer;
import com.google.android.gms.location.places.PlacePhotoMetadataResult;
import com.google.android.gms.location.places.Places;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.Collections;
import java.util.Map;
import java.util.WeakHashMap;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * Created by warefhaque on 1/10/17.
 */

public class ImageLoader {
    public static final String TAG = "Image Loader";
    MemoryCache memoryCache=new MemoryCache();
    FileCache fileCache;
    private Map<ImageView, String> imageViews= Collections.synchronizedMap(new WeakHashMap<ImageView, String>());
    ExecutorService executorService;
    Context mContext;
    int bitmapWidth = 3285;
    int bitmapHeight = 1195;

    public ImageLoader(Context context){
        mContext = context;
        fileCache=new FileCache(context);
        executorService= Executors.newFixedThreadPool(5);
    }

    final int stub_id= R.drawable.placeholder;
    public void DisplayImage(String placeId, ImageView imageView)
    {
        imageViews.put(imageView, placeId);
        Bitmap bitmap=memoryCache.get(placeId);
        if(bitmap!=null){
//            Log.d(TAG, "LOADED FROM CACHE: "+placeId);
            imageView.setImageBitmap(bitmap);
        }
        else
        {
            queuePhoto(placeId, imageView);
            imageView.setImageResource(stub_id);
        }
    }

    private void queuePhoto(String placeId, ImageView imageView)
    {
        PhotoToLoad p=new PhotoToLoad(placeId, imageView);
        executorService.submit(new PhotosLoader(p));
    }

    private Bitmap getBitmap(String placeId)
    {
        File f=fileCache.getFile(placeId);

        //from SD cache
        Bitmap b = decodeFile(f);
        if(b!=null) {
//            Log.d(TAG, "FILE CACHE LOAD");
            return b;
        }

        //from web
//        try {
//            Bitmap bitmap=null;
//            URL imageUrl = new URL(placeId);
//            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
//            conn.setConnectTimeout(30000);
//            conn.setReadTimeout(30000);
//            conn.setInstanceFollowRedirects(true);
//            InputStream is=conn.getInputStream();
//            OutputStream os = new FileOutputStream(f);
//            Utils.CopyStream(is, os);
//            os.close();
//            bitmap = decodeFile(f);
//            return bitmap;
//        } catch (Throwable ex){
//            ex.printStackTrace();
//            if(ex instanceof OutOfMemoryError)
//                memoryCache.clear();
//            return null;
//        }
        // Get a PlacePhotoMetadataResult containing metadata for the first 10 photos.
        PlacePhotoMetadataResult result = Places.GeoDataApi
                .getPlacePhotos(NearbyFragment.mGoogleApiClient, placeId).await();

        // Get a PhotoMetadataBuffer instance containing a list of photos (PhotoMetadata).
        if (result.getPhotoMetadata().getCount()>0) {

            // Get the first photo in the list.
            PlacePhotoMetadataBuffer photoMetadataBuffer = result.getPhotoMetadata();
            PlacePhotoMetadata photo = photoMetadataBuffer.get(0);

            // Get a full-size bitmap for the photo.
            Bitmap image = photo.getScaledPhoto(NearbyFragment.mGoogleApiClient,100,100).await().getBitmap();
            photoMetadataBuffer.release();
            return image;

        }else{
//            Log.d(TAG, "PLACE HAS NO PHOTO: "+placeId);

            Bitmap bitmap = BitmapFactory.decodeResource(mContext.getResources(),R.drawable.restaurant);
            return bitmap;
        }

    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            BitmapFactory.decodeStream(new FileInputStream(f),null,o);

            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE=70;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            return BitmapFactory.decodeStream(new FileInputStream(f), null, o2);
        } catch (FileNotFoundException e) {}
        return null;
    }

    //Task for the queue
    private class PhotoToLoad
    {
        public String placeId;
        public ImageView imageView;
        public PhotoToLoad(String u, ImageView i){
            placeId=u;
            imageView=i;
        }
    }

    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;
        PhotosLoader(PhotoToLoad photoToLoad){
            this.photoToLoad=photoToLoad;
        }

        @Override
        public void run() {
            if(imageViewReused(photoToLoad))
                return;
            Bitmap bmp=getBitmap(photoToLoad.placeId);
            if (memoryCache.get(photoToLoad.placeId)==null){
                memoryCache.put(photoToLoad.placeId, bmp);
                Log.i(TAG, "Saving "+photoToLoad.placeId+"...");
            }

            if(imageViewReused(photoToLoad))
                return;
            BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);
            Activity a=(Activity)photoToLoad.imageView.getContext();
            a.runOnUiThread(bd);
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad){
        String tag=imageViews.get(photoToLoad.imageView);
        if(tag==null || !tag.equals(photoToLoad.placeId))
            return true;
        return false;
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;
        public BitmapDisplayer(Bitmap b, PhotoToLoad p){bitmap=b;photoToLoad=p;}
        public void run()
        {
            if(imageViewReused(photoToLoad))
                return;
            photoToLoad.imageView.setImageBitmap(bitmap);
//            if(bitmap!=null)
//                photoToLoad.imageView.setImageBitmap(bitmap);
//            else{
//                photoToLoad.imageView.setImageBitmap(decodeSampledBitmapFromResource(mContext.getResources(),R.drawable.placeholder,bitmapWidth,bitmapHeight));
//            }


        }
    }

    public void clearCache() {
        memoryCache.clear();
        fileCache.clear();
    }
    public static Bitmap decodeSampledBitmapFromResource(Resources res, int resId,
                                                         int reqWidth, int reqHeight) {

        // First decode with inJustDecodeBounds=true to check dimensions
        final BitmapFactory.Options options = new BitmapFactory.Options();
        options.inJustDecodeBounds = true;
        BitmapFactory.decodeResource(res, resId, options);

        // Calculate inSampleSize
        options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

        // Decode bitmap with inSampleSize set
        options.inJustDecodeBounds = false;
        return BitmapFactory.decodeResource(res, resId, options);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) >= reqHeight
                    && (halfWidth / inSampleSize) >= reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

}


