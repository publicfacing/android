package com.example.warefhaque.dapeandroid.People;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.warefhaque.dapeandroid.Profile.ProfilePresenter;
import com.example.warefhaque.dapeandroid.R;
import com.example.warefhaque.dapeandroid.Utils.AvatarImageView;
import com.parse.ParseException;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Date;

/**
 * Created by warefhaque on 1/14/17.
 */

public class PeopleListItem {

    public static final String TAG = "People List Item";
    TextView name;
    AvatarImageView profilePic;
    TextView aboutMe;
    TextView checkedInTime;
    ArrayList<String> mUserIds;
    int mPosition;
    public View PeopleListItem(LayoutInflater inflater, ViewGroup parent, final Context context, final ArrayList<String> userIds, final int position){

        View root = inflater.inflate(R.layout.people_list_item, parent, false);
        mUserIds = userIds;
        mPosition = position;
        name = (TextView) root.findViewById(R.id.flow_place_name);
        profilePic = (AvatarImageView) root.findViewById(R.id.flow_place_picture);
        aboutMe = (TextView) root.findViewById(R.id.flow_place_address);
        checkedInTime = (TextView) root.findViewById(R.id.last_seen);

        return root;
    }

    public void setUpViews(ParseUser parseUser, Context context){
        try {
            parseUser.fetchIfNeeded();
            name.setText(parseUser.getString("name"));
            aboutMe.setText(parseUser.getString("about"));
            ProfilePresenter.fetchPeopleImage(profilePic, parseUser, "profilePic", 90,90, context);
            Date checkedInDate = parseUser.getDate("lastCheckInTime");
            if (checkedInDate!=null){
                Date date = new Date();
                long diff = date.getTime() - checkedInDate.getTime();
                long seconds = diff / 1000;
                long minutes = seconds / 60;
                long hours = minutes / 60;
                long days = hours / 24;
                String base = "Checked in ";
                if (minutes<2){
                    checkedInTime.setText(base+"Just Now");
                }else{
                    checkedInTime.setText(base + minutes + " minutes ago");
                }

                if (minutes>30){
                    checkedInTime.setText(base + " over an hour ago");
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }
}
