package com.example.warefhaque.dapeandroid.People;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;

import com.example.warefhaque.dapeandroid.R;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by warefhaque on 1/17/17.
 */

// TODO: 4/28/17 NEEDS TO BE DELETED
public class FullScreenImageSlider extends FragmentActivity{
    public static final String TAG = "Full screen image";
    static int NUM_ITEMS ;
    static List<String> uris;

    MyAdapter mAdapter;

    ViewPager mPager;

    static ArrayList<String> mUserIds;

    int mpositionClicked;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_full_screen);

        uris = new ArrayList<>();
        mUserIds = getIntent().getStringArrayListExtra("userIds");
        mpositionClicked = getIntent().getIntExtra("position", 0);

        NUM_ITEMS = mUserIds.size();
        mAdapter = new MyAdapter(getSupportFragmentManager());

        mPager = (ViewPager)findViewById(R.id.image_pager);
        mPager.setAdapter(mAdapter);

        //set the position to the item in the list clicked
        mPager.setCurrentItem(mpositionClicked);
        // Watch for button clicks.
        FloatingActionButton button = (FloatingActionButton) findViewById(R.id.goto_first);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mPager.setCurrentItem(0);
            }
        });
        button = (FloatingActionButton)findViewById(R.id.goto_last);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mPager.setCurrentItem(NUM_ITEMS-1);
            }
        });
    }

    public static class MyAdapter extends FragmentStatePagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            return ArrayListFragment.newInstance(mUserIds.get(position));
        }
    }

    public static class ArrayListFragment extends Fragment {
        String idToFetch;
        ImageButton mBackButton;
        ImageView mFullScreenImage;


        /**
         * Create a new instance of CountingFragment, providing "num"
         * as an argument.
         */
        static ArrayListFragment newInstance(String userId) {
            ArrayListFragment f = new ArrayListFragment();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putString("userId", userId);
            f.setArguments(args);

            return f;
        }

        /**
         * When creating, retrieve this instance's number from its arguments.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            idToFetch = getArguments() != null ? getArguments().getString("userId"): "";
        }

        /**
         * The Fragment's UI is just a simple text view showing its
         * instance number.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {

            View view = inflater.inflate(R.layout.fragment_full_screen_image, container, false);
//            mBackButton = (ImageButton) view.findViewById(R.id.btnClose);
//            Picasso.with(getActivity())
//                    .load(R.drawable.back_arrow)
//                    .fit()
//                    .centerCrop()
//                    .into(mBackButton);
//            mBackButton.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                    Log.d(TAG, "clicked Back");
////                    ProfilePresenter.clearCache(uris,getActivity());
//                    getActivity().finish();
//                }
//            });
            mFullScreenImage = (ImageView) view.findViewById(R.id.imgDisplay);

            ParseQuery<ParseUser> query = ParseUser.getQuery();
            query.whereEqualTo("objectId", idToFetch);
            query.getFirstInBackground(new GetCallback<ParseUser>() {
                @Override
                public void done(ParseUser object, ParseException e) {
                    ParseFile file = object.getParseFile("profilePic");
                    String pictureUrl = file.getUrl();
                    if (!uris.contains(pictureUrl)){
                        uris.add(pictureUrl);
                    }
                    Picasso.with(getActivity())
                            .load(pictureUrl)
                            .resize(400,400)
                            .centerCrop()
                            .onlyScaleDown()
                            .placeholder(R.drawable.placeholder_profile)
                            .into(mFullScreenImage);
                }
            });

            return view;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

        }
    }
}