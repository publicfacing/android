package com.example.warefhaque.dapeandroid.Profile;

import android.content.Context;
import android.graphics.Bitmap;
import android.text.Editable;
import android.widget.EditText;
import android.widget.TextView;

import com.example.warefhaque.dapeandroid.Utils.AvatarImageView;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by warefhaque on 12/27/16.
 */

public interface ProfileInterface {

    interface View{
        void setLoadingIndicator(boolean active);

        void fetchSuccess(List<ProfilePresenter.SocialItem> currentSocialMediaItems, List<String> currentSocialMedia);

        void setPresenter(Presenter presenter);

        void presenterMessageCallback(String message);

        void logOut();
    }

    interface Presenter {
        void fetchCurrentUserProfile(EditText instagramAccount, EditText snapChatAccount, EditText twitterAccount, EditText whatsAppAccount, EditText aboutMe, EditText otherSm, TextView name, Context context);
        void fetchImage(AvatarImageView avatarImageView, String picture, int height, int width, Context context);
        void storeProfile(Editable instagramAccount, Editable snapChatAccount, Editable twitterAccount, Editable whatsAppAccount, Editable aboutMe, Editable other, String[] dareEntitlements, String [] profileEntitlements, String[] currentSocialMedia);
        void uploadImage(Bitmap image, String imageSelector);
        void setupProfileTextAndSocialMedia(ParseUser currUser, EditText aboutMe, TextView name);
    }
}
