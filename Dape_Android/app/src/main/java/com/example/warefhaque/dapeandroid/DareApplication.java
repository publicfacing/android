package com.example.warefhaque.dapeandroid;

import android.app.Application;
import android.util.Log;

import com.layer.sdk.LayerClient;
import com.parse.Parse;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.parse.interceptors.ParseLogInterceptor;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class DareApplication extends Application {
    private final static String PARSE_APP_ID = "BetDAREiiaig5uxPY68P";
    private final static String CLIENT_KEY = "HPE2ylbFh&HFAHgfiH@nAU";
    private static final String TAG = "DareApplication";
    private String testUrl = "http://10.0.3.2:1337/parse/";
    private String mainUrl = "https://dare-server.herokuapp.com/parse";

    @Override
    public void onCreate() {
        super.onCreate();

        /**
         * DB Setup
         */
        Parse.initialize(new Parse.Configuration.Builder(this)
                .applicationId(PARSE_APP_ID) // should correspond to APP_ID env variable
                .clientKey(CLIENT_KEY)  // set explicitly blank unless clientKey is configured on ParseAuthentication server
                .addNetworkInterceptor(new ParseLogInterceptor())
                .server(mainUrl).build());

        ParseUser.enableRevocableSessionInBackground();
        ParseFacebookUtils.initialize(getApplicationContext());
        initializeParsePush();
        LayerClient.applicationCreated(this);
    }

    void initializeParsePush(){
        ParseInstallation installation = ParseInstallation.getCurrentInstallation();

        if (installation == null){
            Log.e(TAG, "Parse Installation is null during app entry");
            return;
        }

        ParseUser currentUser = ParseUser.getCurrentUser();
        if (currentUser != null){
            installation.put("user", currentUser);
        }
        else{
            installation.remove("user");
        }

        List<String> channels = new ArrayList<>();
        channels.add("Main");
        String[] channelStrings = channels.toArray(new String[channels.size()]);
        installation.put("channels", Arrays.asList(channelStrings));
        ParseInstallation.getCurrentInstallation().saveInBackground(new SaveCallback(){
            @Override
            public void done(ParseException e) {
                if (e == null){
                    Log.i(TAG, "Parse Installation updated!");
                }
                else{
                    Log.e(TAG, "Parse Installation failed to update. Error: " + e.getLocalizedMessage());
                }
            }
        });
    }
}