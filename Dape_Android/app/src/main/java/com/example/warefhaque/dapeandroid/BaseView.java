package com.example.warefhaque.dapeandroid;

/**
 * Created by warefhaque on 12/15/16.
 */
public interface BaseView<T> {

    void setPresenter(T presenter);

}
