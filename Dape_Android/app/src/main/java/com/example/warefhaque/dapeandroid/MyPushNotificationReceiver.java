package com.example.warefhaque.dapeandroid;

import android.app.Notification;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.parse.ParsePushBroadcastReceiver;

import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by warefhaque on 3/13/17.
 */

public class MyPushNotificationReceiver extends ParsePushBroadcastReceiver {
    public static final String NEW_CHECKIN = "user-check-in";
    public static final String CHECK_OUT =  "user-check-out";
    public static final String DARE = "user-dare";
    public static FlowNotificationListener flowNotificationListener;
    public static PeopleNotificationListener peopleNotificationListener;
    @Override
    public void onReceive(Context context, Intent intent) {
        super.onReceive(context, intent);
    }

    @Override
    protected void onPushOpen(Context context, Intent intent) {

        JSONObject pushData = this.getPushData(intent);
        if (pushData!=null)
        {
            try {

                if (pushData.getString("type").equals(NEW_CHECKIN))
                {
                    Intent pushIntent = new Intent(context, LauncherActivity.class);
                    pushIntent.putExtra("ch_in_push", true);
                    context.startActivity(pushIntent);
                }
                if (pushData.getString("type").equals(DARE))
                {
                    Intent pushIntent = new Intent(context, LauncherActivity.class);
                    pushIntent.putExtra("dape_push", true);
                    context.startActivity(pushIntent);
                }
                // dont recieve anything for check out

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    @Override
    protected Notification getNotification(Context context, Intent intent) {
        JSONObject pushData = this.getPushData(intent);
        if(pushData != null && (pushData.has("alert") || pushData.has("title"))){
            try {
                if (peopleNotificationListener!=null && flowNotificationListener!=null){
                    if (pushData.getString("type").equals(NEW_CHECKIN)){
                        peopleNotificationListener.upDatePeopleList();
                    }else if (pushData.getString("type").equals(CHECK_OUT)){
                        peopleNotificationListener.upDatePeopleList();
                    }else if (pushData.getString("type").equals(DARE)){
                        flowNotificationListener.updateFlowList();
                    }
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }


        return super.getNotification(context, intent);

    }

    private JSONObject getPushData(Intent intent) {
        try {
            return new JSONObject(intent.getStringExtra("com.parse.Data"));
        } catch (JSONException var3) {
            Log.e("Parse Push", "Unexpected JSONException when receiving push data: " + var3);
            return null;
        }
    }

    public static interface FlowNotificationListener{
         void updateFlowList();
    }
    public static interface PeopleNotificationListener{
         void upDatePeopleList();
    }
}
