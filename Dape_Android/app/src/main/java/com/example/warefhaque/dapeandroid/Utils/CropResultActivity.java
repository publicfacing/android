package com.example.warefhaque.dapeandroid.Utils;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.LauncherActivity;
import com.example.warefhaque.dapeandroid.LoadingDialog;
import com.example.warefhaque.dapeandroid.R;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import java.io.ByteArrayOutputStream;

/**
 * Created by warefhaque on 5/2/17.
 */

public final class CropResultActivity extends Activity {

    /**
     * The image to show in the activity.
     */
    static Bitmap mImage;

    FloatingActionButton mSaveImage;
    private ImageView imageView;
    public LoadingDialog mLoadingDialog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_crop_result);

        imageView = ((ImageView) findViewById(R.id.resultImageView));
        imageView.setBackgroundColor(Color.rgb(0, 0, 0));
        Intent intent = getIntent();
        if (mImage != null) {
            imageView.setImageBitmap(mImage);
            int sampleSize = intent.getIntExtra("SAMPLE_SIZE", 1);
            double ratio = ((int) (10 * mImage.getWidth() / (double) mImage.getHeight())) / 10d;
            int byteCount = 0;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.HONEYCOMB_MR1) {
                byteCount = mImage.getByteCount() / 1024;
            }
            String desc = "(" + mImage.getWidth() + ", " + mImage.getHeight() + "), Sample: " + sampleSize + ", Ratio: " + ratio + ", Bytes: " + byteCount + "K";
            ((TextView) findViewById(R.id.resultImageText)).setText(desc);
        } else {
            Uri imageUri = intent.getParcelableExtra("URI");
            if (imageUri != null) {
                imageView.setImageURI(imageUri);
            } else {
                Toast.makeText(this, "No image is set to show", Toast.LENGTH_LONG).show();
            }
        }

        final Bitmap bitmap = MainFragment.mCroppedImage;
        mSaveImage = (FloatingActionButton) findViewById(R.id.btn_save_img);

        mSaveImage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                saveFile(bitmap, MainActivity.imgNum);

            }
        });
    }

    public void saveFile(Bitmap bitmap, final String imageSelector){
//        mView.setLoadingIndicator(true);
        loadingIndicator(true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (bitmap != null) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] data = stream.toByteArray();
            final ParseFile parseFile = new ParseFile("jpeg", data);

            parseFile.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
//                    mView.setLoadingIndicator(false);
                    loadingIndicator(false);
                    try {
                        ParseUser.getCurrentUser().fetchIfNeeded();
                        ParseUser.getCurrentUser().put(imageSelector, parseFile);
                        ParseUser.getCurrentUser().saveInBackground();
                        Toast.makeText(CropResultActivity.this, "Your image has been saved!", Toast.LENGTH_SHORT).show();

                        Intent intent = new Intent(CropResultActivity.this, LauncherActivity.class);
                        intent.putExtra("from_image_select", true);
                        startActivity(intent);


                    } catch (ParseException e1) {
                        e1.printStackTrace();
                        Toast.makeText(CropResultActivity.this, "Could not save image. Please try again.", Toast.LENGTH_SHORT).show();
                    }
                }
            });
        }else{

            Toast.makeText(CropResultActivity.this, "Could not save image. Please try again.", Toast.LENGTH_SHORT).show();

        }
    }

    public void loadingIndicator(boolean active) {
        if (mLoadingDialog==null){
            mLoadingDialog=new LoadingDialog(CropResultActivity.this);
        }
        if (active){
            mLoadingDialog.show();
        }else{
            mLoadingDialog.dismiss();
        }
    }


    @Override
    public void onBackPressed() {
        releaseBitmap();
        super.onBackPressed();
    }

    public void onImageViewClicked(View view) {
        releaseBitmap();
        finish();
    }

    private void releaseBitmap() {
        if (mImage != null) {
            mImage.recycle();
            mImage = null;
        }
    }
}