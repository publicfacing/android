package com.example.warefhaque.dapeandroid.Database;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;

import com.facebook.AccessToken;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseUser;
import com.parse.SaveCallback;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by warefhaque on 12/14/16.
 * All authentication calls to the db are here
 */

public class ParseAuthentication implements ParseInterface.authentication {


    public static final String INSTAGRAM = "instagram";
    public static final String TWITTER = "twitter";
    public static final String SNAPCHAT = "snapchat";
    public static final String WHATSAPP = "whatsapp";
    public static final String TUMBLR = "tumblr";
    public static final String PINTEREST = "pinterest";
    public static final String LINKEDIN = "linked_in";
    public static final String DEFAULT = "other";
    public static final String FACEBOOK="facebook";

    private static final String TAG= "ParseAuthentication";
    private String mEmail;
    private String mName;
    private String mGender;
    private String mFbId;
    private String mBirthday;
    private String mLocation;
    private String mRelationShipStatus;
    private ParseUser parseUser;
    private Bitmap mProfilePic;
    private String mCoverUrl;
    private String mAboutMe;
    private String mFacebookPictureURL;
    private ParseInterface.authenticationCallBack mAuthenticationCallback;

    public ParseAuthentication(){
       //empty
    }

    public void setAuthenticationCallback(ParseInterface.authenticationCallBack authenticationCallback){
        this.mAuthenticationCallback = authenticationCallback;
    }

    @Override
    public void getUserDetailsFb() {

        Log.d(TAG,"Entered the getUserDetailsFb");
        GraphRequest request = GraphRequest.newMeRequest(AccessToken.getCurrentAccessToken(), new GraphRequest.GraphJSONObjectCallback() {

            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {

                    Log.d(TAG,response.toString());
                    Log.d(TAG,object.toString());
                    String firstName = response.getJSONObject().getString("first_name");
                    String lastName = response.getJSONObject().getString("last_name");
                    mName = firstName+" "+lastName;

                    if (response.getJSONObject().has("email")){
                        mEmail = response.getJSONObject().getString("email");
                    }

                    if (response.getJSONObject().has("about")){
                        mAboutMe=response.getJSONObject().getString("about");
                    }

                    if (response.getJSONObject().has("gender")){
                        mGender = response.getJSONObject().getString("gender");
                    }

                    if (response.getJSONObject().has("birthday")){
                        mBirthday = response.getJSONObject().getString("birthday");
                    }

                    if (response.getJSONObject().has("id")){
                        mFbId = response.getJSONObject().getString("id");
                        String pictureUrl="https://graph.facebook.com/" + mFbId + "/picture?type=large&return_ssl_resources=1";
                        new ProfilePhotoUtils(pictureUrl).execute();
                    }

                    if (response.getJSONObject().has("location")){
                        if (response.getJSONObject().getJSONObject("location").has("name")){
                            mLocation = response.getJSONObject().getJSONObject("location").getString("name");
                        }
                    }

                    if (response.getJSONObject().has("relationship_status")){
                        mRelationShipStatus = response.getJSONObject().getString("relationship_status");
                    }

                    if (response.getJSONObject().has("cover")){
                        if (response.getJSONObject().getJSONObject("cover").has("source")){
                            mCoverUrl = response.getJSONObject().getJSONObject("cover").getString("source");
                        }
                    }


                } catch (JSONException e) {
                    e.printStackTrace();
                    mAuthenticationCallback.newUserSignupFailed(e.getMessage());
                }
            }
        });
        Bundle parameters = new Bundle();
        parameters.putString("fields", "first_name,last_name,location,email,id,gender,birthday,cover,hometown,religion,website,relationship_status,picture,about");
        request.setParameters(parameters);
        request.executeAsync();
    }

    @Override
    public void getUserDetailsParse() {
        parseUser = ParseUser.getCurrentUser();
        if (parseUser!=null){
            Log.d("getUserDetailsParse","name: "+parseUser.getString("name"));
            mAuthenticationCallback.userRetrievalSuccessfull(parseUser.getString("name"));
        }else{
            mAuthenticationCallback.userRetrievalFailed("Name is not defined");
        }
    }

    /**
     * UTIL CLASS BELOW: Handles dowloading image in async thread
     */
    private class ProfilePhotoUtils extends AsyncTask<String,String,String> {

        Bitmap bitmap;
        private String url;
        ProfilePhotoUtils(String url) {
            this.url = url;
        }


        @Override
        protected String doInBackground(String... params) {
            bitmap = DownloadImageBitmap(url);
            mProfilePic = bitmap;
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
            //Only save on post execute as the picture takes a longer time to save
            Log.i(TAG, "bitmap recieved successfully");
            saveNewUser();
        }

        private Bitmap DownloadImageBitmap(String url) {
            Bitmap bm = null;
            try {
                URL aURL = new URL(url);
                URLConnection conn = aURL.openConnection();
                conn.connect();
                InputStream is = conn.getInputStream();
                BufferedInputStream bis = new BufferedInputStream(is);
                bm = BitmapFactory.decodeStream(bis);
                bis.close();
                is.close();
            } catch (IOException e) {
                Log.e(TAG, "Error getting bitmap", e);
                mAuthenticationCallback.newUserSignupFailed(e.getMessage());
            }

            return bm;
        }

    }

    private void saveNewUser(){
        List<String> mDefaultSocialMedia = new ArrayList<>();
        mDefaultSocialMedia.add(FACEBOOK);
        mDefaultSocialMedia.add(INSTAGRAM);
        mDefaultSocialMedia.add(SNAPCHAT);
        mDefaultSocialMedia.add(WHATSAPP);
        mDefaultSocialMedia.add(TWITTER);

        List<String> dareEntitlements = new ArrayList<>();
        dareEntitlements.add(FACEBOOK);

        List<String> profileEntitlements = new ArrayList<>();
        profileEntitlements.add(FACEBOOK);
        parseUser = ParseUser.getCurrentUser();

        //must be statically saved
        String[] defaultSocialMedia = mDefaultSocialMedia.toArray(new String[mDefaultSocialMedia.size()]);
        String[] dareTitles = dareEntitlements.toArray(new String[dareEntitlements.size()]);
        String[] profileTitles = profileEntitlements.toArray(new String[profileEntitlements.size()]);
        parseUser.put("dareEntitlements", Arrays.asList(dareTitles));
        parseUser.put("publicEntitlements",Arrays.asList(profileTitles));
        parseUser.put("currentSocialMedia", Arrays.asList(defaultSocialMedia));

        parseUser.put("instagram", mName);

        if (mName!=null){
            parseUser.put("name",mName);
        }

        if (mEmail!=null){
            parseUser.put("email",mEmail);
        }

        if (mAboutMe!=null){
            parseUser.put("about",mAboutMe);
        }
        if (mBirthday!=null){
            parseUser.put("birthday",mBirthday);
        }

        if (mFbId!=null){
            parseUser.put("facebookID",mFbId);
        }

        if(mGender!=null){
            parseUser.put("gender",mGender);
        }

        if (mLocation!=null){
            parseUser.put("location",mLocation);
        }

        if(mRelationShipStatus!=null){
            parseUser.put("relationship",mRelationShipStatus);
        }

        if (mFacebookPictureURL!=null){
            parseUser.put("facebookPictureURL",mFacebookPictureURL);
        }
        if (mCoverUrl!=null){
            List<String> additionalPhotos = new ArrayList<>();
            additionalPhotos.add(mCoverUrl);
            parseUser.put("additionalPhotosURL",additionalPhotos);
        }
        Log.d("Parse User: ",parseUser.toString());
        //Don't save unless atleast name was captured!
        if (mName !=null){
            //Saving profile photo as a ParseFile
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            Bitmap bitmap = mProfilePic;
            if (bitmap != null) {
                bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
                byte[] data = stream.toByteArray();
                String thumbName = mName.replaceAll("\\s+", "_");
                final ParseFile parseFile = new ParseFile(thumbName + ".jpg", data);

                parseFile.saveInBackground(new SaveCallback() {
                    @Override
                    public void done(ParseException e) {
                        parseUser.put("profilePic", parseFile);

                        //Finally save all the user details

                        saveParseUserWithouProfilePicture();

                    }
                });
            }else{
                Log.e(TAG,"Bitmap=null in saveNewUser");
                saveParseUserWithouProfilePicture();
                mAuthenticationCallback.newUserSignupFailed("Saved the profile without profile picture!");

            }
        }else{
            mAuthenticationCallback.newUserSignupFailed("Could not save the user's name. Please try again");
        }
    }

    public void saveParseUserWithouProfilePicture(){
        parseUser.saveInBackground(new SaveCallback() {

            @Override
            public void done(ParseException e) {
                if (e==null){
                    mAuthenticationCallback.newUserSaveSuccessful(mName);
                }else{
                    mAuthenticationCallback.newUserSignupFailed(e.getMessage());
                }
            }
        });

    }
}
