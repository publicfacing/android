package com.example.warefhaque.dapeandroid.Utils;

/**
 * Created by warefhaque on 5/2/17.
 */

/**
 * Created by Arthu on 24-03-16.
 */
enum CropDemoPreset {
    RECT,
    CIRCULAR,
    CUSTOMIZED_OVERLAY,
    MIN_MAX_OVERRIDE,
    SCALE_CENTER_INSIDE,
    CUSTOM
}