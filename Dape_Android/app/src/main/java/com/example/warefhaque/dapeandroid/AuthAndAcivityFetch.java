package com.example.warefhaque.dapeandroid;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.Authentication.LoginActivity;
import com.example.warefhaque.dapeandroid.Layer.LayerCallbacks;
import com.example.warefhaque.dapeandroid.Layer.LayerImpl;
import com.layer.sdk.exceptions.LayerException;

/**
 * Created by warefhaque on 3/12/17.
 */

public class AuthAndAcivityFetch extends AppCompatActivity implements LayerCallbacks {
    public static final String TAG = "Auth And Activity Fetch";
    private boolean mFirstTimeLogin;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getIntent()!=null){
            if (getIntent().hasExtra("first_time")){
                mFirstTimeLogin = getIntent().getBooleanExtra("first_time",false);
            }
            else{
                mFirstTimeLogin = false;
            }
        }

    }

    @Override
    protected void onResume() {
        super.onResume();
        LayerImpl.setContext(this);
    }

    @Override
    public void onLayerConnected() {
        Log.d(TAG, "onLayerConnected");
        LayerImpl.authenticateUser();
//        if (!LayerImpl.isAuthenticated()) {
//            Log.d(TAG, "Calling authenticateUer...");
//
//        }else{
//
//            Intent intent = new Intent(this, LauncherActivity.class);
//            intent.putExtra("first_time",mFirstTimeLogin);
//            startActivity(intent);
//            finish();
//        }
    }

    @Override
    public void onLayerDisconnected() {
        Log.e(TAG, "onLayerDisconnected");
        Toast.makeText(getApplicationContext(), "Network Error: Please Authenticate again.", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);

    }

    @Override
    public void onLayerConnectionError(LayerException e) {
        Log.e(TAG, "onLayerConnectionError" + e.toString());
        Toast.makeText(getApplicationContext(), "Network Error: Please Authenticate again.", Toast.LENGTH_SHORT).show();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onUserAuthenticated(String id) {
        Log.d(TAG,"Authenticated: " + id);

            Intent intent = new Intent(this, LauncherActivity.class);
            intent.putExtra("first_time",mFirstTimeLogin);
            startActivity(intent);
            finish();
    }

    @Override
    public void onUserAuthenticatedError(LayerException e) {
        Toast.makeText(getApplicationContext(), "onUserAuthenticatedError: " + e.toString(), Toast.LENGTH_SHORT).show();
        Log.e(TAG, "onUserAuthenticatedError" + e.toString());
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    @Override
    public void onUserDeauthenticated() {
        Log.e(TAG, "User Deauthenticated");
    }

}
