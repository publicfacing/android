package com.example.warefhaque.dapeandroid.Profile;

import android.content.Context;
import android.graphics.Bitmap;
import android.net.Uri;
import android.text.Editable;
import android.util.Log;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.warefhaque.dapeandroid.Database.ParseAuthentication;
import com.example.warefhaque.dapeandroid.R;
import com.example.warefhaque.dapeandroid.Utils.AvatarImageView;
import com.parse.FindCallback;
import com.parse.GetCallback;
import com.parse.ParseException;
import com.parse.ParseFile;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;
import com.parse.SaveCallback;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

/**
 * Created by warefhaque on 12/26/16.
 */

public class ProfilePresenter implements ProfileInterface.Presenter{

    public static final String TAG = "Profile Presenter";
    ProfileInterface.View mView;


    public List<String> mCurrentSocialMedia;
    public ProfilePresenter(ProfileInterface.View view){
        mView=view;
    }


    @Override
    public void fetchCurrentUserProfile(final EditText instagramAccount, final EditText snapChatAccount, final EditText twitterAccount, final EditText whatsAppAccount, final EditText aboutMe, final EditText otherSm, final TextView name, final Context context) {

        //loading...
//        mView.setLoadingIndicator(true);
        final ParseUser currentUser =  ParseUser.getCurrentUser();

        if (currentUser!=null){
            ParseQuery<ParseUser> query = ParseQuery.getQuery("User");
            query.whereEqualTo("objectId", ParseUser.getCurrentUser().getObjectId());
            query.setCachePolicy(ParseQuery.CachePolicy.CACHE_THEN_NETWORK);
            query.findInBackground(new FindCallback<ParseUser>() {
                @Override
                public void done(List<ParseUser> objects, ParseException e) {
                    if (e==null){
                        setupProfileTextAndSocialMedia(currentUser,aboutMe, name);
                    }else{
                       Log.e(TAG, e.getMessage());
                    }
                }
            });



            currentUser.fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {

                }
            });
        }else{
            mView.logOut();
        }
    }

    @Override
    public void fetchImage(AvatarImageView avatarImageView, String picture, int height, int width, Context context) {

        try {
            if (ParseUser.getCurrentUser()!=null){
                ParseFile file = ParseUser.getCurrentUser().fetchIfNeeded().getParseFile(picture);
                if (file!=null){
                    String pictureUrl = file.getUrl();
                    if (pictureUrl!=null){
                        Uri fileUri = Uri.parse(pictureUrl);
                        Picasso.with(context)
                                .load(fileUri.toString())
                                .fit()
                                .centerCrop()
                                .placeholder(R.drawable.placeholder_profile)
                                .into(avatarImageView);
                    }else {
                        mView.presenterMessageCallback("Error "+ picture + " image does not exist!");
                    }
                }
            }
        } catch (ParseException e) {
            e.printStackTrace();
            mView.presenterMessageCallback("Error "+ picture + " image does not exist!");
        }

    }

    public static void fetchPeopleImage(AvatarImageView avatarImageView, ParseUser parseUser, String picture, int height, int width, Context context) {

        try {
            ParseFile file = parseUser.fetchIfNeeded().getParseFile(picture);
            String pictureUrl = file.getUrl();
            if (pictureUrl!=null){
                Uri fileUri = Uri.parse(pictureUrl);
                Picasso.with(context)
                        .load(fileUri.toString())
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder_profile)
                        .into(avatarImageView);
            }else {
                Log.d(TAG, "no profile pic");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }

    public static void fetchUserProfileImage(ImageView avatarImageView, ParseUser parseUser, String picture, int height, int width, Context context) {

        try {
            ParseFile file = parseUser.fetchIfNeeded().getParseFile(picture);
            String pictureUrl = file.getUrl();
            if (pictureUrl!=null){
                Uri fileUri = Uri.parse(pictureUrl);
                Picasso.with(context)
                        .load(fileUri.toString())
                        .fit()
                        .centerCrop()
                        .placeholder(R.drawable.placeholder_profile)
                        .into(avatarImageView);
            }else {
                Log.d(TAG, "no profile pic");
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }

    }


    @Override
    public void storeProfile(Editable instagramAccount, Editable snapChatAccount, Editable twitterAccount, Editable whatsAppAccount, Editable aboutMe, Editable other, String[] dareEntitlements, String [] profileEntitlements, String[] currentSocialMedia ) {


        try {
            ParseUser.getCurrentUser().fetchIfNeeded();

            ParseUser.getCurrentUser().put("instagram",instagramAccount.toString());
            ParseUser.getCurrentUser().put("snapchat",snapChatAccount.toString());
            ParseUser.getCurrentUser().put("twitter",twitterAccount.toString());
            ParseUser.getCurrentUser().put("whatsapp",whatsAppAccount.toString());
            ParseUser.getCurrentUser().put("about",aboutMe.toString());
            ParseUser.getCurrentUser().put("otherSM",other.toString());
            ParseUser.getCurrentUser().put("dareEntitlements", Arrays.asList(dareEntitlements));
            ParseUser.getCurrentUser().put("publicEntitlements",Arrays.asList(profileEntitlements));
            ParseUser.getCurrentUser().put("currentSocialMedia", Arrays.asList(currentSocialMedia));

//            mView.setLoadingIndicator(true);
            ParseUser.getCurrentUser().saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
//                    mView.setLoadingIndicator(false);
                    if (e==null){
                        mView.presenterMessageCallback("Your data has been saved!");
                    }else{
                        mView.presenterMessageCallback(e.getMessage()+" "+"Please try again.");
                    }
                }
            });
        } catch (ParseException e) {
            e.printStackTrace();
            mView.presenterMessageCallback(e.getMessage()+" "+"Please try again.");
        }
    }

    @Override
    public void uploadImage(Bitmap image, String imageSelector) {
        switch (imageSelector) {
            case "leftImage":
                saveFile(image, "leftPic");
                break;
            case "mainImage":
                saveFile(image, "profilePic");
                break;
            case "rightImage":
                saveFile(image, "rightPic");
                break;
            default:
                mView.presenterMessageCallback("Error: Could not upload picture. Please try selecting an image again.");
        }
    }

    private void saveFile(Bitmap bitmap, final String imageSelector){
//        mView.setLoadingIndicator(true);
        ByteArrayOutputStream stream = new ByteArrayOutputStream();
        if (bitmap != null) {
            bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
            byte[] data = stream.toByteArray();
            final ParseFile parseFile = new ParseFile("jpeg", data);

            parseFile.saveInBackground(new SaveCallback() {
                @Override
                public void done(ParseException e) {
//                    mView.setLoadingIndicator(false);
                    try {
                        ParseUser.getCurrentUser().fetchIfNeeded();
                        ParseUser.getCurrentUser().put(imageSelector, parseFile);
                        mView.presenterMessageCallback("You image has been saved!");

                    } catch (ParseException e1) {
                        e1.printStackTrace();
                        mView.presenterMessageCallback("Error: Could not upload picture. Please try selecting an image again.");
                    }
                }
            });
        }else{
            mView.presenterMessageCallback("Error: Could not upload picture. Please try selecting an image again.");
        }
    }

    public class SocialItem {

        String mHint;
        String mPermission;
        int mSocialLogoId;
        String mSocialAccount;
        boolean mDareenabled;
        boolean mProfileEnabled;

        public SocialItem(int socialLogoId, String account, boolean darenEnabled, boolean profileEnabled,String permission, String hint){
            mSocialAccount = account;
            mSocialLogoId = socialLogoId;
            mDareenabled = darenEnabled;
            mProfileEnabled = profileEnabled;
            mPermission = permission;
            mHint = hint;
        }

    }

    /**
     *  @description: you pass in the socialItemList with the name.
     *  fill the icons here and then fill the dare and profile enables in the next function
     *  then pass the completed list upwards
     * @param dareEnables array of strings containing the dareEnables
     * @param profileEnables array of strings containing the profileEnables
     * @param socialItemList all required info to fill the social Media items on the profile page
     *                       look at the Class Social Item
     * @return
     */
    public List<SocialItem> setUpSocialItem(List<String> dareEnables, List<String> profileEnables,  List<SocialItem> socialItemList){


        for (int i = 0; i< socialItemList.size(); i++){
            if (socialItemList.get(i).mPermission.equals(ParseAuthentication.INSTAGRAM)) {
                 socialItemList.get(i).mSocialLogoId = R.drawable.instagram_logo_profile;
                 socialItemList.get(i).mHint = "@";

            }else if (socialItemList.get(i).mPermission.equals(ParseAuthentication.TWITTER)) {
                socialItemList.get(i).mSocialLogoId = R.drawable.twitter_logo_profile;
                socialItemList.get(i).mHint = "@";


            } else   if (socialItemList.get(i).mPermission.equals(ParseAuthentication.LINKEDIN)) {
                socialItemList.get(i).mSocialLogoId = R.drawable.linked_in_logo_profile;
                socialItemList.get(i).mHint = "@";

            }else   if (socialItemList.get(i).mPermission.equals(ParseAuthentication.SNAPCHAT)) {
                socialItemList.get(i).mSocialLogoId = R.drawable.snapchat_logo_profile;
                socialItemList.get(i).mHint = "Type it here...";

            }else   if (socialItemList.get(i).mPermission.equals(ParseAuthentication.WHATSAPP)) {
                socialItemList.get(i).mSocialLogoId = R.drawable.whatsapp_logo_profile;
                socialItemList.get(i).mHint = "Add your number";


            }else   if (socialItemList.get(i).mPermission.equals(ParseAuthentication.TUMBLR)) {
                socialItemList.get(i).mSocialLogoId = R.drawable.tumblr_logo_profile;
                socialItemList.get(i).mHint = "@";



            }else   if (socialItemList.get(i).mPermission.equals(ParseAuthentication.PINTEREST)) {
                socialItemList.get(i).mSocialLogoId = R.drawable.pinterest_logo_profile;
                socialItemList.get(i).mHint = "@";

            }else   if (socialItemList.get(i).mPermission.equals(ParseAuthentication.DEFAULT)) {
                socialItemList.get(i).mSocialLogoId = R.drawable.hashtag_filled;
                socialItemList.get(i).mHint = "Choose from list";
            }else if (socialItemList.get(i).mPermission.equals(ParseAuthentication.FACEBOOK)){
                socialItemList.get(i).mSocialLogoId = R.drawable.facebook_logo_profile;
                socialItemList.get(i).mHint = "Name";
            }

        }

        //list filled
        List<SocialItem> list = setUpDareAndPublicEntitles(dareEnables, profileEnables, socialItemList);
        return list;

    }

    private List<SocialItem> setUpDareAndPublicEntitles(List<String> dareEnables, List<String> profileEnables, List<SocialItem> currentSocial){

        if (dareEnables!=null){
            for (int i =0; i < dareEnables.size(); i++){
                for (int j=0; j< currentSocial.size();j++){
                    if (dareEnables.get(i).equals(currentSocial.get(j).mPermission)){
                        currentSocial.get(j).mDareenabled=true;
                    }
                }
            }

        }

        if (profileEnables!=null){
            for (int i =0; i < profileEnables.size(); i++){
                for (int j=0; j< currentSocial.size();j++){
                    if (profileEnables.get(i).equals(currentSocial.get(j).mPermission)){
                        currentSocial.get(j).mProfileEnabled=true;
                    }
                }
            }

        }


        //if no dare or profile enabled yet then you send the same list back
        return currentSocial;
    }

    public static void clearCache(List<String> uris, Context context){
        for (String uri:uris){
            Log.d(TAG, "invalidating"+uri);
            Picasso.with(context).invalidate(uri);
        }
    }

    public void setupProfileTextAndSocialMedia(ParseUser currUser, EditText aboutMe, TextView name){
        List<String> currentSocialMedia = new ArrayList<String>();
        List<String> dareEntitlements = new ArrayList<String>();
        List<String> publicEntitlements = new ArrayList<String>();
        List<SocialItem> socialItems = new ArrayList<SocialItem>();

        name.setText(currUser.getString("name"));
        if (currUser.getString("about")!=null)
            aboutMe.setText(currUser.getString("about"));

        if (currUser.get("currentSocialMedia")!=null){
            currentSocialMedia = currUser.getList("currentSocialMedia");
        }else{
            currentSocialMedia=ProfileFragment.mDefaultSocialMedia;
        }

        if (currUser.get("dareEntitlements")!=null){
            dareEntitlements = currUser.getList("dareEntitlements");
        }

        if (currUser.get("publicEntitlements")!=null){
            publicEntitlements = currUser.getList("publicEntitlements");
        }

        //set the social items together
        if (currUser.getString("instagram")!=null)
            socialItems.add(new SocialItem(0,currUser.getString("instagram"),false,false,currentSocialMedia.get(0),""));
        else
            socialItems.add(new SocialItem(0,"",false,false,currentSocialMedia.get(0),""));


        if (currUser.getString("twitter")!=null)
            socialItems.add(new SocialItem(0,currUser.getString("twitter"),false,false,currentSocialMedia.get(1),""));
        else
            socialItems.add(new SocialItem(0,"",false,false,currentSocialMedia.get(1),""));

        if (currUser.getString("snapchat")!=null)
            socialItems.add(new SocialItem(0,currUser.getString("snapchat"),false,false,currentSocialMedia.get(2),""));
        else
            socialItems.add(new SocialItem(0,"",false,false,currentSocialMedia.get(2),""));

        if (currUser.getString("whatsapp")!=null)
            socialItems.add(new SocialItem(0,currUser.getString("whatsapp"),false,false,currentSocialMedia.get(3),""));
        else
            socialItems.add(new SocialItem(0,"",false,false,currentSocialMedia.get(3),""));

        if (currUser.get("otherSM")!=null)
            socialItems.add(new SocialItem(0,currUser.getString("otherSM"),false,false,currentSocialMedia.get(4),""));
        else
            socialItems.add(new SocialItem(0,"",false,false,currentSocialMedia.get(4),""));

        List<SocialItem> finalItems = setUpSocialItem(dareEntitlements,publicEntitlements,socialItems);
//        mView.setLoadingIndicator(false);
        mView.fetchSuccess(finalItems,currentSocialMedia);
    }


}
