package com.example.warefhaque.dapeandroid.Settings;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.preference.Preference;
import android.support.v7.preference.PreferenceFragmentCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.LauncherActivity;
import com.example.warefhaque.dapeandroid.Layer.LayerImpl;
import com.example.warefhaque.dapeandroid.R;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.parse.LogOutCallback;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

/**
 * Created by warefhaque on 12/18/16.
 */

public class SettingsFragment extends PreferenceFragmentCompat {
    public Button mLogout;

    @Override
    public void onCreatePreferences(Bundle bundle, String s) {

      addPreferencesFromResource(R.xml.preference_screen);
      Preference inviteFriends = findPreference("invite_friend");

      inviteFriends.setOnPreferenceClickListener(new Preference.OnPreferenceClickListener() {
        @Override
        public boolean onPreferenceClick(Preference preference) {
          Toast.makeText(getActivity(), "CLICKE!!", Toast.LENGTH_SHORT).show();
          try {
            Intent i = new Intent(Intent.ACTION_SEND);
            i.setType("text/plain");
            i.putExtra(Intent.EXTRA_SUBJECT, "Dape");
            String sAux = "\nBe a tap away from anyone\n\n";
            sAux = sAux + "https://dapetheapp.com \n\n";
            i.putExtra(Intent.EXTRA_TEXT, sAux);
            startActivity(Intent.createChooser(i, "choose one"));
          } catch(Exception e) {
            //e.toString();
          }

          return false;
        }
      });

    }



//    @Nullable
//    @Override
//    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        View view = inflater.inflate(R.layout.fragment_settings,container,false);
//
//        mLogout = (Button) view.findViewById(R.id.logout_button);
//        mLogout.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//
//                LauncherActivity launcherActivity = (LauncherActivity) getActivity();
//                launcherActivity.logoutUser();
//
//
//            }
//        });
//        return view;
//    }
}
