package com.example.warefhaque.dapeandroid.Layer;

import com.layer.sdk.exceptions.LayerException;

/**
 * Created by warefhaque on 2/18/17.
 */

public interface LayerCallbacks {

    //Layer connection callbacks
    public void onLayerConnected();
    public void onLayerDisconnected();
    public void onLayerConnectionError(LayerException e);

    //Layer authentication callbacks
    public void onUserAuthenticated(String id);
    public void onUserAuthenticatedError(LayerException e);
    public void onUserDeauthenticated();
}
