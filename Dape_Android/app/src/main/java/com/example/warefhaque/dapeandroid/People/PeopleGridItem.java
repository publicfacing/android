package com.example.warefhaque.dapeandroid.People;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.example.warefhaque.dapeandroid.Profile.ProfilePresenter;
import com.example.warefhaque.dapeandroid.R;
import com.example.warefhaque.dapeandroid.Utils.AvatarImageView;
import com.parse.ParseException;
import com.parse.ParseUser;

/**
 * Created by warefhaque on 1/14/17.
 */

public class PeopleGridItem {

    AvatarImageView profilePic;
    TextView name;
    public View PeopleGridItem(LayoutInflater inflater, Context context, ParseUser user, ViewGroup parent){
        View view = inflater.inflate(R.layout.people_grid_item, parent, false);
        try {
            user.fetchIfNeeded();


            profilePic = (AvatarImageView) view.findViewById(R.id.profile_picture_grid);
            name = (TextView)view.findViewById(R.id.first_and_last_name_label_grid);

            name.setText(user.getString("name"));
            ProfilePresenter.fetchPeopleImage(profilePic, user, "profilePic", 90,90, context);

        } catch (ParseException e) {
            e.printStackTrace();
        }

        return  view;
    }
}
