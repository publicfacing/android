package com.example.warefhaque.dapeandroid.Messages;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;

import com.example.warefhaque.dapeandroid.R;
import com.example.warefhaque.dapeandroid.Utils.ActivityUtils;

/**
 * Created by warefhaque on 3/6/17.
 */

public class DareInterfaceActivity extends AppCompatActivity{
    String mParticipantId;
    public static UpDateDare upDateDare;
    String mObjectId;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Intent intent = getIntent();
        if (intent!=null){
            if (intent.hasExtra("participantId")){
                mParticipantId = intent.getStringExtra("participantId");
            }if (intent.hasExtra("objectId")){
                mObjectId = intent.getStringExtra("objectId");
            }
        }

        setContentView(R.layout.dare_parent);
        if (getSupportActionBar()!=null){
            getSupportActionBar().hide();
        }

        //get the Frame in which the Fragment will live
        DareFragment dareFragment = (DareFragment) getSupportFragmentManager().findFragmentById(R.id.dare_content_frame);
        //if the frame does not contain a fragment, create it
        if (dareFragment==null){
            dareFragment=DareFragment.newInstance();
            //add the Fragment in the Frame slot in the activity
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), dareFragment, R.id.dare_content_frame);
        }

    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        upDateDare.updateDareCell(mObjectId);
    }

    public static interface UpDateDare{
        public void updateDareCell(String objectId);
    }
}
