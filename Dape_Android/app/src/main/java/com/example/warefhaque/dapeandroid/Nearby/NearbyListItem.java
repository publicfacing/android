package com.example.warefhaque.dapeandroid.Nearby;

import android.graphics.Bitmap;

/**
 * Created by warefhaque on 12/22/16.
 */

class NearbyListItem {

    String mPlace;
    String mImageUrl;
    String mAddress;
    String mDistance;
    String mId;
    boolean mFetched ;
    Bitmap mBitmap;
    int mIconId;
    double mLat;
    double mLng;

    public NearbyListItem(String name, String photos, String address, double lat, double lng, double currentLat, double currentLong,String id, boolean fetched, Bitmap bitmap, int iconId){
        mPlace=name;
        mImageUrl = photos;
        mLat = lat;
        mLng = lng;
        setmDistance(lat,lng,currentLat,currentLong);
        mAddress = address;
        mId = id;
        mFetched = fetched;
        mBitmap = bitmap;
        mIconId = iconId;
    }
    private void setmDistance(double lat, double lng, double currentLat, double currentLong){
        float [] results = new float[1];
        android.location.Location.distanceBetween(currentLat,currentLong,lat,lng,results);

        int distance = (int) results[0];

        distance =  (Math.round(distance));
        mDistance = Integer.toString(distance) + " m";

    }

}
