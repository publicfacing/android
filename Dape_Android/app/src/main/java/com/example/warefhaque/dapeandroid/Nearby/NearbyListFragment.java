package com.example.warefhaque.dapeandroid.Nearby;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.LauncherActivity;
import com.example.warefhaque.dapeandroid.LoadingDialog;
import com.example.warefhaque.dapeandroid.People.PeopleInterface;
import com.example.warefhaque.dapeandroid.R;
import com.google.android.gms.maps.model.LatLng;
import com.parse.FunctionCallback;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by warefhaque on 12/22/16.
 */

public class NearbyListFragment extends Fragment implements NearbyInterface.View{
    public static final String TAG = "Nearby List Fragment";
    NearbyListAdapter mListAdapter;
    public NearbyInterface.mapViewCallbackListener mNearbyFragment;
    public SwipeRefreshLayout mSwipeRefreshLayout;
    List<NearbyListItem> mAdapterList;
    public LoadingDialog mLoadingDialog;
    private final LatLng mDefaultLocation = new LatLng(-33.8523341, 151.2106085);
    public NearbyInterface.Presenter mPresenter;
    public PeopleInterface.View mPeopleFragment;
    public FloatingActionButton mMapButton;
    public ParseObject mCurrentCheckedInPlace;
    public RelativeLayout mPlacesLayout;
    public TextView mNoPlaces;
    public RelativeLayout mFetchingPlacesLayout;
    public static final String CHECKED_IN = "you have checked in";



    public NearbyListFragment() {
        //empty default constructor
    }
    public void setPresenter(NearbyInterface.Presenter presenter){
        mPresenter = presenter;
        Log.d(TAG, "Set the presenter to " + presenter.toString());
    }

    public void setMapViewCallback(NearbyInterface.mapViewCallbackListener mapViewCallbackListener) {
        Log.d(TAG, "CALLED SETMAPVIEW");
        mNearbyFragment = mapViewCallbackListener;
    }

    public void setupPeopleFragment(PeopleInterface.View peopleFragment){
        mPeopleFragment = peopleFragment;

    }

    public static NearbyListFragment newInstance() {
        return new NearbyListFragment();
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
//        For testing only!
//        DemoPlace demoPlace = new DemoPlace("Rendezvous Hotel Sydney The Rocks","30 Pitt Street, Sydney",-33.85423,151.208219,"ChIJKRVAqFyuEmsRQGlTmkFIVOo");
//        DemoPlace demoPlace2 = new DemoPlace("Sydney Harbour Marriott Hotel at Circular Quay","30 Pitt Street, Sydney",-33.8627261,151.2092998,"ChIJC-dWE0KuEmsR_cLanc3QLkc");
//
//        checkInUser(null,null,demoPlace);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
//        setHasOptionsMenu(true);
        if (getActivity().getActionBar()!=null){
            getActivity().getActionBar().setTitle("Places");
        }
        View root = inflater.inflate(R.layout.fragment_nearby_list, container, false);
        ListView listView = (ListView) root.findViewById(R.id.nearby_list);
        mListAdapter = new NearbyListAdapter();
        mSwipeRefreshLayout = (SwipeRefreshLayout) root.findViewById(R.id.swipe_to_refresh);
        mPlacesLayout = (RelativeLayout) root.findViewById(R.id.places_layout);
        mFetchingPlacesLayout  = (RelativeLayout) root.findViewById(R.id.loading_places);
        mNoPlaces = (TextView) root.findViewById(R.id.no_places);
        if (mPresenter!=null){
           fetchPlacesInfo();
        }
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                mAdapterList = new ArrayList<>();
                fetchPlacesInfo();
                mSwipeRefreshLayout.setRefreshing(false);
            }
        });

//        mMapButton  = (FloatingActionButton) root.findViewById(R.id.map_button);
//        mMapButton.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                mMapButton.setVisibility(View.GONE);
//                mNearbyFragment.displayMapFromList();
//            }
//        });
        listView.setAdapter(mListAdapter);

        return root;
    }
    public void loadingIndicator(boolean active) {
        if (mLoadingDialog==null){
            mLoadingDialog=new LoadingDialog(getActivity());
        }
        if (active){
            mLoadingDialog.show();
        }else{
            mLoadingDialog.dismiss();
        }
    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void fetchSuccess(final JSONArray places) {
                    /**
             * The code for the async fetch using the web api and json parsing:
             */
        if (places.length() == 0 )
        {
            setPlacesUIState(PlacesUiState.NO_PLACES);
        }
        else
        {
            setPlacesUIState(PlacesUiState.FETCHED_PLACES);
            if (mAdapterList!=null){
                mAdapterList.clear();
            }else{
                mAdapterList = new ArrayList<>();
            }
            loadingIndicator(false);
            Log.i("PRINTING LIST OF PLACES", places.toString());
            Log.i("SIze of places", String.valueOf(places.length()));
            if (getActivity() != null) {
                getActivity().runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        for (int i=0; i<places.length();i++){
                            try {
                                JSONObject place = places.getJSONObject(i).getJSONObject("result");
                                Log.d(TAG, place.toString());
                                double latitude = -33.8523341;
                                double longitude = 151.2106085;
                                String url = "";
                                String address = "";
                                if (place.has("geometry")) {

                                    JSONObject location = place.getJSONObject("geometry").getJSONObject("location");
                                    String lat = location.getString("lat");
                                    String lng =  location.getString("lng");

                                    latitude = Double.parseDouble(lat);
                                    longitude = Double.parseDouble(lng);
                                }
                                if (place.has("vicinity")){
                                    address = place.getString("vicinity");
                                }
                                if (place.has("photos")){
                                    JSONArray photos  = place.getJSONArray("photos");
                                    url  = unPackPlacePhotos( photos);
                                }
                                NearbyListItem nearbyListItem;

                                if (mCurrentCheckedInPlace==null){
                                    //user not checked in
                                    if (i<=3)
                                    {
                                        // only show the check in icon for top 3-4 places.
                                        nearbyListItem = new NearbyListItem(place.getString("name"),url,address,latitude,longitude, NearbyFragment.mCurrentLocation.getLatitude(),NearbyFragment.mCurrentLocation.getLongitude(),place.getString("place_id"),false,null,R.drawable.check_in);
                                    }
                                    else
                                    {   // put a 0 in the id to not put any icon there
                                        nearbyListItem = new NearbyListItem(place.getString("name"),url,address,latitude,longitude, NearbyFragment.mCurrentLocation.getLatitude(),NearbyFragment.mCurrentLocation.getLongitude(),place.getString("place_id"),false,null,0);

                                    }
                                }else
                                {
                                    if (place.getString("place_id").equals(mCurrentCheckedInPlace.getString("placeId")))
                                    {
                                        nearbyListItem = new NearbyListItem(place.getString("name"),url,address,latitude,longitude, NearbyFragment.mCurrentLocation.getLatitude(),NearbyFragment.mCurrentLocation.getLongitude(),place.getString("place_id"),false,null,R.drawable.check_out);
                                    }
                                    else
                                    {
                                        nearbyListItem = new NearbyListItem(place.getString("name"),url,address,latitude,longitude, NearbyFragment.mCurrentLocation.getLatitude(),NearbyFragment.mCurrentLocation.getLongitude(),place.getString("place_id"),false,null,0);

                                    }
                                }

                                mAdapterList.add(nearbyListItem);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                        }
                        Log.d(TAG, mAdapterList.toString());
                        mListAdapter.notifyDataSetChanged();
                    }
                });
            }else{
                Log.e(TAG, "Activity is null!");
            }
        }
    }

    @Override
    public void fetchFailure(String message) {
        setPlacesUIState(PlacesUiState.FETCHED_PLACES);
    }



    /**
     * Adapter class begins here
     */

    private class NearbyListAdapter extends BaseAdapter {


        public NearbyListAdapter() {
            mAdapterList = new ArrayList<>();
        }

        @Override
        public int getCount() {
            return mAdapterList.size();
        }

        @Override
        public NearbyListItem getItem(int position) {
            return mAdapterList.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, final View convertView, ViewGroup parent) {
            View rowView = convertView;

            PlaceHolder placeHolder;

            if (rowView == null) {
                placeHolder = new PlaceHolder();
                LayoutInflater inflater = LayoutInflater.from(parent.getContext());
                rowView = inflater.inflate(R.layout.fragment_nearby_list_item, parent, false);
                placeHolder.placeName = (TextView) rowView.findViewById(R.id.name_of_place);
                placeHolder.placeImage = (ImageView) rowView.findViewById(R.id.place_image);
                placeHolder.placeAddress = (TextView) rowView.findViewById(R.id.address_of_place);
                placeHolder.distance = (TextView) rowView.findViewById(R.id.distance_to_place);
                placeHolder.icon = (ImageButton) rowView.findViewById(R.id.check_in);
                rowView.setTag(placeHolder);
            }else{
                placeHolder = (PlaceHolder) rowView.getTag();
            }
            NearbyListItem place = mAdapterList.get(position);
            placeHolder.setUpViews(place);

            return rowView;
        }
    }

    /**
     * Cloud function
     */
    public void checkInUser(final View view, DemoPlace secondPlace){

        HashMap<String, Object> params = new HashMap<>();

        params.put("place_id", secondPlace.id);
        params.put("place_name", secondPlace.name);

        params.put("place_address", secondPlace.address);
        params.put("place_location_lat", secondPlace.lat);
        params.put("place_location_lon", secondPlace.lng);
        params.put("place_types", "_type");
        params.put("user_id", ParseUser.getCurrentUser().getObjectId());
        params.put("user_location_lat",  NearbyFragment.mCurrentLocation.getLatitude());
        params.put("user_location_lon",NearbyFragment.mCurrentLocation.getLongitude());
//
        loadingIndicator(true);
        ParseCloud.callFunctionInBackground("check-in", params, new FunctionCallback<Object>() {
            public void done(Object data, ParseException e) {
                loadingIndicator(false);
                if (e == null) {
                    Log.d(TAG, data.toString());
                    Log.d("NearbyListFragment","Cloud Call worked!");
                    String dataString = (String) data;
                        Toast.makeText(getActivity(),"You've checked in!",Toast.LENGTH_SHORT).show();
                        LauncherActivity launcherActivity = (LauncherActivity) getActivity();
                        launcherActivity.startTimer();
                        //update the current Nearby List Screen
                        fetchPlacesInfo();
                        //update the people checked in to the place.
                        launcherActivity.peopleFragment.mPresenter.fetchPeopleCheckedIn();
                        //move focus to the people fragment
                        launcherActivity.mViewPager.setCurrentItem(1);
                        //update the "Flow" screen
                        launcherActivity.messagesFragment.updateActivities();
                        launcherActivity.mTabCheckOutButton.setImageResource(R.drawable.check_action_bar);
                        launcherActivity.mTabCheckOutButton.setEnabled(true);
                }else{
                    Toast.makeText(getActivity(),"Error: " + e.getMessage(),Toast.LENGTH_SHORT).show();
                    Log.d(TAG, e.getMessage());

                }
            }
        });
    }

    public void checkOut(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(getActivity());
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle("Checkout");
        alertDialogBuilder.setMessage("Are you sure you want to checkout?");


        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                HashMap < String, Object > params = new HashMap<>();
                ParseCloud.callFunctionInBackground("check-out", params , new FunctionCallback<Object>() {
                    public void done(Object data, ParseException e) {
                        if (e == null) {
                            Log.d(TAG, data.toString());

                            Log.d("NearbyListFragment","Check-out call worked!");

                            LauncherActivity launcherActivity = (LauncherActivity) getActivity();
                            launcherActivity.mTabCheckOutButton.setImageResource(android.R.color.transparent);
                            launcherActivity.mTabCheckOutButton.setEnabled(false);
                            Toast.makeText(getActivity(),"You've checked out!",Toast.LENGTH_SHORT).show();

                            launcherActivity.peopleFragment.mPresenter.fetchPeopleCheckedIn();
                            launcherActivity.stoptimertask();
                            launcherActivity.messagesFragment.updateActivities();
                            fetchPlacesInfo();


                        }else{
                            Toast.makeText(getActivity(),"CLOUD LOG---"+e.getCode(),Toast.LENGTH_SHORT).show();
                            Log.d(TAG, e.getMessage());

                        }
                    }
                });

            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //nothing
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();

    }

    public static class DemoPlace {
        public String name;
        public String address;
        public double lat;
        public double lng;
        public String id;

        DemoPlace(String name, String address, double lat, double lng, String id){
            this.id = id;
            this.address = address;
            this.lat = lat;
            this.lng = lng;
            this.name = name;

        }


    }

    public class PlaceHolder{
        TextView placeName;
        TextView placeAddress;
        TextView distance;
        ImageView placeImage;
        ImageButton icon;

        public void setUpViews(final NearbyListItem place){
            placeName.setText(place.mPlace);


            Log.d(TAG, place.mImageUrl);
            Picasso.with(getActivity())
                    .load(Uri.parse(place.mImageUrl))
                    .placeholder(R.drawable.placeholder_places)
                    .fit()
                    .centerCrop()
                    .into(placeImage);


            placeAddress.setText(place.mAddress);


            distance.setText(place.mDistance);



            if (place.mIconId==R.drawable.check_in){
                Picasso.with(getActivity())
                        .load(R.drawable.check_in)
                        .fit()
                        .into(icon);
                icon.setVisibility(View.VISIBLE);
            }
            else if (place.mIconId==R.drawable.check_out){
                Picasso.with(getActivity())
                        .load(R.drawable.check_out)
                        .fit()
                        .into(icon);
                icon.setVisibility(View.VISIBLE);
            }
            else{
                icon.setVisibility(View.INVISIBLE);
            }
            icon.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d(TAG, "clicked checkin/checkout");
                    if (place.mIconId==R.drawable.check_in){
                        Log.d(TAG, "clicked CHECKIN!");
                        NearbyListItem nearbyListItem = place;
                        DemoPlace demoPlace = new DemoPlace(nearbyListItem.mPlace, nearbyListItem.mAddress, nearbyListItem.mLat,nearbyListItem.mLng,nearbyListItem.mId);
                        checkInUser(v, demoPlace);
                    }else if (place.mIconId == R.drawable.check_out){
                        Log.d(TAG, "clicked CHECKOU!");
                       checkOut();
                    }
                }
            });
        }
    }

    public static String unPackPlacePhotos(JSONArray photos){

        StringBuilder urlBuilder = new StringBuilder("https://maps.googleapis.com/maps/api/place/photo?");
        try {
            if (photos.getJSONObject(0).has("photo_reference")) {
                if (photos.getJSONObject(0).has("width")){
                    urlBuilder.append("maxwidth=").append( photos.getJSONObject(0).getString("width"));
                }else{
                    urlBuilder.append("maxwidth=").append( photos.getJSONObject(0).getString("width"));
                }
                urlBuilder.append("&photoreference=").append(photos.getJSONObject(0).getString("photo_reference"));
                urlBuilder.append("&key=AIzaSyA8TUEVYgazWsn6scxuMBJ51JwLg_uOLrg");
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return urlBuilder.toString();
    }

    public void fetchPlacesInfo(){

        try {
            ParseUser.getCurrentUser().fetch();
            mCurrentCheckedInPlace = ParseUser.getCurrentUser().getParseObject("checkedIn");

            if (mCurrentCheckedInPlace!=null){
                mCurrentCheckedInPlace.fetch();
            }
            setPlacesUIState(PlacesUiState.FETCHING_PLACES);
            mPresenter.fetchPlacesList();
        } catch (ParseException e) {
            e.printStackTrace();
        }
    }

    enum PlacesUiState{
        FETCHING_PLACES,
        FETCHED_PLACES,
        NO_PLACES
    }

    public void setPlacesUIState(PlacesUiState placesUIState){
        switch (placesUIState){
            case FETCHED_PLACES:
                mPlacesLayout.setVisibility(View.VISIBLE);
                mFetchingPlacesLayout.setVisibility(View.GONE);
                mNoPlaces.setVisibility(View.GONE);
                break;
            case FETCHING_PLACES:
                mFetchingPlacesLayout.setVisibility(View.VISIBLE);
                mPlacesLayout.setVisibility(View.GONE);
                mNoPlaces.setVisibility(View.GONE);
                break;
            case NO_PLACES:
                mNoPlaces.setVisibility(View.VISIBLE);
                mFetchingPlacesLayout.setVisibility(View.GONE);
                mPlacesLayout.setVisibility(View.GONE);

        }
    }

}

