package com.example.warefhaque.dapeandroid.People;

import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by warefhaque on 1/14/17.
 */

public interface PeopleInterface {

    interface View {
        void fetchedCheckedInPeople(List<ParseUser> parseUserList);
        void failedfetch(boolean networkError);
        void updatePeopleList (ParseObject place);
    }

    interface Presenter{
        void fetchPeopleCheckedIn();
    }

}
