package com.example.warefhaque.dapeandroid.People;


import android.util.Log;

import com.parse.FindCallback;
import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseQuery;
import com.parse.ParseUser;

import java.util.List;

/**
 * Created by warefhaque on 1/14/17.
 */

public class PeoplePresenter implements PeopleInterface.Presenter{

    PeopleInterface.View mView;
    public PeoplePresenter(PeopleInterface.View view){
        mView = view;
    }

    @Override
    public void fetchPeopleCheckedIn() {

        try {

            ParseUser.getCurrentUser().fetch();
            ParseObject mPlace = ParseUser.getCurrentUser().getParseObject("checkedIn");

            if (mPlace==null){
                mView.failedfetch(false);
            }else{
                ParseQuery<ParseUser> query = ParseUser.getQuery();
                query.whereEqualTo("checkedIn",mPlace);
                query.whereNotEqualTo("objectId",ParseUser.getCurrentUser().getObjectId());
                query.findInBackground(new FindCallback<ParseUser>() {
                    @Override
                    public void done(List<ParseUser> objects, ParseException e) {

                        if (e==null){
                            Log.d("FETCHED PEOPLE", ""+ objects.size());
                            mView.fetchedCheckedInPeople(objects);
                        }else{
                            mView.failedfetch(true);
                        }
                    }
                });
            }


        } catch (ParseException e) {
            e.printStackTrace();
        }


    }
}
