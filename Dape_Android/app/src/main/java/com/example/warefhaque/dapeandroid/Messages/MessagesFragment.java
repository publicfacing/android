package com.example.warefhaque.dapeandroid.Messages;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.example.warefhaque.dapeandroid.Layer.LayerImpl;
import com.example.warefhaque.dapeandroid.LoadingDialog;
import com.example.warefhaque.dapeandroid.MyPushNotificationReceiver;
import com.example.warefhaque.dapeandroid.People.FragmentStatePagerSupport;
import com.example.warefhaque.dapeandroid.R;
import com.layer.sdk.messaging.Conversation;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

import static com.facebook.FacebookSdk.getApplicationContext;

/**
 * Created by warefhaque on 2/18/17.
 */

public class MessagesFragment extends Fragment implements MessagesPresenter.MessageAdapterListener, MessagesListAdapter.UpdateActivities, FragmentStatePagerSupport.DareCompleted, MyPushNotificationReceiver.FlowNotificationListener{
    public static final String TAG = "Messages Fragment";
    View rootView;
    MessagesListAdapter mMessagesListAdapter;
    MessagesPresenter mMessagesPresenter= new MessagesPresenter(this);
    public LoadingDialog mLoadingDialog;
    public TextView loadingActivies;
    public SwipeRefreshLayout mSwipeRefreshLayout;
    public StickyListHeadersListView activitiesList;
    public HashMap<String, Date> mActivityToCheckin;
    public RelativeLayout mMesagesLayout;
    public RelativeLayout mFetchingMessages;
    public TextView mNoActivities;
    public TextView mFetchingActivities;


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_messages, container, false);
        //set the adapter
        mMessagesListAdapter = new MessagesListAdapter(getActivity(),this);
        FragmentStatePagerSupport.dareCompleted = this;
        MyPushNotificationReceiver.flowNotificationListener = this;
        loadingActivies = (TextView) rootView.findViewById(R.id.no_activities);
        mSwipeRefreshLayout = (SwipeRefreshLayout) rootView.findViewById(R.id.messages_swipe_to_refresh);
        mMesagesLayout = (RelativeLayout) rootView.findViewById(R.id.messages_layout);
        mFetchingMessages = (RelativeLayout) rootView.findViewById(R.id.loading_activities);
        mNoActivities = (TextView) rootView.findViewById(R.id.no_current_activities);
        mFetchingActivities = (TextView) rootView.findViewById(R.id.fetching_activities);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                    setUiState(MessagesUiState.FETCHING_ACTIVITIES);
                    setupConversationView();


            }
        });

        StickyListHeadersListView activityView = (StickyListHeadersListView) rootView.findViewById(R.id.list);

        activityView.setAdapter(mMessagesListAdapter);

        activityView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
//                Toast.makeText(getApplicationContext(),"Loading Conversation...", Toast.LENGTH_SHORT).show();
                String type = mMessagesListAdapter.mActivities.get(position).mActivity.getString("type");

                if (type.equals("conversation")){

                    showConversation(position);

                }else if (type.equals("dare")){

                    if (mMessagesListAdapter.mActivities.get(position).mActivity.getParseObject("createdBy").getObjectId().equals(ParseUser.getCurrentUser().getObjectId())){
                        //you have created it. go to the OTHER persons profile
                        showUserProfile(position);

                    }else{
                        if (ifDareOpened(position)){

                            showUserProfile(position);

                        }else{

                            openDare(position);

                        }


                    }
                }

            }
        });
        updateActivities();
        return rootView;
    }

    //Set up the Query Adapter that will drive the RecyclerView on the conversations_screen
    public void setupConversationView() {

        //only come here when you are authenticated
        /**
         * In the xml have a view which says loading
         * have the whole conversation as gone. only display when it goes to
         * fetchedListOfActivities
         */
        Log.d(TAG, "Calling fetchUserActivities...");
        mMessagesPresenter.fetchUserActivities();

    }

    @Override
    public void onResume() {
        super.onResume();

    }

    @Override
    public void fetchedListOfActivities(List<ParseObject> activities, List<Conversation>conversations) {

        if (activities.size()>0){
            setUiState(MessagesUiState.FETCHED_ACTIVITIES);
            mSwipeRefreshLayout.setRefreshing(false);

            List<MessagesListAdapter.DareActivity> adapterList = setupAdaoterList(activities,conversations);

            mMessagesListAdapter.mActivities.clear();
            mMessagesListAdapter.mActivities.addAll(adapterList);
            mMessagesListAdapter.mSectionIndices = mMessagesListAdapter.getSectionIndices();
            mMessagesListAdapter.mSectionHeaders = mMessagesListAdapter.getSectionLetters();
            mMessagesListAdapter.notifyDataSetChanged();
        }else {
            //set ui state for the no activities
            setUiState(MessagesUiState.NO_ACTIVITIES);
        }
    }

    boolean ifDareOpened (int position){
        List<ParseUser> readBy = mMessagesListAdapter.mActivities.get(position).mActivity.getList("readBy");
        for (int i =0;i<readBy.size();i++){
            if (readBy.get(i).getObjectId().equals(ParseUser.getCurrentUser().getObjectId())){
                return true;
            }
        }

        return false;

    }

    public void showUserProfile(int position){
        List<ParseObject> participants = mMessagesListAdapter.mActivities.get(position).mActivity.getList("participants");
        String userId="";

        for (ParseObject particpant:participants){

            if (!particpant.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) userId = particpant.getObjectId();

        }

        Intent intent = new Intent(getActivity().getApplicationContext(),DareOpenedActivity.class);
        intent.putExtra("user_id",userId);
        startActivity(intent);


    }
    public void openDare(int position){
        Intent intent = new Intent(getApplicationContext(),DareInterfaceActivity.class);
        intent.putExtra("objectId",mMessagesListAdapter.mActivities.get(position).mActivity.getObjectId());

        List<ParseObject> participants = mMessagesListAdapter.mActivities.get(position).mActivity.getList("participants");
        for (ParseObject particpant:participants){

            if (!particpant.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) intent.putExtra("participantId",particpant.getObjectId());

        }

        startActivity(intent);
    }


    public void showConversation(int position){
        List<ParseObject> participants = mMessagesListAdapter.mActivities.get(position).mActivity.getList("participants");
        String participantName="";
        String participantId = "";

        for (ParseObject participant:participants) {

            if (!participant.getObjectId().equals(ParseUser.getCurrentUser().getObjectId())) {

                if (participant.has("name")) participantName = participant.getString("name");
                participantId = participant.getObjectId();

            }
        }

        String conversationId = mMessagesListAdapter.mActivities.get(position).mActivity.getString("cid");
        Intent intent = new Intent(getApplicationContext(), MessagesListActivity.class);

        Log.d(TAG,"conversation item clicked " + conversationId);

        Uri uri = Uri.parse(conversationId);

        intent.putExtra(PushNotificationReceiver.LAYER_CONVERSATION_KEY, uri);
        intent.putExtra("participantName",participantName);
        intent.putExtra("user_id",participantId);

        startActivity(intent);
    }

    /**
     * Goes through the list of all the conversatins and activities and makes them
     * into Dare ACtivity objects. The adapter uses this for displaying.
     * @param activities the list of user activities fetched from parse
     * @param conversations the list of conversations fetcehd from dare
     * @return
     */
    public List<com.example.warefhaque.dapeandroid.Messages.MessagesListAdapter.DareActivity> setupAdaoterList(List<ParseObject> activities, List<Conversation>conversations){


        List<MessagesListAdapter.DareActivity> result = new ArrayList<>();
        List<ParseObject> checkinsOnly = new ArrayList<>();
        mActivityToCheckin = new HashMap<>();
        /**
         * get the checkin object only, this includes the duplicates
         */
        for (ParseObject parseObject: activities){
             if (parseObject.getString("type").equals("checkin")) {
                 checkinsOnly.add(parseObject);
             }
        }

        /**
         * get Unique checkins sorted by the most recent one that the user made
         * this is required to store the checkin times in hash map and populate the headers
         * with the correct time as you do dareActivty.checkinTime and relate to a checkin object below
         */

        List<ParseObject> uniqueCheckins = getUniqueSortedCheckins(checkinsOnly);
        for (ParseObject parseObject: uniqueCheckins){
            mActivityToCheckin.put(parseObject.getParseObject("place").getObjectId(),parseObject.getDate("when"));
        }

        /**
         * go through the conversations and activies
         */
        for (Conversation conversation: conversations){
            for (ParseObject parseObject: activities){
                if (parseObject.getString("type").equals("conversation")){
                    if (parseObject.getString("cid")!=null) {
                        Uri cid = Uri.parse(parseObject.getString("cid"));
                        if (conversation.getId().equals(cid)) {
                            ParseUser createdBy = (ParseUser) parseObject.get("createdBy");
                            MessagesListAdapter.DareActivity dareActivity = new MessagesListAdapter.DareActivity(conversation, parseObject,createdBy,parseObject.getString("type"));
                            dareActivity.checkinTime = mActivityToCheckin.get(parseObject.getParseObject("place").getObjectId());
                            result.add(dareActivity);
                        }
                    }
                }
            }
        }

        /**
         * now dares
         */

        for (ParseObject parseObject:activities){
            if (parseObject.getString("type").equals("dare")){
                ParseUser createdBy = (ParseUser) parseObject.get("createdBy");
                MessagesListAdapter.DareActivity dareActivity= new MessagesListAdapter.DareActivity(null,parseObject,createdBy,parseObject.getString("type"));
                dareActivity.checkinTime = mActivityToCheckin.get(parseObject.getParseObject("place").getObjectId());
                result.add(dareActivity);
            }
        }

        /**
         * finaly the unique check in objects themselves.
         */

        for (ParseObject parseObject: uniqueCheckins){
            if (parseObject.getString("type").equals("checkin")){
                ParseUser createdBy = (ParseUser) parseObject.get("createdBy");
                MessagesListAdapter.DareActivity dareActivity= new MessagesListAdapter.DareActivity(null,parseObject,createdBy,parseObject.getString("type"));
                //same as the "when"
                dareActivity.checkinTime = parseObject.getCreatedAt();
                result.add(dareActivity);
            }
        }

        HashMap<String, List<MessagesListAdapter.DareActivity>> hashMap = new HashMap<>();

        Collections.sort(result, new DareActivityComparator());

        for (int i =result.size()-1;i>=0;i--){
            MessagesListAdapter.DareActivity dareActivity = result.get(i);
            if (!hashMap.containsKey(dareActivity.mActivity.getParseObject("place").getObjectId())) {
                List<MessagesListAdapter.DareActivity> list = new ArrayList<>();
                list.add(dareActivity);
                hashMap.put(dareActivity.mActivity.getParseObject("place").getObjectId(), list);
            } else {
                hashMap.get(dareActivity.mActivity.getParseObject("place").getObjectId()).add(dareActivity);
            }
        }

        List<MessagesListAdapter.DareActivity> finalList = new ArrayList<>();
        for (int i =0; i<uniqueCheckins.size();i++){
            ParseObject checkin = uniqueCheckins.get(i);
            List<MessagesListAdapter.DareActivity> groupedList = hashMap.get(checkin.getParseObject("place").getObjectId());
            finalList.addAll(groupedList);
        }

        Log.d(TAG, "Size: "+result.size());
        return finalList;
    }

    @Override
    public void updateActivities() {
        if (LayerImpl.isAuthenticated()) {
            setupConversationView();
        }
    }

    @Override
    public void updateFlowList(String name) {
        Log.d(TAG, "trying to dare " + name);
        updateActivities();
    }

    @Override
    public void updateFlowList() {
        setupConversationView();
    }


    public static class DareActivityComparator implements Comparator<MessagesListAdapter.DareActivity> {
        @Override
        public int compare(MessagesListAdapter.DareActivity o1, MessagesListAdapter.DareActivity o2) {
           Date date1 = o1.mActivity.getCreatedAt();
           Date date2 = o2.mActivity.getCreatedAt();


           return date1.compareTo(date2);
        }
    }

    public static class ParseObjectComparator implements Comparator<ParseObject>{

        @Override
        public int compare(ParseObject parseObject, ParseObject t1) {
            Date date1 = parseObject.getCreatedAt();
            Date date2 = t1.getCreatedAt();

            return date1.compareTo(date2);

        }
    }

    enum MessagesUiState{
        FETCHING_ACTIVITIES,
        FETCHED_ACTIVITIES,
        NO_ACTIVITIES,
        AUTHENTING_LAYER,
        AUTHENTICATION_FAILED
    }

    public void setUiState(MessagesUiState messagesUiState){
        switch (messagesUiState){
            case FETCHED_ACTIVITIES:
                mMesagesLayout.setVisibility(View.VISIBLE);
                mFetchingMessages.setVisibility(View.GONE);
                mNoActivities.setVisibility(View.GONE);
                break;
            case FETCHING_ACTIVITIES:
                mNoActivities.setVisibility(View.GONE);
                mFetchingMessages.setVisibility(View.VISIBLE);
                mMesagesLayout.setVisibility(View.GONE);
                break;
            case NO_ACTIVITIES:
                mFetchingMessages.setVisibility(View.GONE);
                mMesagesLayout.setVisibility(View.GONE);
                mNoActivities.setVisibility(View.VISIBLE);
                mNoActivities.setText("No activities yet. Start a conversation, check in somewhere or Dape someone!");
                break;
            case AUTHENTING_LAYER:
                mNoActivities.setVisibility(View.GONE);
                mFetchingMessages.setVisibility(View.VISIBLE);
                mMesagesLayout.setVisibility(View.GONE);
                break;
            case AUTHENTICATION_FAILED:
                mFetchingMessages.setVisibility(View.GONE);
                mMesagesLayout.setVisibility(View.GONE);
                mNoActivities.setVisibility(View.VISIBLE);
                mNoActivities.setText("Authentication failed. Please refresh the application");
        }
    }

    /**
     *
     * Utility functions which gives me unique check objects only
     * @param checkinsOnly
     * @return
     */
    public List<ParseObject> getUniqueSortedCheckins(List<ParseObject> checkinsOnly){
        Collections.sort(checkinsOnly, new ParseObjectComparator());
        //from now on sorted checkins only




        for (int i =checkinsOnly.size()-1;i>=0;i--){
            Log.d("check ins only", checkinsOnly.get(i).getParseObject("place").getObjectId());
        }

        //placeIds are sorted here according to the created at
        List<String> placeIds = new ArrayList<>();
        for (ParseObject parseObject: checkinsOnly){
            placeIds.add(parseObject.getParseObject("place").getObjectId());
        }
        //can be optimized, doing a lot of computation.


        for (int i =placeIds.size()-1;i>=0;i--){
            Log.d("placeIds", placeIds.get(i));
        }
        List<String> uniqueIds = new ArrayList<>();
        for (int i =placeIds.size()-1; i>=0; i--){
            if (!uniqueIds.contains(placeIds.get(i))){
                uniqueIds.add(placeIds.get(i));
            }
        }

        List<ParseObject> uniqueCheckins = new ArrayList<>();
        for (int i =0; i< uniqueIds.size(); i++){
            String id = uniqueIds.get(i);
            for (int j=checkinsOnly.size()-1; j>=0; j--){
                if (checkinsOnly.get(j).getParseObject("place").getObjectId().equals(id)){
                    uniqueCheckins.add(checkinsOnly.get(j));
                    break;
                }
            }
        }

        for (int i =0; i< uniqueIds.size(); i++){
            Log.d("UNIQUE IDS", uniqueIds.get(i));
        }

        for (int i =0; i< uniqueCheckins.size(); i++){
            Log.d("UNIQUE CHECKINS LS", uniqueCheckins.get(i).getParseObject("place").getObjectId());
        }
        return uniqueCheckins;

    }

}
