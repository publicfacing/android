package com.example.warefhaque.dapeandroid.People;

import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.warefhaque.dapeandroid.R;
import java.util.ArrayList;

/**
 * Created by warefhaque on 1/14/17.
 */

public class FragmentStatePagerSupport extends FragmentActivity {
   static int NUM_ITEMS ;

    MyAdapter mAdapter;

    ViewPager mPager;

    static ArrayList<String> mUserIds;

    int mpositionClicked;

    public static DareCompleted dareCompleted;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_pager);

        mUserIds = getIntent().getStringArrayListExtra("userIds");
        mpositionClicked = getIntent().getIntExtra("position", 0);

        NUM_ITEMS = mUserIds.size();
        mAdapter = new MyAdapter(getSupportFragmentManager());

        mPager = (ViewPager)findViewById(R.id.pager);
        mPager.setAdapter(mAdapter);

        //set the position to the item in the list clicked
        mPager.setCurrentItem(mpositionClicked);
        // Watch for button clicks.
        FloatingActionButton button = (FloatingActionButton) findViewById(R.id.goto_first);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mPager.setCurrentItem(0);
            }
        });
        button = (FloatingActionButton)findViewById(R.id.goto_last);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                mPager.setCurrentItem(NUM_ITEMS-1);
            }
        });
    }

    public static class MyAdapter extends FragmentStatePagerAdapter {
        public MyAdapter(FragmentManager fm) {
            super(fm);
        }

        @Override
        public int getCount() {
            return NUM_ITEMS;
        }

        @Override
        public Fragment getItem(int position) {
            return ArrayListFragment.newInstance(mUserIds.get(position));
        }
    }

    public static class ArrayListFragment extends Fragment {
        String idToFetch;


        /**
         * Create a new instance of CountingFragment, providing "num"
         * as an argument.
         */
        static ArrayListFragment newInstance(String userId) {
            ArrayListFragment f = new ArrayListFragment();

            // Supply num input as an argument.
            Bundle args = new Bundle();
            args.putString("userId", userId);
            f.setArguments(args);

            return f;
        }

        /**
         * When creating, retrieve this instance's number from its arguments.
         */
        @Override
        public void onCreate(Bundle savedInstanceState) {
            super.onCreate(savedInstanceState);
            idToFetch = getArguments() != null ? getArguments().getString("userId"): "";
        }

        /**
         * The Fragment's UI is just a simple text view showing its
         * instance number.
         */
        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            DetailedPeopleItem detailedPeopleItem = new DetailedPeopleItem();

            View v = detailedPeopleItem.DetailedPeopleItem(getLayoutInflater(savedInstanceState),container, idToFetch, getActivity());

            return v;
        }

        @Override
        public void onActivityCreated(Bundle savedInstanceState) {
            super.onActivityCreated(savedInstanceState);

        }
    }

    public static interface DareCompleted{
        public void updateFlowList(String name);
    }
}