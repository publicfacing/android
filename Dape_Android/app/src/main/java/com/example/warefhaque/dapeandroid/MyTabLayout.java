package com.example.warefhaque.dapeandroid;

import android.content.Context;
import android.support.design.widget.TabLayout;
import android.util.AttributeSet;
import android.view.MotionEvent;

/**
 * Created by warefhaque on 3/8/17.
 */

public class MyTabLayout extends TabLayout {
    private boolean pagingEnabled = true;

    public MyTabLayout(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public void setTabEnabled(boolean enabled) {
        pagingEnabled = enabled;
    }

    @Override
    public boolean onInterceptTouchEvent(MotionEvent event) {
        if (!pagingEnabled) {
            return false; // do not intercept
        }
        return super.onInterceptTouchEvent(event);
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (!pagingEnabled) {
            return false; // do not consume
        }
        return super.onTouchEvent(event);
    }
}
