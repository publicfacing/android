package com.example.warefhaque.dapeandroid.Authentication;

import android.content.Context;

import com.example.warefhaque.dapeandroid.BaseView;

/**
 * Created by warefhaque on 12/14/16.
 *
 * Description: Interface between the view and the presenter ONLY
 */

public interface LoginInterface {
    /**
     * Implemented by: LoginFragment
     * Called by: LoginPresenter
     */
    interface View extends BaseView<Presenter> {
        void facebookSignupComplete(String name);
        void facebookLoginComplete(String name);
        void facebookLoginIncomplete(String error);
        void facebookSignupIncomplete(String error);
        void setLoadingIndicator(boolean active);
    }

    /**
     * Implemented by: LoginPresenter
     * Called by: LoginFragment
     */
    interface Presenter{
        void facebookLogin(Context applicationContext);
    }
}
