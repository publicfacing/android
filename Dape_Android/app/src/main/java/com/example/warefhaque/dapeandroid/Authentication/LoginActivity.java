package com.example.warefhaque.dapeandroid.Authentication;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.example.warefhaque.dapeandroid.Database.ParseAuthentication;
import com.example.warefhaque.dapeandroid.R;
import com.example.warefhaque.dapeandroid.Utils.ActivityUtils;
import com.parse.ParseFacebookUtils;

public class LoginActivity extends AppCompatActivity {

    private LoginPresenter mPresenter;
    private ParseAuthentication mParseAuthentication;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        ActionBar actionBar = getSupportActionBar();
        if (actionBar!=null){
            actionBar.hide();
        }
        /**
         * Uses the Activity/Fragment/Presenter to keep the pattern consistent with the other tabs
         */

        //get the Frame in which the Fragment will live
        LoginFragment loginFragment = (LoginFragment) getSupportFragmentManager().findFragmentById(R.id.contentFrame);
        //if the frame does not contain a fragment, create it
        if (loginFragment==null){
            loginFragment=LoginFragment.newInstance();
            //add the Fragment in the Frame slot in the activity
            ActivityUtils.addFragmentToActivity(getSupportFragmentManager(), loginFragment, R.id.contentFrame);
        }

        mParseAuthentication = new ParseAuthentication();

        //set up the listener link between the presenter and the database.
        mPresenter = new LoginPresenter(loginFragment,mParseAuthentication);
        mParseAuthentication.setAuthenticationCallback(mPresenter);


        //connect the view and the presenter
        loginFragment.setPresenter(mPresenter);

    }



    @Override
    public void onBackPressed()
    {
        moveTaskToBack(true);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        ParseFacebookUtils.onActivityResult(requestCode, resultCode, data);
    }

}
