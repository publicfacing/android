package com.example.warefhaque.dapeandroid;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.example.warefhaque.dapeandroid.Authentication.LoginActivity;
import com.example.warefhaque.dapeandroid.Layer.LayerCallbacks;
import com.example.warefhaque.dapeandroid.Layer.LayerImpl;
import com.example.warefhaque.dapeandroid.Messages.MessagesFragment;
import com.example.warefhaque.dapeandroid.Nearby.NearbyFragment;
import com.example.warefhaque.dapeandroid.Nearby.NearbyListFragment;
import com.example.warefhaque.dapeandroid.Nearby.NearbyPresenter;
import com.example.warefhaque.dapeandroid.People.PeopleFragment;
import com.example.warefhaque.dapeandroid.People.PeoplePresenter;
import com.example.warefhaque.dapeandroid.Profile.ProfileFragment;
import com.example.warefhaque.dapeandroid.Profile.ProfilePresenter;
import com.example.warefhaque.dapeandroid.Settings.SettingsFragment;
import com.facebook.AccessToken;
import com.facebook.login.LoginManager;
import com.layer.sdk.exceptions.LayerException;
import com.parse.FunctionCallback;
import com.parse.GetCallback;
import com.parse.LogOutCallback;
import com.parse.Parse;
import com.parse.ParseCloud;
import com.parse.ParseException;
import com.parse.ParseGeoPoint;
import com.parse.ParseInstallation;
import com.parse.ParseObject;
import com.parse.ParseUser;

import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by warefhaque on 2/24/17.
 */

public class LauncherActivity extends AppCompatActivity implements LayerCallbacks{
    private static final String TAG = "Main Activity";
    public MyViewPager mViewPager;
    public ActionBar mActionBar;
    public MyTabLayout mtabLayout;
    public boolean mFirstTimeLogin;
    public static List<String> currentSocialMediaCopy;
    public TextView mSaveBtn;
    public ImageView mTabCheckOutButton;
    public TextView mEditBtn;
    public static final String lockScreenMessae = "You must share publicly at least 1 account";

    // 2700000 =  45 minutes
    public static int CHECK_OUT_THRESHOLD = 2700000;

    // 43200000 = 12 hours
    public static int FORCE_CHECKOUT = 43200000;

    public static boolean isActive=false;
    public LoadingDialog mLoadingDialog;


    //setActionBar("Places");
    public final ProfileFragment profileFragment = new ProfileFragment();
    public final MessagesFragment messagesFragment = new MessagesFragment();
    public final PeopleFragment peopleFragment = new PeopleFragment();
    final SettingsFragment settingsFragment = new SettingsFragment();
    final NearbyFragment nearbyFragment = new NearbyFragment();
    final NearbyListFragment nearbyListFragment = new NearbyListFragment();
    public Timer timer;
    TimerTask timerTask;
    final Handler handler = new Handler();
    public static final String LOGGED_OUT = "logged_out";
    public static final String LOGGED_IN = "logged_in";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        isActive=true;
        if (getIntent()!=null){
            if (getIntent().hasExtra("first_time")){
                mFirstTimeLogin = getIntent().getBooleanExtra("first_time",false);
            }
            else{
                mFirstTimeLogin = false;
            }
        }

//        mFirstTimeLogin = true;
        LayerImpl.initialize(getApplicationContext(), getApplication());

        // Registers the activity so callbacks are executed on the correct class
        LayerImpl.setContext(this);

        getSupportActionBar().setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM);
        getSupportActionBar().setCustomView(R.layout.custom_abs_layout);

        setContentView(R.layout.activity_view_pager);
        setActionBar("Places",0);
        setUpViewsAndFragments();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (timer == null){
            Log.d(TAG, "Declaring mytimer in launcher activity...");
            timer = new Timer();
        }
    }

    public void setActionBar(String title, int position){
//        if (mActionBar!=null){
//            mActionBar.setCustomView(R.layout.custom_abs_layout);
//            TextView myTitle = (TextView) mActionBar.getCustomView().findViewById(R.id.mytext);
//            myTitle.setText(title);
//        }
        Typeface custom_font = Typeface.createFromAsset(getAssets(), "fonts/RockoFLF.ttf");
        final TextView editbtn = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.edit_btn);
        final TextView savebtn = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.save_btn);

        final ImageView tabMapBtn = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.tab_map_button);
        final ImageView tabListBtn = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.tab_list_button);

        ImageView tabCheckOutBtn = (ImageView) getSupportActionBar().getCustomView().findViewById(R.id.tab_checkou_button);
        editbtn.setTypeface(custom_font);
        savebtn.setTypeface(custom_font);
        mEditBtn = editbtn;
        mSaveBtn = savebtn;
        mTabCheckOutButton = tabCheckOutBtn;
        mTabCheckOutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                checkOut();
            }
        });

        // logout button
        final TextView logoutButton = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.log_out_action_bar);
        logoutButton.setText("Log Out");
        logoutButton.setTypeface(custom_font);

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                logoutUser();
            }
        });
        if (mFirstTimeLogin)
        {
            //ON EDIT PRESS
            //the active state is enabled later in the code!
            //when the view pager is declared
            savebtn.setVisibility(View.VISIBLE);
            logoutButton.setVisibility(View.GONE);
            editbtn.setText("Cancel");
        }

        editbtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                    if (!profileFragment.state) {
                        //ON EDIT PRESS
                        savebtn.setVisibility(View.VISIBLE);
                        profileFragment.setStateActive(true);
                        editbtn.setText("Cancel");

                    } else {
                        //ON CANCEL PRESS

                        if (!mFirstTimeLogin){
                            if (profileFragment.shouldILockScreen()) {
                               lockScreen();
                                profileFragment.showDialog(lockScreenMessae);
                            }else{
                                unLockScreen();
                                profileFragment.setStateActive(false);
                                editbtn.setText("Edit");
                                savebtn.setVisibility(View.GONE);

                                //reset the values
                                profileFragment.mPresenter.setupProfileTextAndSocialMedia(ParseUser.getCurrentUser(), profileFragment.mAboutMe, profileFragment.mName);
                            }
                        }else{
                            //no cancel on first time login
                          lockScreen();
                            profileFragment.showDialog(lockScreenMessae);
                        }
                    }
            }
        });

        savebtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ON SAVE PRESS
                if (profileFragment.shouldILockScreen()){
                    lockScreen();
                    profileFragment.showDialog(lockScreenMessae);
                }else{
                    unLockScreen();
                    editbtn.setText("Edit");
                    profileFragment.setStateActive(false);
                    savebtn.setVisibility(View.GONE);
                    profileFragment.editProfileClicked();
                }
            }
        });

       if (!mFirstTimeLogin){
           if (position==0){
               editbtn.setVisibility(View.GONE);
               savebtn.setVisibility(View.GONE);
               logoutButton.setVisibility(View.GONE);
               if (nearbyFragment!=null)
               {
                   if (nearbyFragment.mListLayout!=null)
                   {
                       // this part only takes care of making it disappear in the list view
                       // and making it appear in the map view
                       // not the actual changing of the icons
                       if (nearbyFragment.mListLayout.getVisibility()==View.GONE)
                       {
                           tabMapBtn.setVisibility(View.GONE);
                           tabListBtn.setVisibility(View.VISIBLE);
                           mTabCheckOutButton.setVisibility(View.VISIBLE);

                           //checks which icon to put based on
                           // if placeCheckedIn == null or not
                           setmTabCheckOutButtonState();

                       }
                       else
                       {
                           tabMapBtn.setVisibility(View.VISIBLE);
                           tabListBtn.setVisibility(View.GONE);
                           mTabCheckOutButton.setVisibility(View.GONE);
                       }
                   }
                   else
                   {
                       tabMapBtn.setVisibility(View.GONE);
                       tabListBtn.setVisibility(View.VISIBLE);

                       // for the first time the app loads
                       // the list view of places = null
                       // need to check state manually
                       setmTabCheckOutButtonState();
                   }
               }
               else
               {
                   //shouldn't be
               }
               tabMapBtn.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       if (nearbyFragment!=null){
                           nearbyFragment.displayMapFromList();
                           tabMapBtn.setVisibility(View.GONE);
                           tabListBtn.setVisibility(View.VISIBLE);
                           mTabCheckOutButton.setVisibility(View.VISIBLE);

                           //checks which icon to put based on
                           // if placeCheckedIn == null or not
                           setmTabCheckOutButtonState();
                       }
                   }
               });
               tabListBtn.setOnClickListener(new View.OnClickListener() {
                   @Override
                   public void onClick(View view) {
                       if (nearbyFragment!=null){
                           nearbyFragment.goToListView();
                           tabMapBtn.setVisibility(View.VISIBLE);
                           tabListBtn.setVisibility(View.GONE);
                           mTabCheckOutButton.setVisibility(View.GONE);
                       }
                   }
               });
           }else if (position==1){
               //else set it to GONE
               tabCheckOutBtn.setVisibility(View.GONE);
               tabMapBtn.setVisibility(View.GONE);
               tabListBtn.setVisibility(View.GONE);
               editbtn.setVisibility(View.GONE);
               savebtn.setVisibility(View.GONE);
               logoutButton.setVisibility(View.GONE);
           }else if (position==2){
               tabCheckOutBtn.setVisibility(View.GONE);
               tabMapBtn.setVisibility(View.GONE);
               tabListBtn.setVisibility(View.GONE);
               tabCheckOutBtn.setVisibility(View.GONE);
               editbtn.setVisibility(View.GONE);
               savebtn.setVisibility(View.GONE);
               logoutButton.setVisibility(View.GONE);
           }else if (position==3){
               //default settings, only show the button at the top when
               // you are at the profile tab
               tabCheckOutBtn.setVisibility(View.GONE);
               tabMapBtn.setVisibility(View.GONE);
               tabListBtn.setVisibility(View.GONE);
               tabCheckOutBtn.setVisibility(View.GONE);
               editbtn.setVisibility(View.VISIBLE);
               logoutButton.setVisibility(View.GONE);
               if (profileFragment != null)
               {
                   //check if editing
                   if (profileFragment.state)
                   {
                       //editing...
                       savebtn.setVisibility(View.VISIBLE);
                   }
                   else
                   {
                       //not editing
                       savebtn.setVisibility(View.GONE);
                   }

               }
           }else if (position==4){
               tabCheckOutBtn.setVisibility(View.GONE);
               tabMapBtn.setVisibility(View.GONE);
               tabListBtn.setVisibility(View.GONE);
               tabCheckOutBtn.setVisibility(View.GONE);
               editbtn.setVisibility(View.GONE);
               savebtn.setVisibility(View.GONE);
               logoutButton.setVisibility(View.VISIBLE);
           }

       }else{
           tabListBtn.setVisibility(View.GONE);
           tabMapBtn.setVisibility(View.GONE);
           tabCheckOutBtn.setVisibility(View.GONE);
           //the edti and save buttns taken care of elsewhere
       }

        TextView textView = (TextView) getSupportActionBar().getCustomView().findViewById(R.id.title);


        textView.setTypeface(custom_font);
        textView.setText(title);


    }

    public void setmTabCheckOutButtonState()
    {
        //check if the user is checked in to see icon
        // this is for when the app reopens and no state of the button
        if (ParseUser.getCurrentUser() != null)
        {
            if (ParseUser.getCurrentUser().getParseObject("checkedIn") != null)
            {
                // maybe setting visible twice in some cases
                // but safe choice for when the app loads for the first time
                mTabCheckOutButton.setVisibility(View.VISIBLE);
                mTabCheckOutButton.setImageResource(R.drawable.check_action_bar);
                mTabCheckOutButton.setEnabled(true);
            }
            else
            {
                // dont need to set it gone if its transparent and gone
                mTabCheckOutButton.setImageResource(android.R.color.transparent);
                mTabCheckOutButton.setEnabled(false);
            }

        }
    }

    public void setUpViewsAndFragments(){
        mViewPager = (MyViewPager) findViewById(R.id.view_pager);

        mActionBar = getSupportActionBar();
        //setup the Profile presenter and the view to communicate
        final ProfilePresenter profilePresenter = new ProfilePresenter(profileFragment);
        profileFragment.setPresenter(profilePresenter);

        //setup the Nearby List Presenter and the View classes
        NearbyPresenter nearbyPresenter = new NearbyPresenter(nearbyListFragment);
        nearbyListFragment.setPresenter(nearbyPresenter);

        //setup the people fragment and the presenter
        PeoplePresenter peoplePresenter = new PeoplePresenter(peopleFragment);
        peopleFragment.setPresenter(peoplePresenter);

        //setup the people fragment and nearby list for place click
        nearbyFragment.setUpPeopleFragment(peopleFragment);

        mViewPager.setAdapter(new FragmentPagerAdapter(getSupportFragmentManager()) {
            @Override
            public Fragment getItem(int position) {
                if (position==0){
                    return nearbyFragment;
                }else if (position==1){
                    return peopleFragment;
                }else if (position==2){
                    return messagesFragment;
                }else if (position==3){
                    return profileFragment;
                }else{
                    return settingsFragment;
                }
            }


            @Override
            public int getCount() {
                return 5;
            }
        });

        mViewPager.setOffscreenPageLimit(3);

        mtabLayout = (MyTabLayout) findViewById(R.id.tab_layout);
        mtabLayout.setupWithViewPager(mViewPager);
        mtabLayout.getTabAt(0).setIcon(R.drawable.nearby_selector);
        mtabLayout.getTabAt(1).setIcon(R.drawable.people_selector);
        mtabLayout.getTabAt(2).setIcon(R.drawable.messages_selector);
        mtabLayout.getTabAt(3).setIcon(R.drawable.profile_selector);
        mtabLayout.getTabAt(4).setIcon(R.drawable.settings_selector);

        mtabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                mViewPager.setCurrentItem(tab.getPosition());
                if (tab.getPosition()==0){
                    setActionBar("Places",0);
                }else if (tab.getPosition()==1){
                    setActionBar("People",1);
                }else if (tab.getPosition()==2){
                    setActionBar("Flow",2);
                }else if (tab.getPosition()==3){
                    setActionBar("Profile",3);
                }else{
                    setActionBar("Settings",4);
                }
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                //default
            }
        });


        if (mFirstTimeLogin){
            lockScreen();
            mViewPager.setCurrentItem(3);
            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LauncherActivity.this);
            alertDialogBuilder.setCancelable(false);
            alertDialogBuilder.setTitle("Info");
            alertDialogBuilder.setMessage("Please share one social media for your dares or public entitlements");
            alertDialogBuilder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    //dismiss
                }
            });
            AlertDialog alertDialog = alertDialogBuilder.create();
            alertDialog.show();

        }

        if (getIntent().hasExtra("from_image_select"))
        {

            mViewPager.setCurrentItem(3);

        }

        // user opened push from the home screen.
        if (getIntent().hasExtra("ch_in_push"))
        {
            mViewPager.setCurrentItem(1);
            peopleFragment.upDatePeopleList();
        }

        if (getIntent().hasExtra("dape_push"))
        {
            mViewPager.setCurrentItem(2);
            messagesFragment.setupConversationView();
        }
        autoCheckout();
    }

    public void lockScreen(){
        mViewPager.setPagingEnabled(false);
        disableTab();
    }

    public void unLockScreen(){
        mViewPager.setPagingEnabled(true);
        enableTabs();
    }
    public void disableTab(){
        LinearLayout tabStrip = ((LinearLayout)mtabLayout.getChildAt(0));
        tabStrip.setEnabled(false);
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(false);
        }
    }

    public void enableTabs(){
        LinearLayout tabStrip = ((LinearLayout)mtabLayout.getChildAt(0));
        tabStrip.setEnabled(true);
        for(int i = 0; i < tabStrip.getChildCount(); i++) {
            tabStrip.getChildAt(i).setClickable(true);
        }
    }

    /**********************************************************
     * AUTO CHECKOUT FUNCTIONALITY BELOW
     * *********************************************************
     */

    public void autoCheckout(){

        if (ParseUser.getCurrentUser()!=null){
            ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
                @Override
                public void done(ParseObject object, ParseException e) {
                    if (e==null){
                        ParseObject place = ParseUser.getCurrentUser().getParseObject("checkedIn");
                        if (place!=null){
                            Date lastCheckedInTime = ParseUser.getCurrentUser().getDate("lastCheckInTime");
                            if (lastCheckedInTime==null){
                                Log.d(TAG, "last check in time = null");
                            }else{
                                Date currentDate = new Date();
                                // TODO: 4/25/17 what is 2700000 in hours?

                                if (currentDate.getTime()-lastCheckedInTime.getTime()>CHECK_OUT_THRESHOLD)
                                {
                                    alertDialogCheckout();
                                }

                                //happens if the user is chekced in beyong the 12 hour mark
                                if (currentDate.getTime() - lastCheckedInTime.getTime() > FORCE_CHECKOUT)
                                {
                                    // calls the checkout cloud code and then updates all the fragments
                                    checkOutCloudCall();
                                    Toast.makeText(LauncherActivity.this, "You cannot stay checked-in to one place for more than 12 hours!", Toast.LENGTH_SHORT).show();

                                }
                            }

                        }
                    }else{
                        if (e.getCode()==209){
                            Toast.makeText(LauncherActivity.this, "Your session token has expired Please login again", Toast.LENGTH_SHORT).show();
                            Intent intent = new Intent(LauncherActivity.this, LoginActivity.class);
                            startActivity(intent);
                        }
                    }
                }
            });

        }

    }


    public void startTimer() {
        //set a new Timer
        Log.d(TAG, "Started timer...");
        timer = new Timer();

        //initialize the TimerTask's job
        initializeTimerTask();

        //schedule the timer, after the first 5000ms the TimerTask will run every 10000ms
        timer.schedule(timerTask,CHECK_OUT_THRESHOLD); //
    }

    public void stoptimertask() {
        //stop the timer, if it's not already null
        if (timer != null) {
            timer.cancel();
            timer = null;
        }
    }

    public void initializeTimerTask() {

        timerTask = new TimerTask() {
            public void run() {

                //use a handler to run a toast that shows the current timestamp
                handler.post(new Runnable() {
                    public void run() {
                        alertDialogCheckout();
                    }
                });
            }
        };
    }

    public void alertDialogCheckout(){

        ParseUser.getCurrentUser().fetchInBackground(new GetCallback<ParseObject>() {
            @Override
            public void done(ParseObject object, ParseException e) {
                if (e==null){
                    try {

                        final ParseObject place = ParseUser.getCurrentUser().getParseObject("checkedIn");
                        if (place!=null){
                            place.fetch();
                            StringBuilder message = new StringBuilder("");
                            message.append(place.getString("name")).append("\n").append(place.getString("address"));
                            AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LauncherActivity.this);
                            alertDialogBuilder.setCancelable(false);
                            alertDialogBuilder.setTitle("Are you still here?");
                            alertDialogBuilder.setMessage(message);
                            alertDialogBuilder.setPositiveButton("I'm still here",
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface arg0, int arg1) {
                                            checkInStillHere(place);
                                        }
                                    });

                            alertDialogBuilder.setNegativeButton("Check out", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    //calls the cloud code and then updates each of fragments
                                    checkOutCloudCall();

                                }
                            });

                            AlertDialog alertDialog = alertDialogBuilder.create();
                            alertDialog.show();
                        }else{
                            Log.e(TAG, "Alert called but no checked in place found!");
                        }

                    } catch (ParseException exception) {
                        exception.printStackTrace();
                    }
                }else{
                    if (e.getCode()==209){
                        Toast.makeText(LauncherActivity.this, "Your session token has expired Please login again", Toast.LENGTH_SHORT).show();
                        Intent intent = new Intent(LauncherActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }
                }
            }
        });


    }

    public void checkInStillHere(ParseObject secondPlace)
    {

        HashMap<String, Object> params = new HashMap<>();

        params.put("place_id", secondPlace.get("placeId"));
        params.put("place_name", secondPlace.get("name"));

        params.put("place_address", secondPlace.get("address"));
        ParseGeoPoint location = secondPlace.getParseGeoPoint("location");
        params.put("place_location_lat", location.getLatitude());
        params.put("place_location_lon", location.getLongitude());
        params.put("place_types", "_type");
        params.put("user_id", ParseUser.getCurrentUser().getObjectId());
        params.put("user_location_lat",  NearbyFragment.mCurrentLocation.getLatitude());
        params.put("user_location_lon",NearbyFragment.mCurrentLocation.getLongitude());

        loadingIndicator(true);

        ParseCloud.callFunctionInBackground("check-in", params, new FunctionCallback<Object>() {
            public void done(Object data, ParseException e) {
                loadingIndicator(false);
                if (e == null) {
                    Log.d(TAG, data.toString());
                    Log.d("Launcher Activity","Cloud Call worked!");

                    stoptimertask();
                    startTimer();

                    //update the current Nearby List Screen
                    // this null makes sure the fragment is in the view
                    if (nearbyListFragment.mFetchingPlacesLayout !=null){
                        nearbyListFragment.fetchPlacesInfo();
                    }

                    //update the people checked in to the place.
                    peopleFragment.mPresenter.fetchPeopleCheckedIn();

                    //update the "Flow" screen
                    messagesFragment.updateActivities();

                    // put icons correctly
                    mTabCheckOutButton.setImageResource(R.drawable.check_action_bar);
                    mTabCheckOutButton.setEnabled(true);
                }else{
                    Toast.makeText(getApplicationContext(),"Error: " + e.getMessage(),Toast.LENGTH_SHORT).show();
                    Log.d(TAG, e.getMessage());
                    checkOut();

                }
            }
        });

    }

    public void checkOut(){

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(LauncherActivity.this);
        alertDialogBuilder.setCancelable(false);
        alertDialogBuilder.setTitle("Checkout");
        alertDialogBuilder.setMessage("Are you sure you want to checkout?");


        alertDialogBuilder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {

                //calls the cloud code and then updates each of the tabs
                checkOutCloudCall();

            }
        });

        alertDialogBuilder.setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                //nothing
            }
        });

        AlertDialog alertDialog = alertDialogBuilder.create();
        alertDialog.show();
    }

    @Override
    protected void onPause() {
        super.onPause();
        isActive=false;
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        isActive=false;
    }

    @Override
    public void onLayerConnected() {
        Log.d(TAG, "onLayerConnected");
        if (!LayerImpl.isAuthenticated()) {
            Log.d(TAG, "Calling authenticateUer...");
            LayerImpl.authenticateUser();
        }
    }

    @Override
    public void onLayerDisconnected() {
        Log.e(TAG, "onLayerDisconnected");
    }

    @Override
    public void onLayerConnectionError(LayerException e) {
        Log.e(TAG, "onLayerConnectionError" + e.toString());
    }

    @Override
    public void onUserAuthenticated(String id) {
        Log.d(TAG,"Authenticated: " + id);
    }

    @Override
    public void onUserAuthenticatedError(LayerException e) {
        Toast.makeText(getApplicationContext(), "onUserAuthenticatedError: " + e.toString(), Toast.LENGTH_SHORT).show();
        Log.e(TAG, "onUserAuthenticatedError" + e.toString());
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
    }

    public void logoutUser()
    {

        loadingIndicator(true);
        //STEP 1: check the user out
        if (ParseUser.getCurrentUser().get("checkedIn") !=null)
        {
            HashMap<String, Object> params = new HashMap<>();
            ParseCloud.callFunctionInBackground("check-out", params, new FunctionCallback<Object>() {
                @Override
                public void done(Object object, ParseException e) {

                    if (e==null)
                    {
                        // user checked out

                        //STEP 2: log the user out
                        ParseUser.getCurrentUser().logOutInBackground(new LogOutCallback() {
                            @Override
                            public void done(ParseException e) {
                                LoginManager.getInstance().logOut();
                                AccessToken.setCurrentAccessToken(null);

                                ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                                if (installation != null){
                                    installation.remove("user");
                                    installation.saveInBackground();
                                }
                                loadingIndicator(false);
                                // STEP 3: Deauthenticate from lat
                                //layer callback will take the user back to login activity.
                                LayerImpl.getLayerClient().deauthenticate();
                            }
                        });

                    }
                    else
                    {
                        Toast.makeText(LauncherActivity.this, "Error: Could not check you out. Please try logging out again.", Toast.LENGTH_SHORT).show();
                    }

                }
            });
        }
        else
        {
            ParseUser.getCurrentUser().logOutInBackground(new LogOutCallback() {
                @Override
                public void done(ParseException e) {
                    LoginManager.getInstance().logOut();
                    AccessToken.setCurrentAccessToken(null);

                    ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                    if (installation != null){
                        installation.remove("user");
                        installation.saveInBackground();
                    }

                    // STEP 3: Deauthenticate from lat
                    //layer callback will take the user back to login activity.
                    LayerImpl.getLayerClient().deauthenticate();
                }
            });
        }

    }

    @Override
    public void onUserDeauthenticated() {
        Log.e(TAG, "User Deauthenticated");
        // check the user out before logging out

//        Intent testIntent = new Intent(LauncherActivity.this, LoginSignupActivity.class);
//        startActivity(testIntent);
        Intent intent = new Intent(LauncherActivity.this, LoginActivity.class);
        startActivity(intent);

    }

    public void checkOutCloudCall()
    {

        HashMap<String, Object> params = new HashMap<>();
        ParseCloud.callFunctionInBackground("check-out", params , new FunctionCallback<Object>() {
            public void done(Object data, ParseException e) {
                if (e == null) {
                    Log.d(TAG, data.toString());

                    Toast.makeText(LauncherActivity.this,"You've checked out!",Toast.LENGTH_SHORT).show();

                    //update the people tab
                    LauncherActivity.this.peopleFragment.mPresenter.fetchPeopleCheckedIn();

                    //stop running the timer
                    if (LauncherActivity.this.timer!=null)
                    {
                        LauncherActivity.this.stoptimertask();
                    }
                    //update the flow tab
                    LauncherActivity.this.messagesFragment.updateActivities();

                    //start the map fragment over to avoid bugs
                    LauncherActivity.this.nearbyFragment.displayMapFromList();

                    // remove the checkout button from the tab bar at the top.
                    mTabCheckOutButton.setImageResource(android.R.color.transparent);
                    mTabCheckOutButton.setEnabled(false);

                }
                else
                {
                    Toast.makeText(LauncherActivity.this,"CLOUD LOG---"+e.getCode(),Toast.LENGTH_SHORT).show();
                    Log.d(TAG, e.getMessage());

                }
            }
        });
    }

    public void loadingIndicator(boolean active) {
        if (mLoadingDialog==null){
            mLoadingDialog=new LoadingDialog(LauncherActivity.this);
        }
        if (active){
            mLoadingDialog.show();
        }else{
            mLoadingDialog.dismiss();
        }
    }
}
