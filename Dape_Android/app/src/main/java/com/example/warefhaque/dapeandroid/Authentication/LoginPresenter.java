package com.example.warefhaque.dapeandroid.Authentication;

import android.app.Activity;
import android.content.Context;
import android.support.annotation.NonNull;
import android.util.Log;

import com.example.warefhaque.dapeandroid.Database.ParseAuthentication;
import com.example.warefhaque.dapeandroid.Database.ParseInterface;
import com.facebook.AccessToken;
import com.facebook.FacebookAuthorizationException;
import com.facebook.login.LoginManager;
import com.parse.LogInCallback;
import com.parse.ParseException;
import com.parse.ParseFacebookUtils;
import com.parse.ParseInstallation;
import com.parse.ParseUser;

import java.util.ArrayList;
import java.util.List;

import static com.google.common.base.Preconditions.checkNotNull;

/**
 * Created by warefhaque on 12/14/16.
 */

class LoginPresenter implements LoginInterface.Presenter,ParseInterface.authenticationCallBack {

    private static final String TAG = "Login Presenter";

    @NonNull
    private ParseInterface.authentication mAuthentication;

    @NonNull
    private LoginInterface.View mView;

    private static final List<String> mPermissions = new ArrayList<String>() {{
        add("public_profile");
        add("email");
        add("user_friends");
        add("user_about_me");
        add("user_location");
        add("user_birthday");
        add("user_photos");
        add("user_relationships");

    }};

    /**
     * Construct the presenter
     * @param view - The LoginFragment which implements the LoginInterface.View
     * @param mParseAuthentication - Instance of the ParseAuthentication class which implements ParseInterface.authentication
     */

    LoginPresenter(LoginInterface.View view, ParseAuthentication mParseAuthentication){

        mView = checkNotNull(view);
        mAuthentication = checkNotNull(mParseAuthentication);
    }


    /**
     * Description: Call coming in from LoginFragment.
     *              This starts FB login and then calls
     *              DB interface to save user and retrieve profile pic.
     */

    @Override
    public void facebookLogin(@NonNull Context context) {

        AccessToken.setCurrentAccessToken(null);
        if (ParseUser.getCurrentUser()!=null){
            //logging out just to be safe.
            ParseUser.logOut();
        }
        mView.setLoadingIndicator(true);
        Log.d(TAG,"Before Requesting for the permissions from the user!");
        ParseFacebookUtils.logInWithReadPermissionsInBackground((Activity) context, mPermissions, new LogInCallback() {
            @Override
            public void done(ParseUser user, ParseException err) {
                Log.d(TAG,"After permissions received!");
                if (err==null){
                    if (user == null) {
                        Log.d(TAG, "Uh oh. The user cancelled the Facebook login.");
                        mView.facebookSignupIncomplete("Uh oh. The user cancelled the Facebook login");
                    } else if (user.isNew()) {
                        Log.d(TAG, "User signed up and logged in through Facebook!");
                        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                        if (installation != null) {
                            installation.put("user", user);
                            installation.saveInBackground();
                        }
                        //The callbacks to the user interface come in through the other methods in this case

                        mAuthentication.getUserDetailsFb();
                    } else {
                        ParseInstallation installation = ParseInstallation.getCurrentInstallation();
                        if (installation != null) {
                            installation.put("user", user);
                            installation.saveInBackground();
                        }
                        //same as case 2
                        Log.d(TAG, "User logged in through Facebook!");
                        //mView.setLoadingIndicator(true);
                        mAuthentication.getUserDetailsParse();
                    }
                }else{
                    Log.e(TAG,err.getMessage());
                }

            }
        });


    }

    /**
     * Description: Call coming in from the DB saying the save
     *              and the profile pic retrieval was a success.
     */
    @Override
    public void newUserSaveSuccessful(String name) {
        //information coming in from the database to the presenter
        Log.d("newUserSignupSuccessful", "In the presenter success function!");
        mView.setLoadingIndicator(false);
        mView.facebookSignupComplete(name);
    }

    @Override
    public void userRetrievalSuccessfull(String name) {
        Log.d(TAG, "User login successful!");
        mView.setLoadingIndicator(false);
        mView.facebookLoginComplete(name);
    }

    @Override
    public void userRetrievalFailed(String error) {
        Log.d("user ", "In the presenter error function!");
        mView.setLoadingIndicator(false);
        mView.facebookLoginIncomplete(error);
    }

    /**
     * Description: Call coming in from the DB saying the save
     *              and the profile pic retrieval was a FAILURE!
     */
    @Override
    public void newUserSignupFailed(String error) {
        Log.d(TAG, "User Login failed");
        mView.setLoadingIndicator(false);
        mView.facebookSignupIncomplete(error);
    }
}
