package com.example.warefhaque.dapeandroid.Nearby;

import org.json.JSONArray;

/**
 * Created by warefhaque on 1/7/17.
 */

public interface NearbyInterface {

    interface View{
        void fetchSuccess(JSONArray places);
        void fetchFailure(String message);
    }
    interface Presenter{
        void fetchPlacesNearby(String url);
        void fetchPlacesList();
    }

    /**
     * display the map once the location button in places list is tapped
     */
    interface mapViewCallbackListener{
        void displayMapFromList();
    }


}

